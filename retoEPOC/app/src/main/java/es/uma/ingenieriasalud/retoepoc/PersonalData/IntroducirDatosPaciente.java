package es.uma.ingenieriasalud.retoepoc.personalData;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.MainActivity;
import es.uma.ingenieriasalud.retoepoc.persistence.PatientPersonalData;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.R;

public class IntroducirDatosPaciente extends AppCompatActivity {

    private EditText nombreEt, apellidosEt, nsgEt, alergiasEt,
            cuidadorInfEt, cuidadorInfTelEt,
            enfermeroEt, estadoEt, alturaEt, pesoEt, edadEt, enfermedadesEt, medicoFamiliaEt,
            medicoEspecialistaEt, relacionEt;

    private Modelo modelo;

    private boolean fum, diab;

    private RadioButton fumNo, diabNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_consulta_edicion_datos);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ImageButton editar;
        TextView nombreTv, apellidosTv, nsgTv;

        modelo = Modelo.getInstance();
        modelo.init(this);

        nombreEt = (EditText) findViewById(R.id.editTextNombre);
        apellidosEt = (EditText) findViewById(R.id.editTextApellidos);
        nsgEt = (EditText) findViewById(R.id.editTextNSG);
        alergiasEt = (EditText) findViewById(R.id.editTextAlergias);
        cuidadorInfEt = (EditText) findViewById(R.id.editTextCuidadorInformal);
        cuidadorInfTelEt = (EditText) findViewById(R.id.editTextTelCuidadorInformal);
        enfermeroEt = (EditText) findViewById(R.id.editTextEnfermeroReferencia);
        estadoEt = (EditText) findViewById(R.id.editTextEstado);
        pesoEt = (EditText) findViewById(R.id.editTextPeso);
        alturaEt = (EditText) findViewById(R.id.editTextAltura);
        edadEt = (EditText) findViewById(R.id.editTextEdad);
        enfermedadesEt = (EditText) findViewById(R.id.editTextEnfermedades);
        medicoFamiliaEt = (EditText) findViewById(R.id.editTextMedicoFamilia);
        medicoEspecialistaEt = (EditText) findViewById(R.id.editTextEspecialista);
        relacionEt = (EditText) findViewById(R.id.editTextRelacion);
        editar = (ImageButton) findViewById(R.id.imageEditar);
        nombreTv = (TextView) findViewById(R.id.textViewNombre);
        apellidosTv = (TextView) findViewById(R.id.textViewApellidos);
        nsgTv = (TextView) findViewById(R.id.textViewNSG);
        fumNo = (RadioButton) findViewById(R.id.radioButtonFumadorNo);
        diabNo = (RadioButton) findViewById(R.id.radioButtonDiabeticoNo);

        fumNo.setChecked(true);
        diabNo.setChecked(true);


        fum = false;
        diab = false;




        editar.setVisibility(View.GONE);
        nombreTv.append("*");
        apellidosTv.append("*");
        nsgTv.append("*");

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


    }

    public void onClickRadioButtonFumador (View view){
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButtonFumadorSi:
                if (checked)
                    fum = true;
                break;
            case R.id.radioButtonFumadorNo:
                if (checked)
                    fum = false;
                break;
        }
    }
    public void onClickRadioButtonDiabetico (View view){
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButtonDiabeticoSi:
                if (checked)
                    diab = true;
                break;
            case R.id.radioButtonDiabeticoNo:
                if (checked)
                    diab = false;
                break;
        }
    }




    public void onClickGuardar(View view) {
        if (nombreEt.getText().toString().equals("") || apellidosEt.getText().toString().equals("") || nsgEt.getText().toString().equals("")) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
            alertDialogBuilder
                    .setMessage(R.string.alertDialogCamposObligatorios)
                    .setCancelable(false)
                    .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else {
            SharedPreferences prefs = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("datosIntroducidos", true);
            editor.commit();

            modelo.insertPatientPersonalData(
                    new PatientPersonalData(nombreEt.getText().toString(), apellidosEt.getText().toString(),
                            nsgEt.getText().toString(), alturaEt.getText().toString(), pesoEt.getText().toString(),
                            edadEt.getText().toString(),fum,diab, enfermedadesEt.getText().toString(),
                            estadoEt.getText().toString(),alergiasEt.getText().toString(),
                            medicoFamiliaEt.getText().toString(), medicoEspecialistaEt.getText().toString(),
                            cuidadorInfEt.getText().toString(), cuidadorInfTelEt.getText().toString(),
                            relacionEt.getText().toString(),enfermeroEt.getText().toString()));

            Intent myIntent = new Intent(IntroducirDatosPaciente.this, MainActivity.class);
            startActivity(myIntent);

        }

    }

    @Override
    public void onBackPressed() {
    }
}
