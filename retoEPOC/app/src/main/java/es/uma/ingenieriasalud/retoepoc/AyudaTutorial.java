/***********************************************************************
 * This file is part of MyEPOC.
 * <p>
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class AyudaTutorial extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_tutoriales_listview);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        String listaTutoriales[] = new String[]{
                getResources().getString(R.string.textViewTituloTutorialMenuPrincipal),
                getResources().getString(R.string.textViewTituloTutorialManualesFisioterapia),
                getResources().getString(R.string.textViewTituloTutorialManualesInhaladores),
                getResources().getString(R.string.textViewTituloTutorialManualesInhaladorRescate),
                getResources().getString(R.string.textViewTituloCambiarIdioma),
                getResources().getString(R.string.textViewTituloTutorialCrearTratamiento),
                getResources().getString(R.string.textViewTituloTutorialConsultarTratamiento),
                getResources().getString(R.string.textViewTituloTutorialAlarmasToma),
                getResources().getString(R.string.textViewTituloTutorialCrearCitas),
                getResources().getString(R.string.textViewTituloTutorialConsultaCitas),
                getResources().getString(R.string.textViewTituloTutorialModConsDatosPersonales),
                getResources().getString(R.string.textViewTituloTutorialSms),};
        ListView listViewTutorials = (ListView) findViewById(R.id.listViewTutorials);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaTutoriales);
        listViewTutorials.setAdapter(adapter);
        listViewTutorials.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                startIntentWithLayout(position);
            }
        });

    }

    private void startIntentWithLayout(int position) {
        int resourceId = R.layout.activity_desarrollo_tutorial_manual_inhaladores;
        if (position == 0) {
            resourceId = R.layout.activity_desarrollo_tutorial_menu_principal;

        } else if (position == 1) {
            resourceId = R.layout.activity_desarrollo_tutorial_manuales_fisio;
        } else if (position == 2) {
            resourceId = R.layout.activity_desarrollo_tutorial_manual_inhaladores;

        } else if (position == 3) {
            resourceId = R.layout.activity_desarrollo_tutorial_inhalador_rescate;


        } else if (position == 4) {
            resourceId = R.layout.activity_desarrollo_tutorial_cambiar_idioma;

        } else if (position == 5) {
            resourceId = R.layout.activity_desarrollo_tutorial_crear_tratamiento;

        } else if (position == 6) {
            resourceId = R.layout.activity_desarrollo_tutorial_consultar_tratamiento;

        } else if (position == 7) {
            resourceId = R.layout.activity_desarrollo_tutorial_alarma_tomas;

        } else if (position == 8) {
            resourceId = R.layout.activity_desarrollo_tutorial_crear_citas;

        } else if (position == 9) {
            resourceId = R.layout.activity_desarrollo_tutorial_consultar_citas;

        } else if (position == 10) {
            resourceId = R.layout.activity_desarrollo_tutorial_mod_consulta_datos_personales;

        } else if (position == 11) {
            resourceId = R.layout.activity_desarrollo_tutorial_sms;

        }
        Bundle layoutBundle = new Bundle();
        layoutBundle.putInt("layout", resourceId);
        Intent intent = new Intent(getApplicationContext(), DesarrolloTutorial.class);
        intent.putExtras(layoutBundle);
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }

}
