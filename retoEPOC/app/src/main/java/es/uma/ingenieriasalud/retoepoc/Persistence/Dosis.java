/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.persistence;

import android.os.Parcel;
import android.os.Parcelable;

public class Dosis implements Parcelable{

    private double cantidad1, cantidad2;


    public Dosis(double cantidad1, double cantidad2) {
        this.cantidad1 = cantidad1;
        this.cantidad2 = cantidad2;
    }

    protected Dosis(Parcel in) {
        cantidad1 = in.readDouble();
        cantidad2 = in.readDouble();
    }

    public static final Creator<Dosis> CREATOR = new Creator<Dosis>() {
        @Override
        public Dosis createFromParcel(Parcel in) {
            return new Dosis(in);
        }

        @Override
        public Dosis[] newArray(int size) {
            return new Dosis[size];
        }
    };

    public double getCantidad1() {
        return cantidad1;
    }

    public void setCantidad1(double cantidad1) {
        this.cantidad1 = cantidad1;
    }

    public void setCantidad2(double cantidad2) {
        this.cantidad2 = cantidad2;
    }

    public double getCantidad2() {
        return cantidad2;
    }

    public String toString(){
        String representation = Double.toString(cantidad1);
        if (cantidad2 != 0) {
            representation += " / " + Double.toString(cantidad2);
        }
        return representation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(cantidad1);
        dest.writeDouble(cantidad2);
    }
}
