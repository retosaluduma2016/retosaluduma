package es.uma.ingenieriasalud.retoepoc.persistence;

/**
 * Created by Sergio on 19/05/2016.
 */
public class Treatment {

    private int id;
    private String recomendaciones;
    private int rescate;

    public Treatment(int id, String recomendaciones, int rescate) {

        this.id = id;
        this.recomendaciones = recomendaciones;
        this.rescate = rescate;
    }

    public String getRecomendaciones() {
        return recomendaciones;
    }

    public void setRecomendaciones(String recomendaciones) {
        this.recomendaciones = recomendaciones;
    }

    public int getRescate() {
        return rescate;
    }

    public void setRescate(int rescate) {
        this.rescate = rescate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
