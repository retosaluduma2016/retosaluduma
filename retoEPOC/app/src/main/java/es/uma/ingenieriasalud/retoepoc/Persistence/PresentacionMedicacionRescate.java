package es.uma.ingenieriasalud.retoepoc.persistence;

/**
 * Created by jmalvarez on 18/5/16.
 */
public class PresentacionMedicacionRescate {
    int id;
    String medicine, activeIngredient, inhaler;
    float dose_ai1, dose_ai2;

    public PresentacionMedicacionRescate(int id, String medicine, String activeIngredient,
                                         String inhaler, float dose_ai1, float dose_ai2) {
        this.id = id;
        this.medicine = medicine;
        this.activeIngredient = activeIngredient;
        this.inhaler = inhaler;
        this.dose_ai1 = dose_ai1;
        this.dose_ai2 = dose_ai2;
    }

    public int getId() { return id; }

    public String getMedicine() { return medicine; }

    public String getActiveIngredient() { return activeIngredient; }

    public String getInhaler() { return inhaler; }

    public float getDose_ai1() { return dose_ai1; }

    public float getDose_ai2() { return dose_ai2; }
}
