/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.appointments;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.persistence.Appointment;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.R;

public class ListViewCitasAdapter extends ArrayAdapter<Appointment> {

    private Context context;

    public ListViewCitasAdapter(Context context, ArrayList<Appointment> citas) {
        super(context, R.layout.listview_row_consultar_tratamiento, R.id.textView3, citas);
        this.context = context;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView==null){

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_row_consultar_citas, parent, false);
            holder=new ViewHolder();
            holder.botonBorrar = (ImageButton) convertView.findViewById(R.id.imageButton);
            holder.fechaCita= (TextView) convertView.findViewById(R.id.textView11);
            holder.horaCita= (TextView) convertView.findViewById(R.id.textView12);
            holder.especialidad= (TextView) convertView.findViewById(R.id.textView13);
            holder.nombre_medico= (TextView) convertView.findViewById(R.id.textView14);
            holder.p_diag= (TextView) convertView.findViewById(R.id.textView15);
            holder.p_val= (TextView) convertView.findViewById(R.id.textView16);
            convertView.setTag(holder);
        }else{

            holder = (ViewHolder) convertView.getTag();

        }

        holder.fechaCita.setText(context.getString(R.string.fechacita) + " "+getItem(position).getDia()+"/"+getItem(position).getMes()+"/"+getItem(position).getAño());
        holder.horaCita.setText(context.getString(R.string.horacita) + " "+getItem(position).getHora()+":"+getItem(position).getMin());
        holder.especialidad.setText(context.getString(R.string.especialidadcita) + " "+getItem(position).getEsp());
        holder.nombre_medico.setText(context.getString(R.string.atendidocita) + " "+getItem(position).getN_doctor());
        holder.p_diag.setText(context.getString(R.string.diagcita) + " "+getItem(position).getP_diag());
        holder.p_val.setText(context.getString(R.string.valcita) + " "+getItem(position).getP_val());
        holder.botonBorrar.setOnClickListener(new View.OnClickListener() {
                                                  @Override
                                                  public void onClick(View v) {
                                                      android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
                                                      alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
                                                      alertDialogBuilder
                                                              .setCancelable(true)
                                                              .setMessage(R.string.mensajeEliminarCita)
                                                              .setNegativeButton(R.string.alertDialogBotonNo, new DialogInterface.OnClickListener() {
                                                                  public void onClick(DialogInterface dialog, int id) {
                                                                      dialog.cancel();
                                                                  }
                                                              }).setPositiveButton(R.string.alertDialogBotonSi, new DialogInterface.OnClickListener() {
                                                          public void onClick(DialogInterface dialog, int id) {
                                                              Modelo modelo = Modelo.getInstance();
                                                              modelo.init(context);
                                                              modelo.borrarCita(getItem(position));
                                                              ListViewCitasAdapter.this.remove(getItem(position));
                                                              notifyDataSetChanged();
                                                              dialog.cancel();
                                                          }
                                                      });
                                                      android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                                                      alertDialog.show();
                                                  }
                                              }
        );

        return convertView;


    }

    static class ViewHolder {

        ImageButton botonBorrar;
        TextView fechaCita;
        TextView horaCita;
        TextView especialidad;
        TextView nombre_medico;
        TextView p_diag;
        TextView p_val;

    }

}
