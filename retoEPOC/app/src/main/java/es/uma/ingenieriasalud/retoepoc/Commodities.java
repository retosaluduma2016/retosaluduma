/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

public class Commodities {

    public final static String prefsFileName = "myPrefs";

    static public <T extends HasGetNameText> T getElementByName(ArrayList<T> list, String name){
        Iterator<T> iter = list.iterator();
        boolean found = false;
        T elem = null;
        while(iter.hasNext() && !found){
            elem = iter.next();
            found = elem.getNameText().equals(name);
        }
        if (found) { return elem; }
        else       { return null; }
    }

    static public String retrieveResourceStringFromId(Context context, String resId){
        return context.getResources().getString(context.getResources().getIdentifier(resId, "string", context.getPackageName()));
    }

    static public void setSelectedLanguage(Context context){

        Resources res = context.getResources();
        SharedPreferences preferences = context.getSharedPreferences(prefsFileName, Context.MODE_PRIVATE);
        String localeString = preferences.getString("languageLocale", "es");

        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(localeString.toLowerCase());
        res.updateConfiguration(conf, dm);

    }
    static public void ajustarVistaListView(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() + 10));
        listView.setLayoutParams(params);
    }

}
