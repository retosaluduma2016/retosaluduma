package es.uma.ingenieriasalud.retoepoc;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import es.uma.ingenieriasalud.retoepoc.personalData.IntroducirDatosPaciente;

public class IntroducirContrasena extends AppCompatActivity {

    EditText textsup, textinf;
    TextView text1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_introducir_contrasena);
        textinf = (EditText) findViewById(R.id.editText1);
        textsup = (EditText) findViewById(R.id.editText2);
        text1 = (TextView) findViewById(R.id.textView6);
        textsup.requestFocus();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.SEND_SMS}, PackageManager.PERMISSION_GRANTED);
        }

    }

    public void onClickButtonOk(View view){
        String ts,ti;
        ts = textsup.getText().toString();
        ti = textinf.getText().toString();

        if(ts.equals(ti) && (!ts.equals(""))){
            SharedPreferences prefs = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            String pass = SHA1(ts);
            editor.putString("contrasena", pass);
            editor.putBoolean("sms", false);
            editor.commit();
            //Intent myIntent = new Intent(TermsAndConditions.this, MainActivity.class);
            Intent myIntent = new Intent(this, IntroducirDatosPaciente.class);
            startActivity(myIntent);
        }else{
            Toast pulsarAdd = Toast.makeText(this, R.string.textPasswordsMustMatch, Toast.LENGTH_SHORT);
            pulsarAdd.show();
        }
    }

    /**
     * Returns the SHA1 hash for the provided String
     * @param text
     * @return the SHA1 hash or null if an error occurs
     */
    public static String SHA1(String text) {

        try {

            MessageDigest md;
            md = MessageDigest.getInstance("SHA-1");
            md.update(text.getBytes("UTF-8"),
                    0, text.length());
            byte[] sha1hash = md.digest();

            return toHex(sha1hash);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String toHex(byte[] buf) {

        if (buf == null) return "";

        int l = buf.length;
        StringBuffer result = new StringBuffer(2 * l);

        for (int i = 0; i < buf.length; i++) {
            appendHex(result, buf[i]);
        }

        return result.toString();

    }

    private final static String HEX = "0123456789ABCDEF";

    private static void appendHex(StringBuffer sb, byte b) {

        sb.append(HEX.charAt((b >> 4) & 0x0f))
                .append(HEX.charAt(b & 0x0f));

    }

    @Override
    public void onBackPressed() {

    }
}
