package es.uma.ingenieriasalud.retoepoc.createTreatment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.R;
import es.uma.ingenieriasalud.retoepoc.persistence.Inhalador;
import es.uma.ingenieriasalud.retoepoc.persistence.Medicamento;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.persistence.PrincipioActivo;
import es.uma.ingenieriasalud.retoepoc.persistence.Toma;

/**
 * Created by Sergio on 04/06/2016.
 */
public class AdapterListViewHoras extends ArrayAdapter<String> {

    private Context context;
    private ListView listView;




    public AdapterListViewHoras(Context context, ArrayList<String> listaTomas, ListView listView) {
        super(context, R.layout.vista_list_view_horas, listaTomas);
        // Guardamos los parámetros en variables de clase.
        this.context = context;
        this.listView = listView;


    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.vista_list_view_horas, null);


        TextView hora = (TextView) item.findViewById(R.id.textViewListaHorasTomas);
        hora.setText(getItem(position));

        final String h = hora.getText().toString();

        ImageButton borrar = (ImageButton) item.findViewById(R.id.imageButtonBorrarHora);
        borrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
                alertDialogBuilder
                        .setMessage(R.string.alertDialogEliminarHora)
                        .setCancelable(false)
                        .setPositiveButton(R.string.alertDialogBotonSi, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast pulsarAdd = Toast.makeText(context, R.string.toastBorrarHora, Toast.LENGTH_SHORT);
                                pulsarAdd.show();
                                AdapterListViewHoras.this.remove(h);
                                AdapterListViewHoras.this.notifyDataSetChanged();
                                Commodities.ajustarVistaListView(listView);
                            }
                        })
                        .setNegativeButton(R.string.alertDialogBotonNo, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();

            }
        });

        return item;
    }



}
