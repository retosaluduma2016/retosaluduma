package es.uma.ingenieriasalud.retoepoc.xmlDatabase;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.persistence.DatabaseInserter;

/**
 * Created by jmalvarez on 8/6/16.
 */
public class XmlPosologiesParser extends XmlEntryParser {

    private static final String ELEMENTS_TAGNAME = "posologies";
    private static final String SINGLE_ELEMENT_TAGNAME = "posology";

    private static final String DESC_FIELD_TAGNAME = "description";

    public XmlPosologiesParser(DatabaseInserter dbIns, XmlEntryParser n) {
        super(dbIns, n);
        elementsTagName = ELEMENTS_TAGNAME;
    }

    @Override
    void parseElement(XmlPullParser parser, ArrayList<String> elementsList) throws XmlPullParserException, IOException {
        String name = parser.getName();
        // Starts by looking for the entry tag
        switch (name) {
            case SINGLE_ELEMENT_TAGNAME:
                elementsList.add(readPosologyEntry(parser));
                break;
            default:
                skip(parser);
        }
    }

    private String readPosologyEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, SINGLE_ELEMENT_TAGNAME);
        String posName = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case DESC_FIELD_TAGNAME:
                    posName = readTextField(parser, DESC_FIELD_TAGNAME);
                    break;
                default:
                    skip(parser);
            }
        }
        return posName;
    }
}
