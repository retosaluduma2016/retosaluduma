/***********************************************************************
 * This file is part of MyEPOC.
 * <p/>
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.physiotherapy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.MainActivity;
import es.uma.ingenieriasalud.retoepoc.persistence.Fisioterapia;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.persistence.Paso;
import es.uma.ingenieriasalud.retoepoc.manuals.ScreenSlideActivity;
import es.uma.ingenieriasalud.retoepoc.R;

public class ConsultarManualFisioResp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_consultar_manual_fisio_resp);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        Modelo modelo = Modelo.getInstance();
        modelo.init(this);

        ArrayList<Fisioterapia> lista_fisioterapia = new ArrayList<>();
        obtenerListaFisioterapiasUsuario(modelo, lista_fisioterapia);


        ListView listaManualesFisioterapias = (ListView) findViewById(R.id.listViewManualesFisioResp);

        ListViewManualesFisioAdapter manualesFisioAdapter = new ListViewManualesFisioAdapter(this, lista_fisioterapia);
        listaManualesFisioterapias.setAdapter(manualesFisioAdapter);

        listaManualesFisioterapias.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Fisioterapia fisioterapiaSeleccionada = (Fisioterapia) parent.getItemAtPosition(position);
                consultar(fisioterapiaSeleccionada);

            }
        });


    }


    public void obtenerListaFisioterapiasUsuario(Modelo modelo, ArrayList<Fisioterapia> lista_fisioterapia) {
        ArrayList<Fisioterapia> listaCompletaInhaladores = modelo.obtenerListaFisioterapia();

        for (Fisioterapia fisio : listaCompletaInhaladores) {
            fisio.setListaPasosManual(modelo.obtenerPasosFisioterapia(fisio.getName()));
            boolean exists = false;

            for (Fisioterapia fi : lista_fisioterapia) {
                if (fi.getName().equals(fisio.getName())) {
                    exists = true;
                    break;
                }
            }

            if (!exists) {
                lista_fisioterapia.add(fisio);
            }

        }

    }

    public void consultar(Fisioterapia fis) {
        ArrayList<String> titulos = new ArrayList<>();
        ArrayList<Integer> imagenes = new ArrayList<>();
        ArrayList<String> textos = new ArrayList<>();
        for (Paso p : fis.getListaPasosManual()) {

            titulos.add(Commodities.retrieveResourceStringFromId(this, p.getTitulo()));
            imagenes.add(p.getImagen());
            textos.add(Commodities.retrieveResourceStringFromId(this, p.getTexto()));

        }

        Intent intent = new Intent(this, ScreenSlideActivity.class);
        Bundle b = new Bundle();

        b.putIntegerArrayList("imagenes", imagenes);
        b.putStringArrayList("texto", textos);
        b.putStringArrayList("titulos", titulos);

        intent.putExtras(b);
        startActivity(intent);


    }


    // back UI button
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }


}
