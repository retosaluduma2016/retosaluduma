/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.persistence;

import android.os.Parcel;
import android.os.Parcelable;

public class Hour implements Parcelable{

    private int hour, min;


    public Hour(int hour, int min) {
        this.hour = hour;
        this.min = min;
    }

    protected Hour(Parcel in) {
        hour = in.readInt();
        min = in.readInt();
    }

    public static final Creator<Hour> CREATOR = new Creator<Hour>() {
        @Override
        public Hour createFromParcel(Parcel in) {
            return new Hour(in);
        }

        @Override
        public Hour[] newArray(int size) {
            return new Hour[size];
        }
    };

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public String toString(){

        String representation;
        if(hour < 10){
         representation   ="0" + Integer.toString(hour);

        }else{
            representation   = Integer.toString(hour);

        }
        if(min < 10){
            representation += " : 0" + Integer.toString(min);
        }else{
            representation += " : " + Integer.toString(min);
        }

        //if (min != 0) {
          //  representation += " : " + Integer.toString(min);
        //}
        return representation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(hour);
        dest.writeInt(min);
    }
}
