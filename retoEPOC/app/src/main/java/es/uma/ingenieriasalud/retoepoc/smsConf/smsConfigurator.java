package es.uma.ingenieriasalud.retoepoc.smsConf;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.R;


public class smsConfigurator extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_config_sms);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.SEND_SMS}, PackageManager.PERMISSION_GRANTED);
        }
    }

    public void onClickButtonConf(View view) {

        switch (view.getId()) {
            case R.id.smsConfButtonEnable:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                        dialogConfigWindow(R.string.advertenciaSMSs1, R.string.smsEnabled, true);

                    }
                } else {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                        dialogConfigWindow(R.string.advertenciaSMSs1, R.string.smsEnabled, true);
                    }
                }
                break;
            case R.id.smsConfButtonDisable:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                        dialogConfigWindow(R.string.advertenciaSMSs2, R.string.smsDisabled, false);
                    }
                }else {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                        dialogConfigWindow(R.string.advertenciaSMSs2, R.string.smsDisabled, false);
                    }
                }
                break;
            default:
        }

/*
        android.app.AlertDialog.Builder alertDialogBuilderInicial = new android.app.AlertDialog.Builder(this);
        alertDialogBuilderInicial.setTitle(R.string.alerDialogTitulo);
        alertDialogBuilderInicial
                .setCancelable(true)
                .setMessage(R.string.quedesea)
                .setNegativeButton(R.string.activar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        android.app.AlertDialog.Builder alertDialogBuilder1 = new android.app.AlertDialog.Builder(smsConfigurator.this);
                        alertDialogBuilder1.setTitle(R.string.alerDialogTitulo);
                        alertDialogBuilder1
                                .setCancelable(true)
                                .setMessage(R.string.advertenciaSMSs1)
                                .setNegativeButton(R.string.alertDialogBotonSi, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Toast pulsarAdd = Toast.makeText(getApplicationContext(), R.string.smsEnabled, Toast.LENGTH_SHORT);
                                        pulsarAdd.show();
                                        editor.putBoolean("sms",true);
                                        editor.commit();
                                        dialog.cancel();
                                    }
                                })
                                .setPositiveButton(R.string.alertDialogBotonNo, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        android.app.AlertDialog alertDialog = alertDialogBuilder1.create();
                        alertDialog.show();
                    }
                })
                .setPositiveButton(R.string.desactivar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        android.app.AlertDialog.Builder alertDialogBuilder2 = new android.app.AlertDialog.Builder(smsConfigurator.this);
                        alertDialogBuilder2.setTitle(R.string.alerDialogTitulo);
                        alertDialogBuilder2
                                .setCancelable(true)
                                .setMessage(R.string.advertenciaSMSs2)
                                .setNegativeButton(R.string.alertDialogBotonSi, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Toast pulsarAdd = Toast.makeText(getApplicationContext(), R.string.smsDisabled, Toast.LENGTH_SHORT);
                                        pulsarAdd.show();
                                        editor.putBoolean("sms",false);
                                        editor.commit();
                                        dialog.cancel();
                                    }
                                })
                                .setPositiveButton(R.string.alertDialogBotonNo, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        android.app.AlertDialog alertDialog = alertDialogBuilder2.create();
                        alertDialog.show();
                    }
                });
        android.app.AlertDialog alertDialog = alertDialogBuilderInicial.create();
        alertDialog.show();

//        Intent intent = new Intent(this, MainActivity.class);
//      startActivity(intent);
*/
    }

    private void dialogConfigWindow(int warningMsgId, final int newStateMsgId, final boolean newState) {

        SharedPreferences preferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(smsConfigurator.this);
        alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
        alertDialogBuilder
                .setCancelable(true)
                .setMessage(warningMsgId)
                .setPositiveButton(R.string.alertDialogBotonSi, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast pulsarAdd = Toast.makeText(getApplicationContext(), newStateMsgId, Toast.LENGTH_SHORT);
                        pulsarAdd.show();
                        editor.putBoolean("sms", newState);
                        editor.commit();
                        dialog.cancel();
                    }
                })
                .setNegativeButton(R.string.alertDialogBotonNo, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void onClickBotonVolver(View view) {
        this.onBackPressed();
    }

}
