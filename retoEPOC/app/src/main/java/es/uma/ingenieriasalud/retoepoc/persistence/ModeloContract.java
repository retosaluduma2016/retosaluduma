package es.uma.ingenieriasalud.retoepoc.persistence;

import android.provider.BaseColumns;

public final class ModeloContract {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    private ModeloContract() {
    }

    public static abstract class MedicineTable implements BaseColumns {
        // BaseColumns provides String _ID and _COUNT
        public static final String TABLE_NAME = "Medicine";
        public static final String NAME = "name";
    }

    public static abstract class ActiveIngredientTable implements BaseColumns {
        // BaseColumns provides String _ID and _COUNT
        public static final String TABLE_NAME = "ActiveIngredient";
        public static final String NAME = "name";
    }

    public static abstract class PhysiotherapyTable implements BaseColumns {
        // BaseColumns provides String _ID and _COUNT
        public static final String TABLE_NAME = "Physiotherapy";
        public static final String NAME = "name_physiotherapy_xml";
        public static final String IMAGE = "image";
    }

    public static abstract class InhalerTable implements BaseColumns {
        // BaseColumns provides String _ID and _COUNT
        public static final String TABLE_NAME = "Inhaler";
        public static final String NAME = "name_inhaler_xml";
        public static final String IMAGE = "image";
    }

    public static abstract class PosologyTable implements BaseColumns {
        // BaseColumns provides String _ID and _COUNT
        public static final String TABLE_NAME = "Posology";
        public static final String TEXT_FREQUENCY_XML = "text_frecuency_xml";

    }

    public static abstract class MedicinePresentationTable {
        // BaseColumns provide    public static final String ModeloContract.TakeTable.TABLE_NAME = "Take";
        public static final String TABLE_NAME = "Medicine_Presentation";
        public static final String MEDICINE_NAME = "medicine_name";
        public static final String ACTIVEINGREDIENT_NAME = "activeIngredient_name";
        public static final String INHALER_NAME = "inhaler_name";
        public static final String DOSE_AI1 = "dose_ai1";
        public static final String DOSE_AI2 = "dose_ai2";
        public static final String POSOLOGY_NAME = "posology_name";
    }

    public static abstract class UserManualStepTable implements BaseColumns {
        // BaseColumns provide    public static final String ModeloContract.TakeTable.TABLE_NAME = "Take";
        public static final String TABLE_NAME = "UserManualStep";
        public static final String TITLE_XML = "title_xml";
        public static final String IMAGE_XML = "image_xml";
        public static final String TEXT_XML = "text_xml";
        public static final String INHALER_NAME = "inhaler_name";
    }

    public static abstract class UserManualPhysioStepTable implements BaseColumns {
        // BaseColumns provide    public static final String ModeloContract.TakeTable.TABLE_NAME = "Take";
        public static final String TABLE_NAME = "UserManualPhysioStep";
        public static final String TITLE_XML = "title_xml";
        public static final String IMAGE_XML = "image_xml";
        public static final String TEXT_XML = "text_xml";
        public static final String PHYSIO_NAME = "physio_name";
    }

    public static abstract class RescueTable implements BaseColumns {
        // BaseColumns provides String _ID and _COUNT
        public static final String TABLE_NAME = "Rescue";
        public static final String MEDICINE_NAME = "medicine_name";
        public static final String ACTIVEINGREDIENT_NAME = "activeIngredient_name";
        public static final String INHALER_NAME = "inhaler_name";
        public static final String DOSE_AI1 = "dose_ai1";
        public static final String DOSE_AI2 = "dose_ai2";
    }

    public static abstract class TreatmentTable implements BaseColumns {
        // BaseColumns provides String _ID and _COUNT
        public static final String TABLE_NAME = "Treatment";
        public static final String RESCUE_ID = "rescue_id";
        public static final String RECOMMENDATION = "recommendation";
    }

    public static abstract class TakeTable implements BaseColumns {
        // BaseColumns provides String _ID and _COUNT
        public static final String TABLE_NAME = "Take";
        public static final String TREATMENT_ID = "treatment_id";
        public static final String MEDICINE_NAME = "medicine_name";
        public static final String ACTIVEINGREDIENT_NAME = "active_Ingredient_id";
        public static final String INHALER_NAME = "inhaler_name";
        public static final String DOSE_AI1 = "dose_ai1";
        public static final String DOSE_AI2 = "dose_ai2";
        public static final String HOUR = "hour_hour";
        public static final String MINUTE = "hour_min";
        public static final String DONE = "done";
        public static final String INHALATIONS = "inhalations";
    }

    public static abstract class PatientPersonalDataTable implements BaseColumns {
        // BaseColumns provides String _ID and _COUNT
        public static final String TABLE_NAME = "PatientPersonalData";

        public static final String NAME = "name";
        public static final String SURNAME = "surname";
        public static final String NSS = "nss";
        public static final String HEIGHT = "height";
        public static final String WEIGHT = "weight";
        public static final String AGE = "age";

        public static final String SMOKER = "smoker";
        public static final String DIABETIC = "diabetic";
        public static final String SICKNESS = "sickness";
        public static final String SICKNESS_STATE = "sickness_state";
        public static final String ALERGY = "alergy";

        public static final String FAMILY_DOCTOR = "family_doctor";
        public static final String MEDICAL_SPECIALIST = "medical_specialist";
        public static final String CASUAL_CAREGIVER_NAME = "casual_caregiver_name";
        public static final String CASUAL_CAREGIVER_PHONE = "casual_caregive_phone";
        public static final String CASUAL_CAREGIVER_RELATION = "casual_caregive_relation";
        public static final String NURSE_IN_CHARGE = "nurse_in_charge";


    }

    public static abstract class AppointmentDataTable implements BaseColumns {
        // BaseColumns provides String _ID and _COUNT
        public static final String TABLE_NAME = "Appointment";
        public static final String DAY = "day";
        public static final String MONTH = "month";
        public static final String YEAR = "year";
        public static final String HOUR = "hour";
        public static final String MIN = "min";
        public static final String LOCATION = "location";
        public static final String DOCTOR_NAME = "doctor_name";
        public static final String DIAGNOSTIC_TEST = "diagnostic";
        public static final String VALORATION_TEST = "valoration";
    }
}

