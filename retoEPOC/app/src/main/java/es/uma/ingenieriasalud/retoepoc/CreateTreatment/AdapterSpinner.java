package es.uma.ingenieriasalud.retoepoc.createTreatment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.persistence.PresentacionMedicacionRescate;
import es.uma.ingenieriasalud.retoepoc.R;

/**
 * Created by Sergio on 18/05/2016.
 */
public class AdapterSpinner extends ArrayAdapter<PresentacionMedicacionRescate> {

    private Context context;

    public AdapterSpinner(Context context, int resource, ArrayList<PresentacionMedicacionRescate> datosTomaRescate) {
        super(context, resource, datosTomaRescate);

        this.context = context;
    }

    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        return getCustomView(position, cnvtView, prnt);

    }

    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
        return getCustomView(pos, cnvtView, prnt);
    }
    public View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.vista_spinner_rescate, parent, false);


        TextView medicina = (TextView) item.findViewById(R.id.textViewMedicinaRescate);
        TextView principio = (TextView) item.findViewById(R.id.textViewPrincipioActivoRescate);
        TextView inhalador = (TextView) item.findViewById(R.id.textViewInhaladorRescate);
        TextView dosis = (TextView) item.findViewById(R.id.textViewDosisRescate);




        medicina.append(": "+getItem(position).getMedicine());
        principio.append(": "+getItem(position).getActiveIngredient());
        inhalador.append(": "+getItem(position).getInhaler());

        if(getItem(position).getDose_ai2() == 0){
            dosis.append(": "+Float.toString(getItem(position).getDose_ai1()));
        }else{
            dosis.append(": "+Float.toString(getItem(position).getDose_ai1()) + " / " + Float.toString(getItem(position).getDose_ai2()));

        }


        return item;
    }
}

