package es.uma.ingenieriasalud.retoepoc.appointments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Locale;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.persistence.Appointment;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.R;

public class CitasActivity extends AppCompatActivity {

    TextView tVhora, tVmin;
    EditText tEspe, tName, tDiag, tVal;
    Spinner spinner,spinnerdia,spinnermes,spinneraño;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_citas);

        tVhora = (TextView) findViewById(R.id.textViewHour1);
        tVmin = (TextView) findViewById(R.id.textViewMinutes1);
        tEspe = (EditText) findViewById(R.id.editText4);
        tDiag = (EditText) findViewById(R.id.editText5);
        tVal = (EditText) findViewById(R.id.editText6);

        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.spinnerCitas, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinnerdia = (Spinner) findViewById(R.id.spinnerDia);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.spinnerCitasDia, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerdia.setAdapter(adapter1);

        spinnermes = (Spinner) findViewById(R.id.spinnerMes);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.spinnerCitasMes, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnermes.setAdapter(adapter2);

        spinneraño = (Spinner) findViewById(R.id.spinnerAño);
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this,
                R.array.spinnerCitasAño, android.R.layout.simple_spinner_item);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinneraño.setAdapter(adapter3);

    }


    public void onClickAddHora(View view) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        @NonNull
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user

            TextView tVhora = (TextView) getActivity().findViewById(R.id.textViewHour1);
            TextView tVmin = (TextView) getActivity().findViewById(R.id.textViewMinutes1);
            tVhora.setText(String.valueOf(hourOfDay));
            tVmin.setText(String.format(Locale.getDefault(), "%02d", minute));

        }
    }

    public void onClickAddAppointment(View view) {

        if (spinnerdia.getSelectedItem().toString().equals(getString(R.string.spinner1Dia)) || spinnermes.getSelectedItem().toString().equals(getString(R.string.spinner1Mes)) || spinneraño.getSelectedItem().toString().equals(getString(R.string.spinner1AÑo)) || tVhora.getText().toString().equals("HH") ||
                tVmin.getText().toString().equals("MM") || tEspe.getText().toString().equals("") ||
                spinner.getSelectedItem().toString().equals(getString(R.string.spinner1)) || tDiag.getText().toString().equals("") ||
                tVal.getText().toString().equals("") ) {
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
            alertDialogBuilder
                    .setCancelable(true)
                    .setMessage(R.string.alertDialogCitas)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else {
            Modelo modelo = Modelo.getInstance();
            modelo.init(this);
            String nombre_doctor ="";
            if(spinner.getSelectedItem().toString().equals(getString(R.string.spinner2))){
                 nombre_doctor = "("+getString(R.string.spinner2)+") "+ modelo.getPatientPersonalData().getNurseInCharge();
            }else if(spinner.getSelectedItem().toString().equals(getString(R.string.spinner3))){
                 nombre_doctor = "("+getString(R.string.spinner3)+") "+ modelo.getPatientPersonalData().getMedicoEspecialista();
            }else if(spinner.getSelectedItem().toString().equals(getString(R.string.spinner4))) {
                 nombre_doctor = "("+getString(R.string.spinner4)+") "+ modelo.getPatientPersonalData().getMedicoFamilia();
            }

            Appointment ap = new Appointment(spinnerdia.getSelectedItem().toString(), spinnermes.getSelectedItem().toString(), spinneraño.getSelectedItem().toString(), tVhora.getText().toString(), tVmin.getText().toString(),
                    tEspe.getText().toString(), nombre_doctor,tDiag.getText().toString(),tVal.getText().toString());

            modelo.insertAppointment(ap);
            this.onBackPressed();


        }
    }
}
