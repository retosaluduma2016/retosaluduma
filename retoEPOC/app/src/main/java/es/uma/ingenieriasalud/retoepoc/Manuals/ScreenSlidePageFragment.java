/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.uma.ingenieriasalud.retoepoc.manuals;

import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import es.uma.ingenieriasalud.retoepoc.R;

/**
 * A fragment representing content_crear_tratamiento single step in content_crear_tratamiento wizard. The fragment shows content_crear_tratamiento dummy title indicating
 * the page number, along with some dummy text.
 *
 */
public class ScreenSlidePageFragment extends Fragment {
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";


    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int mPageNumber;

    private ScreenSlideActivity padreFrament;

    /**
     * Factory method for this fragment class. Constructs content_crear_tratamiento new fragment for the given page number.
     */
    public static ScreenSlidePageFragment create(int pageNumber) {
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ScreenSlidePageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
        padreFrament = (ScreenSlideActivity)getActivity(); // Llama content_crear_tratamiento la actividad la cual contiene el fragment.


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout containing content_crear_tratamiento title and body text.
        ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.layout_tutorial_inhalador, container, false);

        // Set the title view to show the page number.
        ((TextView) rootView.findViewById(android.R.id.text1)).setText(
                padreFrament.getTitulos().get(getPageNumber()));

        //Al momento de crear el nuevo fragment se cambian el texto y la imagen que le corresponde.

        ImageView img;

        TextView text;

        Drawable d;

        img = (ImageView)rootView.findViewById(R.id.imagen);
        d = ContextCompat.getDrawable(rootView.getContext(), padreFrament.getImagenes().get(getPageNumber()));
        img.setImageDrawable(d);

        text = (TextView)rootView.findViewById(R.id.texto);
        text.setText(padreFrament.getTexto().get(getPageNumber()));

        return rootView;
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
}
