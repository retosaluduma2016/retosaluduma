/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.checkTreatment;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.R;

public class ListViewTomasAdapter extends ArrayAdapter<ArrayList<String>> {

    private Context context;

    public ListViewTomasAdapter(Context context, ArrayList tomas) {
        super(context, R.layout.listview_row_consultar_tratamiento, R.id.textView3, tomas);
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        boolean showSeparator;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_row_consultar_tratamiento, parent, false);
            holder = new ViewHolder();
            holder.nombreInhalador = (TextView) convertView.findViewById(R.id.textView3);
            holder.imagenInhalador = (ImageView) convertView.findViewById(R.id.row_click_imageView1);
            holder.nombreMedicamento = (TextView) convertView.findViewById(R.id.row_textView1);
            holder.idOculta = (TextView) convertView.findViewById(R.id.textView);
            holder.hora = (TextView) convertView.findViewById(R.id.textView4);
            holder.numero_veces = (TextView) convertView.findViewById(R.id.textView2);
            holder.divider = (TextView) convertView.findViewById(R.id.textView24);
            convertView.setTag(holder);
        } else {

            holder = (ViewHolder) convertView.getTag();

        }

        if (position == 0) {
            showSeparator = true;
        } else {
            if (!this.getItem(position - 1).get(4).equals(this.getItem(position).get(4))) {
                showSeparator = true;
            }else{
                showSeparator = false;
            }



            //holder.divider.setText(getItem(position).get(4));

        }
        holder.nombreMedicamento.setText(getItem(position).get(0));
        holder.idOculta.setText(getItem(position).get(1));
        holder.nombreInhalador.setText(getItem(position).get(2));
        holder.imagenInhalador.setImageResource(context.getResources().getIdentifier(getItem(position).get(3), "drawable", context.getPackageName()));
        holder.imagenInhalador.setImageBitmap(
                decodeSampledBitmapFromResource(context.getResources(), context.getResources().getIdentifier(getItem(position).get(3), "drawable", context.getPackageName()), 100, 100));
        holder.hora.setText(String.format("%s %s", context.getApplicationContext().getResources().getString(R.string.hora), getItem(position).get(4)));
        int inhlsNumber = Integer.parseInt(getItem(position).get(5));
        holder.numero_veces.setText(context.getApplicationContext().getResources().getQuantityString(R.plurals.numberOfInhalations, inhlsNumber, inhlsNumber));
        if (showSeparator) {
            holder.divider.setText(getItem(position).get(4));
            holder.divider.setVisibility(View.VISIBLE);
        } else {
            holder.divider.setVisibility(View.GONE);
        }
//        holder.numero_veces.setText(getItem(position).get(5)+ context.getApplicationContext().getResources().getString(R.string.inhalaciones));
        return convertView;
    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is content_crear_tratamiento power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    static class ViewHolder {

        ImageView imagenInhalador;
        TextView nombreInhalador;
        TextView nombreMedicamento;
        TextView idOculta;
        TextView hora;
        TextView numero_veces;
        TextView divider;
    }

}
