/***********************************************************************
 * This file is part of MyEPOC.
 * <p>
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.xmlDatabase;

import android.content.Context;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.R;

/**
 * Created by jmalvarez on 11/5/16.
 * http://developer.android.com/intl/es/training/basics/network-ops/xml.html
 */
public class DatabaseXMLParserOld {

    public static char FIELD_SEPARATOR = ';';

    // We don't use namespaces
    private static final String ns = null;

    public void parse(Context cxt, ArrayList<String> medicines,
                      ArrayList<String> activeIngredients,
                      ArrayList<String> inhalers,
                      ArrayList<String> physiotherapies,
                      ArrayList<String> posologies,
                      ArrayList<String> medPresentations,
                      ArrayList<String> userManualPhysioSteps,
                      ArrayList<String> rescueMedPresentations,
                      ArrayList<String> userManualSteps) throws XmlPullParserException, IOException {

        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream inStr = cxt.getResources().openRawResource(R.raw.databasecontent);
        parser.setInput(inStr, null);
        parser.nextTag();

//        XmlResourceParser parser = cxt.getResources().getXml(R.xml.databaseinitialcontent);
//        parser.next();

        parser.require(XmlPullParser.START_TAG, ns, "databaseElements");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            // Starts by looking for the entry tag
            switch (parser.getName()) {
                case "medicines":
                    readMedicines(parser, medicines);
                    break;
                case "activeIngredients":
                    readActiveIngredients(parser, activeIngredients);
                    break;
                case "physiotherapies":
                    readPhysiotherapies(parser, physiotherapies);
                    break;
                case "inhalers":
                    readInhalers(parser, inhalers);
                    break;
                case "posologies":
                    readPosologies(parser, posologies);
                    break;
                case "medicinePresentations":
                    readMedicinePresentations(parser, medPresentations);
                    break;
                case "rescueMedicinePresentations":
                    readRescueMedicinePresentations(parser, rescueMedPresentations);
                    break;
                case "userManualPhysioSteps":
                    readUserManualPhysioSteps(parser, userManualPhysioSteps);
                    break;
                case "userManualSteps":
                    readUserManualSteps(parser, userManualSteps);
                    break;
                default:
                    skip(parser);
            }
        }
        inStr.close();
    }

    public ArrayList<String> readMedicines(InputStream in) throws XmlPullParserException, IOException {

        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, null);
        parser.nextTag();

        ArrayList<String> medicines = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, ns, "databaseElements");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String medicinesTagName = parser.getName();
            // Starts by looking for the entry tag
            if (medicinesTagName.equals("medicines")) {
                parser.require(XmlPullParser.START_TAG, ns, "medicines");
                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    String medicineTagName = parser.getName();
                    // Starts by looking for the entry tag
                    switch (medicineTagName) {
                        case "medicine":
                            medicines.add(readMedicineEntry(parser));
                            break;
                        default:
                            skip(parser);
                    }
                }

            } else {
                skip(parser);
            }
        }

        return medicines;
    }

    public ArrayList<String> readActiveIngredients(InputStream in) throws XmlPullParserException, IOException {

        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, null);
        parser.nextTag();

        ArrayList<String> activeIngredients = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, ns, "databaseElements");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String activeIngredientsTagName = parser.getName();
            // Starts by looking for the entry tag
            if (activeIngredientsTagName.equals("activeIngredients")) {
                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    String activeIngredientTagName = parser.getName();
                    // Starts by looking for the entry tag
                    switch (activeIngredientTagName) {
                        case "activeIngredient":
                            activeIngredients.add(readActiveIngredientEntry(parser));
                            break;
                        default:
                            skip(parser);
                    }
                }

            } else {
                skip(parser);
            }
        }

        return activeIngredients;
    }

    public ArrayList<String> readInhalers(InputStream in) throws XmlPullParserException, IOException {

        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, null);
        parser.nextTag();

        ArrayList<String> inhalers = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, ns, "databaseElements");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String inhalersTagName = parser.getName();
            // Starts by looking for the entry tag
            if (inhalersTagName.equals("inhalers")) {
                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    String inhalerTagName = parser.getName();
                    // Starts by looking for the entry tag
                    switch (inhalerTagName) {
                        case "inhaler":
                            inhalers.add(readInhalerEntry(parser));
                            break;
                        default:
                            skip(parser);
                    }
                }

            } else {
                skip(parser);
            }
        }

        return inhalers;
    }

    public ArrayList<String> readPosologies(InputStream in) throws XmlPullParserException, IOException {

        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, null);
        parser.nextTag();

        ArrayList<String> posologies = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, ns, "databaseElements");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String posologiesTagName = parser.getName();
            // Starts by looking for the entry tag
            if (posologiesTagName.equals("posologies")) {
                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    String posologyTagName = parser.getName();
                    // Starts by looking for the entry tag
                    switch (posologyTagName) {
                        case "posology":
                            posologies.add(readPosologyEntry(parser));
                            break;
                        default:
                            skip(parser);
                    }
                }

            } else {
                skip(parser);
            }
        }

        return posologies;
    }

    public ArrayList<String> readMedicinePresentations(InputStream in) throws XmlPullParserException, IOException {

        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, null);
        parser.nextTag();

        ArrayList<String> medicinePresentation = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, ns, "databaseElements");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String medicinePresentationsTagName = parser.getName();
            // Starts by looking for the entry tag
            if (medicinePresentationsTagName.equals("medicinePresentations")) {
                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    String medicinePresentationTagName = parser.getName();
                    // Starts by looking for the entry tag
                    switch (medicinePresentationTagName) {
                        case "medicinePresentation":
                            medicinePresentation.addAll(readMedicinePresentationEntry(parser));
                            break;
                        default:
                            skip(parser);
                    }
                }

            } else {
                skip(parser);
            }
        }

        return medicinePresentation;
    }

    public ArrayList<String> readUserManualSteps(InputStream in) throws XmlPullParserException, IOException {

        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, null);
        parser.nextTag();

        ArrayList<String> userManualSteps = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, ns, "databaseElements");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String userManualStepsTagName = parser.getName();
            // Starts by looking for the entry tag
            if (userManualStepsTagName.equals("userManualSteps")) {
                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    String userManualStepTagName = parser.getName();
                    // Starts by looking for the entry tag
                    switch (userManualStepTagName) {
                        case "userManualStep":
                            userManualSteps.add(readUserManualStepEntry(parser));
                            break;
                        default:
                            skip(parser);
                    }
                }

            } else {
                skip(parser);
            }
        }

        return userManualSteps;
    }

    public void readMedicines(XmlPullParser parser, ArrayList<String> medicines)
            throws XmlPullParserException, IOException {

        parser.require(XmlPullParser.START_TAG, ns, "medicines");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String medicineTagName = parser.getName();
            // Starts by looking for the entry tag
            switch (medicineTagName) {
                case "medicine":
                    medicines.add(readMedicineEntry(parser));
                    break;
                default:
                    skip(parser);
            }
        }
    }

    public void readActiveIngredients(XmlPullParser parser, ArrayList<String> activeIngredients)
            throws XmlPullParserException, IOException {

        parser.require(XmlPullParser.START_TAG, ns, "activeIngredients");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String activeIngredientTagName = parser.getName();
            // Starts by looking for the entry tag
            switch (activeIngredientTagName) {
                case "activeIngredient":
                    activeIngredients.add(readActiveIngredientEntry(parser));
                    break;
                default:
                    skip(parser);
            }
        }
    }

    //crear en databasecontent physiotherpaies --> physiotherapy
    public void readPhysiotherapies(XmlPullParser parser, ArrayList<String> physiotherapies)
            throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "physiotherapies");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            switch (name) {
                case "physiotherapy":
                    physiotherapies.add(readPhysiotherapyEntry(parser));
                    break;
                default:
                    skip(parser);
            }
        }
    }

    public void readInhalers(XmlPullParser parser, ArrayList<String> inhalers)
            throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "inhalers");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            switch (name) {
                case "inhaler":
                    inhalers.add(readInhalerEntry(parser));
                    break;
                default:
                    skip(parser);
            }
        }
    }

    public void readPosologies(XmlPullParser parser, ArrayList<String> posologies) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "posologies");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            switch (name) {
                case "posology":
                    posologies.add(readPosologyEntry(parser));
                    break;
                default:
                    skip(parser);
            }
        }
    }

    public void readMedicinePresentations(XmlPullParser parser, ArrayList<String> medicinePresentation)
            throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "medicinePresentations");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String medicinePresentationTagName = parser.getName();
            // Starts by looking for the entry tag
            switch (medicinePresentationTagName) {
                case "medicinePresentation":
                    medicinePresentation.addAll(readMedicinePresentationEntry(parser));
                    break;
                default:
                    skip(parser);
            }
        }
    }

    public void readUserManualPhysioSteps(XmlPullParser parser, ArrayList<String> userManualPhysioSteps)
            throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "userManualPhysioSteps");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String userManualPhysioStepTagName = parser.getName();
            // Starts by looking for the entry tag
            switch (userManualPhysioStepTagName) {
                case "userManualPhysioStep":
                    userManualPhysioSteps.add(readUserManualPhysioStepEntry(parser));
                    break;
                default:
                    skip(parser);
            }
        }
    }

    public void readRescueMedicinePresentations(XmlPullParser parser, ArrayList<String> rescueMedicinePresentation)
            throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "rescueMedicinePresentations");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String medicinePresentationTagName = parser.getName();
            // Starts by looking for the entry tag
            switch (medicinePresentationTagName) {
                case "rescueMedicinePresentation":
                    rescueMedicinePresentation.add(readRescueMedicinePresentationEntry(parser));
                    break;
                default:
                    skip(parser);
            }
        }
    }

    public void readUserManualSteps(XmlPullParser parser, ArrayList<String> userManualSteps)
            throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "userManualSteps");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String userManualStepTagName = parser.getName();
            // Starts by looking for the entry tag
            switch (userManualStepTagName) {
                case "userManualStep":
                    userManualSteps.add(readUserManualStepEntry(parser));
                    break;
                default:
                    skip(parser);
            }
        }
    }

    // Parses the contents of a medicine. If it encounters a name tag, hands them off
// to the "read" method for processing. Otherwise, skips the tag.
    private String readMedicineEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "medicine");
        String medName = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case "name":
                    medName = readTextField(parser, "name");
                    break;
                default:
                    skip(parser);
            }
        }
        return medName;
    }

    private String readActiveIngredientEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "activeIngredient");
        String aiName = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case "name":
                    aiName = readTextField(parser, "name");
                    break;
                default:
                    skip(parser);
            }
        }
        return aiName;
    }

    private String readPosologyEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "posology");
        String posName = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case "description":
                    posName = readTextField(parser, "description");
                    break;
                default:
                    skip(parser);
            }
        }
        return posName;
    }

    private String readPhysiotherapyEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "physiotherapy");
        String phyName = null;
        String image = null;
        String link = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case "name":
                    phyName = readTextField(parser, "name");
                    break;
                case "image":
                    image = readTextField(parser, "image");
                    break;
                default:
                    skip(parser);
            }
        }
        return phyName + FIELD_SEPARATOR + image;
    }

    private String readInhalerEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "inhaler");
        String inhName = null;
        String image = null;
        String link = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case "name":
                    inhName = readTextField(parser, "name");
                    break;
                case "image":
                    image = readTextField(parser, "image");
                    break;
                default:
                    skip(parser);
            }
        }
        return inhName + FIELD_SEPARATOR + image;
    }

    private ArrayList<String> readMedicinePresentationEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "medicinePresentation");
        ArrayList<String> medPresentations = new ArrayList<>();
        String medRef = null;
        String aiRef = null;
        ArrayList<String> inhRef = new ArrayList<>();
        ArrayList<String> dose = new ArrayList<>();
        ArrayList<String> posRef = new ArrayList<>();
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case "medicineRef":
                    medRef = readTextField(parser, "medicineRef");
                    break;
                case "activeIngRef":
                    aiRef = readTextField(parser, "activeIngRef");
                    break;
                case "inhalerRef":
                    inhRef.add(readTextField(parser, "inhalerRef"));
                    break;
                case "dose":
                    dose.add(readTextField(parser, "dose"));
                    break;
                case "posologyRef":
                    posRef.add(readTextField(parser, "posologyRef"));
                    break;
                default:
                    skip(parser);
            }
        }
        for (String iRef : inhRef){
            for (String dRef : dose){
                for (String pRef : posRef){
                    medPresentations.add(medRef + FIELD_SEPARATOR + aiRef + FIELD_SEPARATOR +
                            iRef + FIELD_SEPARATOR + dRef + FIELD_SEPARATOR + pRef);
                }
            }
        }

        return medPresentations;
    }

    private String readRescueMedicinePresentationEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "rescueMedicinePresentation");
        String medRef = null;
        String aiRef = null;
        String inhRef = null;
        String dose = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case "medicineRef":
                    medRef = readTextField(parser, "medicineRef");
                    break;
                case "activeIngRef":
                    aiRef = readTextField(parser, "activeIngRef");
                    break;
                case "inhalerRef":
                    inhRef = readTextField(parser, "inhalerRef");
                    break;
                case "dose":
                    dose = readTextField(parser, "dose");
                    break;
                default:
                    skip(parser);
            }

        }
        return medRef + FIELD_SEPARATOR + aiRef + FIELD_SEPARATOR +
                inhRef + FIELD_SEPARATOR + dose;
    }

    private String readUserManualPhysioStepEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "userManualPhysioStep");
        String stepTitle = null;
        String stepImage = null;
        String stepText = null;
        String physioRef = null;
        String posRef = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case "stepTitle":
                    stepTitle = readTextField(parser, "stepTitle");
                    break;
                case "stepImage":
                    stepImage = readTextField(parser, "stepImage");
                    break;
                case "stepText":
                    stepText = readTextField(parser, "stepText");
                    break;
                case "physioRef":
                    physioRef = readTextField(parser, "physioRef");
                    break;
                default:
                    skip(parser);
            }
        }
        return stepTitle + FIELD_SEPARATOR + stepImage + FIELD_SEPARATOR +
                stepText + FIELD_SEPARATOR + physioRef;
    }

    private String readUserManualStepEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "userManualStep");
        String stepTitle = null;
        String stepImage = null;
        String stepText = null;
        String inhalerRef = null;
        String posRef = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case "stepTitle":
                    stepTitle = readTextField(parser, "stepTitle");
                    break;
                case "stepImage":
                    stepImage = readTextField(parser, "stepImage");
                    break;
                case "stepText":
                    stepText = readTextField(parser, "stepText");
                    break;
                case "inhalerRef":
                    inhalerRef = readTextField(parser, "inhalerRef");
                    break;
                default:
                    skip(parser);
            }
        }
        return stepTitle + FIELD_SEPARATOR + stepImage + FIELD_SEPARATOR +
                stepText + FIELD_SEPARATOR + inhalerRef;
    }

    // Processes title tags in the feed.
    private String readTextField(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, tag);
        String name = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, tag);
        return name;
    }

    // For the tags title and summary, extracts their text values.
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}
