package es.uma.ingenieriasalud.retoepoc.xmlDatabase;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.persistence.DatabaseInserter;

/**
 * Created by jmalvarez on 8/6/16.
 */
public class XmlUserManualInhParser extends XmlEntryParser {

    private static final String ELEMENTS_TAGNAME = "userManualSteps";
    private static final String SINGLE_ELEMENT_TAGNAME = "userManualStep";

    private static final String TITLE_FIELD_TAGNAME = "stepTitle";
    private static final String IMAGE_FIELD_TAGNAME = "stepImage";
    private static final String TEXT_FIELD_TAGNAME = "stepText";
    private static final String INHREF_FIELD_TAGNAME = "inhalerRef";

    public XmlUserManualInhParser(DatabaseInserter dbIns, XmlEntryParser n) {
        super(dbIns, n);
        elementsTagName = ELEMENTS_TAGNAME;
    }

    @Override
    void parseElement(XmlPullParser parser, ArrayList<String> elementsList) throws XmlPullParserException, IOException {
        String userManualStepTagName = parser.getName();
        // Starts by looking for the entry tag
        switch (userManualStepTagName) {
            case SINGLE_ELEMENT_TAGNAME:
                elementsList.add(readUserManualStepEntry(parser));
                break;
            default:
                skip(parser);
        }
    }

    private String readUserManualStepEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, SINGLE_ELEMENT_TAGNAME);
        String stepTitle = null;
        String stepImage = null;
        String stepText = null;
        String inhalerRef = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case TITLE_FIELD_TAGNAME:
                    stepTitle = readTextField(parser, TITLE_FIELD_TAGNAME);
                    break;
                case IMAGE_FIELD_TAGNAME:
                    stepImage = readTextField(parser, IMAGE_FIELD_TAGNAME);
                    break;
                case TEXT_FIELD_TAGNAME:
                    stepText = readTextField(parser, TEXT_FIELD_TAGNAME);
                    break;
                case INHREF_FIELD_TAGNAME:
                    inhalerRef = readTextField(parser, INHREF_FIELD_TAGNAME);
                    break;
                default:
                    skip(parser);
            }
        }
        return stepTitle + FIELD_SEPARATOR + stepImage + FIELD_SEPARATOR +
                stepText + FIELD_SEPARATOR + inhalerRef;
    }
}
