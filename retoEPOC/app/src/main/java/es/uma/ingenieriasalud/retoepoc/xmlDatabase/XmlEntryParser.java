/**
 * Implementation of chain-of-responsibility design pattern.
 */

package es.uma.ingenieriasalud.retoepoc.xmlDatabase;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.persistence.DatabaseInserter;

public abstract class XmlEntryParser {
    // We don't use namespaces
    protected static final String ns = null;
    public static char FIELD_SEPARATOR = ';';

    XmlEntryParser next;
    String elementsTagName;
    DatabaseInserter inserter;

    public XmlEntryParser(DatabaseInserter dbIns, XmlEntryParser nextEntryParser){
        inserter = dbIns;
        next = nextEntryParser;
    }

    void parse(XmlPullParser parser) throws XmlPullParserException, IOException{
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new XmlPullParserException("expected " +
                    XmlPullParser.TYPES[XmlPullParser.START_TAG] +
                    parser.getPositionDescription());
        } else {
            if (parser.getName().equals(elementsTagName)) {
                ArrayList<String> elementsList = new ArrayList<>();
                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    parseElement(parser, elementsList);
                }
                System.out.println(elementsList);
                inserter.insertData(elementsList);
            } else {
                if (next != null) {
                    next.parse(parser);
                }
            }
        }
    }

    abstract void parseElement(XmlPullParser parser, ArrayList<String> elementsList)
            throws XmlPullParserException, IOException;

    // Processes title tags in the feed.
    protected String readTextField(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, tag);
        String name = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, tag);
        return name;
    }

    // For the tags title and summary, extracts their text values.
    protected String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    protected void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }


}
