/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.persistence;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.HasGetNameText;

public class Inhalador implements HasGetNameText {

// Unique id in DB
    private int id;
// Name id as stored in strings.xml as id
    private String nombre;
// Name as stored in strings.xml as value
    private String nombreTexto;
// Image file id stored in strings.xml
    private String imagen;
// List of user manual steps
    private ArrayList<Paso> listaPasosManual;

    public Inhalador(int id_, String nombre_, String texto, String imagen_, ArrayList<Paso> listaPasosManual_){
        this.id = id_;
        this.nombre=nombre_;
        this.nombreTexto = texto;
        this.imagen=imagen_;
        this.listaPasosManual = listaPasosManual_;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNameText() {
        return nombreTexto;
    }

    public void setNameText(String nombreTexto) {
        this.nombreTexto = nombreTexto;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public ArrayList<Paso> getListaPasosManual() {
        return listaPasosManual;
    }

    public void setListaPasosManual(ArrayList<Paso> listaPasosManual) {
        this.listaPasosManual = listaPasosManual;
    }

}
