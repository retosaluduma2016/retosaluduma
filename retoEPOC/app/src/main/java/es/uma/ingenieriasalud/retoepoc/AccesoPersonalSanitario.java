/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import es.uma.ingenieriasalud.retoepoc.createTreatment.CrearTratamiento;
import es.uma.ingenieriasalud.retoepoc.personalData.ConsultaEdicionDatos;

public class AccesoPersonalSanitario extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_acceso_personal_sanitario);
    }

    //Accion del boton Crear tratamiento de la ventana principal.
    public void onClickBotonCrearTratamiento(View view){
        Intent intent = new Intent(this, CrearTratamiento.class);
        startActivity(intent);
    }

    public void onClickConsultaEdicionDatos(View view){
        Intent intent = new Intent(this, ConsultaEdicionDatos.class);
        startActivity(intent);
    }
}
