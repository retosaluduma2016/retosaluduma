/***********************************************************************
 * This file is part of MyEPOC.
 * <p>
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.createTreatment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.persistence.Dosis;
import es.uma.ingenieriasalud.retoepoc.persistence.Hour;
import es.uma.ingenieriasalud.retoepoc.persistence.Inhalador;
import es.uma.ingenieriasalud.retoepoc.persistence.Medicamento;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.persistence.PrincipioActivo;
import es.uma.ingenieriasalud.retoepoc.persistence.Toma;
import es.uma.ingenieriasalud.retoepoc.R;

public class AddNewTake extends AppCompatActivity {

    static final private short MEDICINE_SPINNER_SELECTED = 0;
    static final private short ACTIVE_INGREDIENT_SPINNER_SELECTED = 1;
    static final private short INHALER_SPINNER_SELECTED = 2;
    static final private short DOSE_SPINNER_SELECTED = 3;
    static final private short INHALATIONS_SPINNER_SELECTED = 4;


    private Modelo modelo;

    private Spinner spinnerMedicamento, spinnerInhaladores, spinnerPrincipio, spinnerDosis, spinnerInhalaciones;

    private ArrayList<Medicamento> listaMedicamentos;
    private ArrayList<PrincipioActivo> listaPrincipiosActivos = new ArrayList<>();
    private ArrayList<Inhalador> listaInhaladores = new ArrayList<>();
    private ArrayList<Dosis> listaDosis = new ArrayList<>();

    private ArrayList<String> listaNameMedicamentosSpinner = new ArrayList<>();
    private ArrayList<String> listaNamePrincipiosActivosSpinner = new ArrayList<>();
    private ArrayList<String> listaNombreInhaladoresSpinner = new ArrayList<>();
    private ArrayList<String> listaNameDosisSpinner = new ArrayList<>();
    private ArrayList<String> listaInhalacionesSpinner = new ArrayList<>();


    private String selecMedicamento, selecPrincipio, selecDosis, seleccInhalador, seleccInhalaciones;

    private String medicamentoSeleccionado, inhaladorSeleccionado, principioSeleccionado, dosisSeleccionada, inhalacionesSeleccionada;

    private TextView tVPrincipio, tVdosis, tVinhalador, tVinhalaciones, tVhora;

    private Button botonHora;

    private ArrayAdapter<String> adaptadorMedicamento, adaptadorPrincipioActivo, adaptadorInhalador,
            adaptadorDosis, adaptadorInhalaciones;

    private ArrayList<Toma> listaTomasNuevas;


    private static ListView listViewHoras;
    private static ArrayList<String> listaHoras;
    private static AdapterListViewHoras adapterHoras;

    // private ArrayList<Toma> listaTomas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_add_new_take);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        listaTomasNuevas = getIntent().getParcelableArrayListExtra("listaTomas");

        //Inicializar base de datos
        modelo = Modelo.getInstance();
        modelo.init(this);

        tVPrincipio = (TextView) findViewById(R.id.textViewPrincipio);
        tVdosis = (TextView) findViewById(R.id.textViewDosis);
        tVinhalador = (TextView) findViewById(R.id.textViewInhaladores);
        tVinhalaciones = (TextView) findViewById(R.id.textViewInhalaciones);
        tVhora = (TextView) findViewById(R.id.textViewHora);
        botonHora = (Button) findViewById(R.id.buttonAddHour);

        listViewHoras = (ListView) findViewById(R.id.listViewHoras);
        listaHoras = new ArrayList();

        adapterHoras = new AdapterListViewHoras(this, listaHoras, listViewHoras);
        listViewHoras.setAdapter(adapterHoras);
        Commodities.ajustarVistaListView(listViewHoras);


        spinnerMedicamento = (Spinner) findViewById(R.id.spinnerMedicamento);
        spinnerPrincipio = (Spinner) findViewById(R.id.spinnerPrincipio);
        spinnerDosis = (Spinner) findViewById(R.id.spinnerDosis);
        spinnerInhaladores = (Spinner) findViewById(R.id.spinnerInhaladores);
        spinnerInhalaciones = (Spinner) findViewById(R.id.spinnerInhalaciones);

        selecMedicamento = "<" + getResources().getString(R.string.noSeleccMedicamento) + ">";
        selecPrincipio = "<" + getResources().getString(R.string.noSeleccPrincipio) + ">";
        selecDosis = "<" + getResources().getString(R.string.noSeleccDosis) + ">";
        seleccInhalador = "<" + getResources().getString(R.string.noSeleccInhalador) + ">";
        seleccInhalaciones = "<" + getResources().getString(R.string.noSeleccInhalaciones) + ">";


        medicamentoSeleccionado = selecMedicamento;
        principioSeleccionado = selecPrincipio;
        dosisSeleccionada = selecDosis;
        inhaladorSeleccionado = seleccInhalador;
        inhalacionesSeleccionada = seleccInhalaciones;

        //Obtener lista MEDICAMENTOS de la BD----------------------------
        listaNameMedicamentosSpinner.add(selecMedicamento);
        listaMedicamentos = modelo.obtenerListaMedicamentos();
        Collections.sort(listaMedicamentos);
        for (Medicamento md : listaMedicamentos) {
            listaNameMedicamentosSpinner.add(md.getNameText());
        }
        adaptadorMedicamento = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, listaNameMedicamentosSpinner);
        spinnerMedicamento.setAdapter(adaptadorMedicamento);

        inicializarSpinner(spinnerMedicamento, listaNameMedicamentosSpinner, AddNewTake.MEDICINE_SPINNER_SELECTED);

        adaptadorPrincipioActivo = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, listaNamePrincipiosActivosSpinner);
        spinnerPrincipio.setAdapter(adaptadorPrincipioActivo);
        inicializarSpinner(spinnerPrincipio, listaNamePrincipiosActivosSpinner, AddNewTake.ACTIVE_INGREDIENT_SPINNER_SELECTED);

        adaptadorInhalador = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, listaNombreInhaladoresSpinner);
        spinnerInhaladores.setAdapter(adaptadorInhalador);
        inicializarSpinner(spinnerInhaladores, listaNombreInhaladoresSpinner, AddNewTake.INHALER_SPINNER_SELECTED);

        adaptadorDosis = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, listaNameDosisSpinner);
        spinnerDosis.setAdapter(adaptadorDosis);
        inicializarSpinner(spinnerDosis, listaNameDosisSpinner, AddNewTake.DOSE_SPINNER_SELECTED);

        listaInhalacionesSpinner.add(seleccInhalaciones);
        listaInhalacionesSpinner.add("1");
        listaInhalacionesSpinner.add("2");
        listaInhalacionesSpinner.add("3");
        listaInhalacionesSpinner.add("4");

        adaptadorInhalaciones = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, listaInhalacionesSpinner);
        spinnerInhalaciones.setAdapter(adaptadorInhalaciones);
        inicializarSpinner(spinnerInhalaciones, listaInhalacionesSpinner, AddNewTake.INHALATIONS_SPINNER_SELECTED);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void inicializarSpinner(final Spinner s, final ArrayList<String> lista, final short selec) {

        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (selec == AddNewTake.MEDICINE_SPINNER_SELECTED) {
                    medicamentoSeleccionado = lista.get(position);
                    if (medicamentoSeleccionado.equals(selecMedicamento)) {

                        tVPrincipio.setVisibility(View.GONE);
                        spinnerPrincipio.setVisibility(View.GONE);
                        tVinhalador.setVisibility(View.GONE);
                        spinnerInhaladores.setVisibility(View.GONE);
                        tVdosis.setVisibility(View.GONE);
                        spinnerDosis.setVisibility(View.GONE);
                        tVinhalaciones.setVisibility(View.GONE);
                        tVhora.setVisibility(View.GONE);
                        spinnerInhalaciones.setVisibility(View.GONE);
                        botonHora.setVisibility(View.GONE);


                    } else {
                        Medicamento selectedMedicine = Commodities.getElementByName(listaMedicamentos, medicamentoSeleccionado);
                        //Obtener lista PRINCIPIOS de la BD----------------------------
                        listaPrincipiosActivos.clear();
                        listaPrincipiosActivos = modelo.obtenerListaPrincipiosActivosByMedicine(selectedMedicine.getName());
                        listaNamePrincipiosActivosSpinner.clear();
                        if (1 < listaPrincipiosActivos.size()) {
                            listaNamePrincipiosActivosSpinner.add(selecPrincipio);
                        }
                        for (PrincipioActivo prinAct : listaPrincipiosActivos) {
                            listaNamePrincipiosActivosSpinner.add(prinAct.getNameText());
                        }
                        adaptadorPrincipioActivo.notifyDataSetChanged();
                        principioSeleccionado = listaNamePrincipiosActivosSpinner.get(0);
                        tVPrincipio.setVisibility(View.VISIBLE);
                        spinnerPrincipio.setVisibility(View.VISIBLE);
                    }
                    refreshDependableSpinners();

                } else if (selec == AddNewTake.ACTIVE_INGREDIENT_SPINNER_SELECTED) {
                    principioSeleccionado = lista.get(position);
                    refreshDependableSpinners();
                } else if (selec == AddNewTake.INHALER_SPINNER_SELECTED) {
                    inhaladorSeleccionado = lista.get(position);
                } else if (selec == AddNewTake.DOSE_SPINNER_SELECTED) {
                    dosisSeleccionada = lista.get(position);
                } else if (selec == AddNewTake.INHALATIONS_SPINNER_SELECTED) {
                    inhalacionesSeleccionada = lista.get(position);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return alertExitTreatment();
        }
        //para las demas cosas, se reenvia el evento al listener habitual
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                return alertExitTreatment();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private boolean alertExitTreatment() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.alerDialogTitulo)
                .setMessage(R.string.alertDialogSalirSinGuardar)
                .setNegativeButton(R.string.alertDialogBotonNoSalir, null)//sin listener
                .setPositiveButton(R.string.alertDialogBotonSalir, new DialogInterface.OnClickListener() {//un listener que al pulsar, cierre la aplicacion
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Salir
                        AddNewTake.this.finish();
                    }
                })
                .show();
        // Si el listener devuelve true, significa que el evento esta procesado, y nadie debe hacer nada mas
        return true;
    }

    public void onClickVolver(View view) {
        this.onBackPressed();
    }

    public void onClickAddHora(View view) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        @NonNull
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            String hora = "";
            if (hourOfDay < 10) {
                hora = "0" + String.valueOf(hourOfDay) + ":" + String.format(Locale.getDefault(), "%02d", minute);

            } else {
                hora = String.valueOf(hourOfDay) + ":" + String.format(Locale.getDefault(), "%02d", minute);

            }
            if(listaHoras.contains(hora)){
                Toast pulsarAdd = Toast.makeText(getContext(), R.string.toastAddTratamientoHoraRepetida, Toast.LENGTH_SHORT);
                pulsarAdd.show();
            }else{
                listaHoras.add(hora);
                adapterHoras.notifyDataSetChanged();
                Commodities.ajustarVistaListView(listViewHoras);
            }

        }

    }




    //Accion del boton Añadir de crear tratamiento.
    public void onClickBotonAdd(View view) {

        //Comprobar si se han introducido el medicamento
        if (medicamentoSeleccionado.equals(selecMedicamento)) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
            alertDialogBuilder
                    .setMessage(R.string.alertDialogNoMedicamentoNoPrincipio)
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else {

            //Comprobar que no hay principio activo repetido
            boolean repetido = false;
            if (!listaTomasNuevas.isEmpty()) {
// repeated if same medicine at same hour:min or if different medicines with the same
// active ingredient or different medicines where one of the medicines has two active
// ingredients and the active ingredient of the other medicine is one of those two
// active ingredients.


                for (int i = 0; !repetido && i < listaTomasNuevas.size(); i++) {
                    Toma tm = listaTomasNuevas.get(i);
                    Medicamento med = modelo.getMedicineByName(tm.getMedicamento());
                    if (med.getNameText().equals(medicamentoSeleccionado)) {
                        // TextView tvh = (TextView) findViewById(R.id.textViewHour);
                        //TextView tvm = (TextView) findViewById(R.id.textViewMinutes);
                        //int selectedHour = Integer.parseInt(tvh.getText().toString());
                        //int selectedMinute = Integer.parseInt(tvm.getText().toString());
                        //repetido = (tm.getHour().getHour() == selectedHour) &&
                        //           (tm.getHour().getMin() == selectedMinute);

                        for (int j = 0; !repetido && j < listaHoras.size(); j++) {
                            repetido = (tm.getHour().getHour() == Integer.valueOf(listaHoras.get(j).substring(0, 2)) &&
                                    (tm.getHour().getMin() ==
                                    Integer.valueOf(listaHoras.get(j).substring(listaHoras.get(j).length() - 2, listaHoras.get(j).length()))));
                        }
                    } else {
                        PrincipioActivo pri =
                                modelo.getActiveIngredientByName(tm.getPrincipio());
                        repetido = pri.getNameText().contains(this.principioSeleccionado) ||
                                this.principioSeleccionado.contains(pri.getNameText());
                    }
                }
            }
            if (!repetido) {
                //Comprobar que se han introducido los campos correctamente
                if (dosisSeleccionada.equals(selecDosis) ||
                        inhaladorSeleccionado.equals(seleccInhalador) ||
                        principioSeleccionado.equals(selecPrincipio) ||
                        inhalacionesSeleccionada.equals(seleccInhalaciones) ||
                        listaHoras.isEmpty()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
                    alertDialogBuilder
                            .setMessage(R.string.alertDialogConfiguracionTomaMala)
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    Medicamento m = Commodities.getElementByName(listaMedicamentos, medicamentoSeleccionado);
                    PrincipioActivo p = Commodities.getElementByName(listaPrincipiosActivos, principioSeleccionado);
                    Inhalador in = Commodities.getElementByName(listaInhaladores, inhaladorSeleccionado);

                    String cantidad1 = null;
                    String cantidad2 = null;
                    if (dosisSeleccionada.contains(" / ")) {
                        StringTokenizer st = new StringTokenizer(dosisSeleccionada, " /");
                        if (st.hasMoreTokens()) {
                            cantidad1 = st.nextToken();
                            cantidad2 = st.nextToken();
                        }
                    } else {
                        cantidad1 = dosisSeleccionada;
                        cantidad2 = "0";
                    }
                    Dosis d = new Dosis(Double.parseDouble(cantidad1),
                            Double.parseDouble(cantidad2));
                    //String hora = tVhour.getText().toString();
                    //String min = tVmin.getText().toString();
                    //Hour h = new Hour (Integer.parseInt(hora),Integer.parseInt(min));

                    int numInhalaciones = Integer.parseInt(inhalacionesSeleccionada);

                    ArrayList<Toma> listaTomas = new ArrayList<>();

                    for (String hora : listaHoras) {
                        Hour h = new Hour(Integer.parseInt(hora.substring(0, 2)), Integer.parseInt(hora.substring(hora.length() - 2, hora.length())));
                        Toma t = new Toma(-1, m.getName(), p.getName(), in.getName(), d, h, false, numInhalaciones);
                        listaTomas.add(t);
                    }

                    //Añadir content_crear_tratamiento la lista
                    // Toma t = new Toma(-1, m.getName(), p.getName(), in.getName(), d, h,false,numInhalaciones);

//                    c.add(t);

                    spinnerMedicamento.setSelection(0);
                    spinnerPrincipio.setSelection(0);
                    spinnerDosis.setSelection(0);
                    spinnerInhaladores.setSelection(0);
                    spinnerInhalaciones.setSelection(0);
                    Toast pulsarAdd = Toast.makeText(this, R.string.toastAddTratamiento, Toast.LENGTH_SHORT);
                    pulsarAdd.show();

                    Intent newTakes = new Intent();
                    newTakes.putParcelableArrayListExtra("newTake", listaTomas);
                    setResult(Activity.RESULT_OK, newTakes);

                    this.onBackPressed();

                }

            } else {
                Toast pulsarAdd = Toast.makeText(this, R.string.toastAddTratamientoExiste, Toast.LENGTH_SHORT);
                pulsarAdd.show();
            }
        }
    }

    private void refreshDependableSpinners() {
        if (medicamentoSeleccionado.equals(selecMedicamento) || principioSeleccionado.equals(selecPrincipio)) {
            spinnerDosis.setVisibility(View.GONE);
            spinnerInhaladores.setVisibility(View.GONE);
            tVdosis.setVisibility(View.GONE);
            tVinhalador.setVisibility(View.GONE);
            tVinhalaciones.setVisibility(View.GONE);
            tVhora.setVisibility(View.GONE);
            spinnerInhalaciones.setVisibility(View.GONE);
            botonHora.setVisibility(View.GONE);

        } else {

            Medicamento selectedMedicine = Commodities.getElementByName(listaMedicamentos, medicamentoSeleccionado);
            //Obtener lista INHALADORES de la BD----------------------------
            listaInhaladores.clear();
            listaInhaladores = modelo.obtenerListaInhaladoresByMedicine(selectedMedicine.getName());
            // actualizar listaNombreInhaladoresSpinner
            listaNombreInhaladoresSpinner.clear();
            if (1 < listaInhaladores.size()) {
                listaNombreInhaladoresSpinner.add(seleccInhalador);
            }
            for (Inhalador inh : listaInhaladores) {
                listaNombreInhaladoresSpinner.add(inh.getNameText());
            }
            adaptadorInhalador.notifyDataSetChanged();
            inhaladorSeleccionado = listaNombreInhaladoresSpinner.get(0);

            //Obtener lista DOSIS de la BD----------------------------
            listaDosis.clear();
            listaDosis = modelo.obtenerListaDosisByMedicine(selectedMedicine.getName());
            listaNameDosisSpinner.clear();
            if (1 < listaDosis.size()) {
                listaNameDosisSpinner.add(selecDosis);
            }
            for (Dosis ds : listaDosis) {
                listaNameDosisSpinner.add(ds.toString());
            }
            adaptadorDosis.notifyDataSetChanged();
            dosisSeleccionada = listaNameDosisSpinner.get(0);

            spinnerDosis.setVisibility(View.VISIBLE);
            spinnerInhaladores.setVisibility(View.VISIBLE);
            tVdosis.setVisibility(View.VISIBLE);
            tVinhalador.setVisibility(View.VISIBLE);
            tVinhalaciones.setVisibility(View.VISIBLE);
            tVhora.setVisibility(View.VISIBLE);
            spinnerInhalaciones.setVisibility(View.VISIBLE);
            botonHora.setVisibility(View.VISIBLE);

        }
    }

}
