/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.persistence;

import android.os.Parcel;
import android.os.Parcelable;

public class Toma implements Parcelable{

    public static final String TABLE_NAME = "Take";
    public static final String DOSIS = "dose_id";
    public static final String PRINCIPIO = "active_Ingredient_id";
    public static final String _ID = "_id";
    public static final String INHALADOR = "inhaler_id";
    public static final String TRATAMIENTO = "treatment_id";
    public static final String MEDICAMENTO = "medicine_id";
    public static final String HOUR = "hour_id";
    public static final String INHALATIONS = "inhalation_id";



    private int id;
    String medicamento, principio, inhalador;
    private Dosis dosis;
    private Hour hour;
    private boolean done;
    private int inhalations;



    public Toma(int id, String medicamento, String principio, String inhalador,
                Dosis dosis ,Hour hour, boolean done, int inhalations) {

        this.id = id;
        this.medicamento = medicamento;
        this.principio = principio;
        this.inhalador = inhalador;
        this.dosis = dosis;
        this.hour = hour;
        this.done = done;
        this.inhalations = inhalations;

    }

    protected Toma(Parcel in) {
        id = in.readInt();
        medicamento = in.readString();
        principio = in.readString();
        inhalador = in.readString();
        dosis = in.readParcelable(Dosis.class.getClassLoader());
        hour = in.readParcelable(Hour.class.getClassLoader());
        done = in.readByte() != 0;
        inhalations = in.readInt();
    }

    public static final Creator<Toma> CREATOR = new Creator<Toma>() {
        @Override
        public Toma createFromParcel(Parcel in) {
            return new Toma(in);
        }

        @Override
        public Toma[] newArray(int size) {
            return new Toma[size];
        }
    };

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public String getMedicamento() {
        return medicamento;
    }

    public void setMedicamento(String medicamento) {
        this.medicamento = medicamento;
    }

    public String getPrincipio() {
        return principio;
    }

    public void setPrincipio(String principio) {
        this.principio = principio;
    }

    public String getInhalador() {
        return inhalador;
    }

    public void setInhalador(String inhalador) {
        this.inhalador = inhalador;
    }

    public Dosis getDosis() {
        return dosis;
    }

    public void setDosis(Dosis dosis) {
        this.dosis = dosis;
    }

    public Hour getHour() {
        return hour;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public int getInhalations() {
        return inhalations;
    }

    public void setInhalations(int inhalations) {
        this.inhalations = inhalations;
    }

    public void setHour(Hour hour) {
        this.hour = hour;
    }

    @Override
    public String toString() {
        // id, dosis, principio, posologia, inhalador,medicamento;
        return id + ", " + dosis.toString() + ", " + principio + ", " +
               inhalador + ", " + medicamento+ ", " + hour.toString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(medicamento);
        dest.writeString(principio);
        dest.writeString(inhalador);
        dest.writeParcelable(dosis, flags);
        dest.writeParcelable(hour, flags);
        dest.writeByte((byte) (done ? 1 : 0));
        dest.writeInt(inhalations);
    }
}
