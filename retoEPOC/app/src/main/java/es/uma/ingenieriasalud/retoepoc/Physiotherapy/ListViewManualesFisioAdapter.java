/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.physiotherapy;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.persistence.Fisioterapia;
import es.uma.ingenieriasalud.retoepoc.R;

public class ListViewManualesFisioAdapter extends ArrayAdapter<Fisioterapia> {

    private Context context;


    public ListViewManualesFisioAdapter(Context context, ArrayList fisioterapias) {

        //nos vale el mismo listview_row --> imagen + text
        super(context, R.layout.listview_row_manual_inhaladores, R.id.imagenListView, fisioterapias);
        this.context = context;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView==null){

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_row_manual_inhaladores, parent, false);

            holder=new ViewHolder();
            holder.nombreFisioterapia = (TextView)convertView.findViewById(R.id.textNombreInhaladorListView);
            holder.imagenFisioterapia=(ImageView)convertView.findViewById(R.id.imagenListView);
            convertView.setTag(holder);

        }else{

            holder = (ViewHolder) convertView.getTag();

        }

        Fisioterapia fisio = getItem(position);

        if(fisio != null) {
            holder.imagenFisioterapia.setImageBitmap(
                    decodeSampledBitmapFromResource(context.getResources(),
                            context.getResources().getIdentifier(fisio.getImagen(), "drawable", context.getPackageName()),
                            172, 204));

            holder.nombreFisioterapia.setText(fisio.getNameText());

        }

        return convertView;


    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is content_crear_tratamiento power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    static class ViewHolder {

        ImageView imagenFisioterapia;
        TextView nombreFisioterapia;
    }


}
