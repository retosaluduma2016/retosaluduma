package es.uma.ingenieriasalud.retoepoc.persistence;

import android.content.ContentValues;

import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by jmalvarez on 8/6/16.
 */
public class PosologiesInserter extends DatabaseInserter {
    public PosologiesInserter(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public void insertData(ArrayList<String> elementsList) {
        for (String posologyName : elementsList) {
            int err;
            ContentValues values = new ContentValues();
            values.put(ModeloContract.PosologyTable.TEXT_FREQUENCY_XML, posologyName);
            long rowid = database.insert(ModeloContract.PosologyTable.TABLE_NAME, null, values);
            if (-1 == rowid) {
                err = 1;
            } else {
                err = 0;
            }
        }

    }
}
