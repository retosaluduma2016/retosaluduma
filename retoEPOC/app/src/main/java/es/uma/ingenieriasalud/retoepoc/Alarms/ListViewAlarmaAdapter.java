/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.alarms;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.persistence.Inhalador;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.persistence.Toma;
import es.uma.ingenieriasalud.retoepoc.R;

public class ListViewAlarmaAdapter extends ArrayAdapter<Toma> {

    private Context context;
    private Modelo modelo;


    public ListViewAlarmaAdapter(Modelo modelo_, Context context, ArrayList<Toma> listaTomas_) {
        super(context, R.layout.activity_consultar_tomas_alarmas, listaTomas_);
        this.context = context;
        this.modelo = modelo_;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView==null){

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_row_tomas_alarma, parent, false);

            holder=new ViewHolder();
            holder.nombreMedicina = (TextView)convertView.findViewById(R.id.row_alarm_textView_MedName);
            holder.idOculta = (TextView)convertView.findViewById(R.id.hiddenIdTake);
            holder.numero_veces = (TextView)convertView.findViewById(R.id.row_alarm_textView_NumInh);
            holder.checkHecho = (CheckBox)convertView.findViewById(R.id.row_alarm_checkDone);
            holder.imagenInhalador=(ImageView)convertView.findViewById(R.id.row_alarm_inhaler_image);
            holder.textViewManualInhalador = (TextView)convertView.findViewById(R.id.row_alarm_inhaler_manual);

            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        final Toma t = getItem(position);

        if(t != null) {

            Inhalador inh = modelo.getInhalerByName(t.getInhalador());

            holder.nombreMedicina.setText(Commodities.retrieveResourceStringFromId(context, t.getMedicamento()));
            int inhlsNumber = t.getInhalations();
            holder.numero_veces.setText(context.getApplicationContext().getResources().getQuantityString(R.plurals.numberOfInhalations, inhlsNumber, inhlsNumber));
            holder.imagenInhalador.setImageBitmap(
                    decodeSampledBitmapFromResource(context.getResources(),
                            context.getResources().getIdentifier(inh.getImagen(), "drawable", context.getPackageName()),
                            100, 100));

//            StringBuilder manualButtonLabel =
//                    new StringBuilder(context.getResources().getString(R.string.manual)).append(" ").
//                                      append(modelo.getMedicineByName(t.getMedicamento()).getNameText());
//            holder.textViewManualInhalador.setText(manualButtonLabel.toString());
            holder.idOculta.setText(String.valueOf(inh.getId()));
            holder.checkHecho.setActivated(t.isDone());

            final TextView textViewIata = holder.idOculta;
/*
            holder.textViewManualInhalador.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String inhaladorSeleccionado = (String) textViewIata.getText();

                    Inhalador inha = modelo.obtenerInhalador(Integer.parseInt(inhaladorSeleccionado));

                    inha.setListaPasosManual(modelo.obtenerPasosInhalador(inha.getName()));

                    ArrayList<String> titulos = new ArrayList<>();
                    ArrayList<Integer> imagenes = new ArrayList<>();
                    ArrayList<String> textos = new ArrayList<>();

                    for(Paso p:inha.getListaPasosManual()){
                        titulos.add(Commodities.retrieveResourceStringFromId(context,p.getTitulo()));

                        imagenes.add(p.getImagen());
                        textos.add(Commodities.retrieveResourceStringFromId(context, p.getTexto()));

                    }
                    Intent intent = new Intent(context, ScreenSlideActivity.class);
                    Bundle b = new Bundle();

                    b.putIntegerArrayList("imagenes", imagenes);
                    b.putStringArrayList("texto", textos);
                    b.putStringArrayList("titulos", titulos);

                    intent.putExtras(b);
                    context.startActivity(intent);

                }
            });
*/
            holder.checkHecho.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                   @Override
                                                   public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                       modelo.cambiarEstadoTomaAlarma(!isChecked,t.getId());
                                                   }
                                               }
            );

        }

        return convertView;

    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is content_crear_tratamiento power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    static class ViewHolder {

        ImageView imagenInhalador;
        TextView nombreMedicina;
        TextView textViewManualInhalador;
        CheckBox checkHecho;
        TextView idOculta;
        TextView numero_veces;
    }

}
