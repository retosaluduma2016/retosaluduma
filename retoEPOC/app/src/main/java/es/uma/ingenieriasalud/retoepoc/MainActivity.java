/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NavUtils;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.appointments.CitasActivity;
import es.uma.ingenieriasalud.retoepoc.appointments.ConsultarCitas;
import es.uma.ingenieriasalud.retoepoc.calls.CallManager;
import es.uma.ingenieriasalud.retoepoc.checkTreatment.ConsultarTratamiento;
import es.uma.ingenieriasalud.retoepoc.hashManagement.HashManager;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.persistence.Toma;
import es.uma.ingenieriasalud.retoepoc.physiotherapy.ConsultarManualFisioResp;
import es.uma.ingenieriasalud.retoepoc.rescueMedicines.ConsultarInhaladorRescate;
import es.uma.ingenieriasalud.retoepoc.smsConf.smsConfigurator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        boolean condicionesAceptadas = preferences.getBoolean("terminoAceptado", false);
        boolean datosIntroducidos = preferences.getBoolean("datosIntroducidos", false);
        if (!condicionesAceptadas) {
            Intent myIntent = new Intent(this, TermsAndConditions.class);
            startActivity(myIntent);
        }else if (!datosIntroducidos) {
            Intent myIntent = new Intent(this, IntroducirContrasena.class);
            startActivity(myIntent);
        }

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("databaseName", "retoEPOC_DB.db"); // value to store
        editor.commit();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.VIBRATE, Manifest.permission.WAKE_LOCK, Manifest.permission.SET_ALARM, Manifest.permission.RECEIVE_BOOT_COMPLETED, Manifest.permission.MODIFY_AUDIO_SETTINGS}, PackageManager.PERMISSION_GRANTED);
        }

    }

    @Override
    protected void onResume(){
        super.onResume();

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_main);

        ImageButton b112 = (ImageButton) findViewById(R.id.button5);
        ImageButton bcuidador = (ImageButton) findViewById(R.id.button15);
        ImageButton btratamiento = (ImageButton) findViewById(R.id.button3);
        ImageButton brescate = (ImageButton) findViewById(R.id.button8);
        ImageButton bfisio  = (ImageButton) findViewById(R.id.physioterapy);
        ImageButton bcitas = (ImageButton) findViewById(R.id.button10);

        b112.setImageBitmap(
                decodeSampledBitmapFromResource(getResources(),
                        this.getResources().getIdentifier("llamar112", "drawable", getPackageName()),
                        200, 200)) ;

        bcuidador.setImageBitmap(
                decodeSampledBitmapFromResource(getResources(),
                        this.getResources().getIdentifier("llamarcuidador", "drawable", getPackageName()),
                        200, 200));

        btratamiento.setImageBitmap(
                decodeSampledBitmapFromResource(getResources(),
                        this.getResources().getIdentifier("consultartratamiento", "drawable", getPackageName()),
                        200, 200));

        brescate.setImageBitmap(
                decodeSampledBitmapFromResource(getResources(),
                        this.getResources().getIdentifier("inhaladorrescate", "drawable", getPackageName()),
                        200, 200));

        bfisio.setImageBitmap(
                decodeSampledBitmapFromResource(getResources(),
                        this.getResources().getIdentifier("fisioterapiarespiratoria", "drawable", getPackageName()),
                        200, 200));

        bcitas.setImageBitmap(
                decodeSampledBitmapFromResource(getResources(),
                        this.getResources().getIdentifier("citas", "drawable", getPackageName()),
                        200, 200));

        SharedPreferences preferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        String password = preferences.getString("contrasena", "");
        ImageButton image = (ImageButton) findViewById(R.id.semaforo);
        if (!password.equals("")) {

            Modelo modelo = Modelo.getInstance();
            modelo.init(this);

            ArrayList<Toma> listaTomas = modelo.obtenerListaTomas();
            int contador = 0;
            for (Toma t : listaTomas) {
                if (t.isDone()) {
                    contador++;
                }
            }
            if (contador == listaTomas.size()) {
                image.setImageResource(R.drawable.lightgreen);
            } else if (contador > 0 && contador < listaTomas.size()) {
                image.setImageResource(R.drawable.lightyellow);
            } else if (contador == 0) {
                image.setImageResource(R.drawable.lightred);
            }
        } else {
            image.setImageResource(R.drawable.lightgreen);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_main_activity, menu);

        updateconfSMSOption(menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){
        super.onPrepareOptionsMenu(menu);
        updateconfSMSOption(menu);
        return true;
    }

    private void updateconfSMSOption(Menu menu){
        SharedPreferences preferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        boolean smsEnabled = preferences.getBoolean("sms", false);

        MenuItem mi = menu.findItem(R.id.conf_SMS);
        if(smsEnabled){
            mi.setTitle(getString(R.string.configurarsms) +" "+ getString(R.string.activadossms));
        }else{
            mi.setTitle(getString(R.string.configurarsms) +" "+ getString(R.string.desactivadossms));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.menu_acceso_sanitario:
                createAlertDialog();
                return true;
            case R.id.menu_ayuda:
                Intent intentAyuda = new Intent(this, AyudaTutorial.class);
                startActivity(intentAyuda);
                return true;
            case R.id.menu_acerca:
                Intent intentAcercaDe = new Intent(this, AcercaDe.class);
                startActivity(intentAcercaDe);
                return true;
            case R.id.menu_cambiarIdioma:
                Intent intentCambiarIdioma = new Intent(this, ChangeLanguage.class);
                startActivity(intentCambiarIdioma);
                return true;
            case R.id.conf_SMS:
                invalidateOptionsMenu();
                Intent intent = new Intent(this, smsConfigurator.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void onClickLlamar112(View v) {
        CallManager.callEmergency(this);
    }

    public void onClickLlamarCuidador(View v) {
        CallManager.callCaregiver(this);
    }

    //Accion del boton Consultar tratamiento de la ventana principal.
    public void onClickBotonConsultarTratamiento(View view) {

        Intent intent = new Intent(this, ConsultarTratamiento.class);
        startActivity(intent);

    }
    //Accion del boton Consultar manual de fisioterapia respiratoria de la ventana principal.
    public void onClickBotonConsultarManualFisio(View view) {

        Intent intent = new Intent(this, ConsultarManualFisioResp.class);
        startActivity(intent);

    }

    public void onClickRescate(View view) {

        Intent intent = new Intent(this, ConsultarInhaladorRescate.class);
        startActivity(intent);
    }

    //Accion del boton de gestion de citas de la ventana principal.
    public void onClickBotonCitas(View view) {

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.citas);
        alertDialogBuilder
                .setCancelable(true)
                .setMessage(R.string.quedesea)
                .setPositiveButton(R.string.anadir, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(getApplicationContext(), CitasActivity.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.eliminar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(getApplicationContext(), ConsultarCitas.class);
                        startActivity(intent);
                    }
                });
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void createAlertDialog(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.textoContraseña3);

        // Use an EditText view to get user input.
        final EditText input = new EditText(this);
        input.setId(0);
        input.setGravity(Gravity.CENTER);
        builder.setView(input);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {

                SharedPreferences preferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
                String password = preferences.getString("contrasena", "");
                if(HashManager.SHA1(input.getText().toString()).equals(password)){
                    Intent intentPersonalSanitario = new Intent(getApplicationContext(), AccesoPersonalSanitario.class);
                    startActivity(intentPersonalSanitario);

                    dialog.cancel();

                }else{
                    Toast pulsarAdd = Toast.makeText(getApplicationContext(), R.string.contraseñaIncorrecta, Toast.LENGTH_SHORT);
                    pulsarAdd.show();
                    dialog.cancel();
                }
            }
        });

        builder.setNegativeButton(R.string.salir, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.create().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        final AlertDialog dialog = builder.create();

        input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            }
        });

        dialog.show();
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is content_crear_tratamiento power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

}
