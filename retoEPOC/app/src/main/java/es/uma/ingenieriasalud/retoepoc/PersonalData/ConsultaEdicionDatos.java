package es.uma.ingenieriasalud.retoepoc.personalData;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.persistence.PatientPersonalData;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.R;

public class ConsultaEdicionDatos extends AppCompatActivity {

    private EditText nombre, apellidos, nsg, cuidador, telefonoCuidador,
            enfermero, alergias, estado, altura, peso, edad, enfermedades,
            medicoFamilia, medicoEspecialista, relacionCuidador;
    private Button guardar;
    private ImageButton editar;
    private ScrollView scroll;
    //private TextView titulo;

    private RadioButton fSi, fNo, dSi, dNo;
    private RadioGroup fRG, dRG;

    private boolean fum, diab;


    private Modelo modelo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_consulta_edicion_datos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        modelo = Modelo.getInstance();
        modelo.init(this);

        //titulo = (TextView) findViewById(R.id.textViewDatosPaciente);
        nombre = (EditText) findViewById(R.id.editTextNombre);
        apellidos = (EditText) findViewById(R.id.editTextApellidos);
        nsg = (EditText) findViewById(R.id.editTextNSG);
        altura = (EditText) findViewById(R.id.editTextAltura);
        peso = (EditText) findViewById(R.id.editTextPeso);
        edad = (EditText) findViewById(R.id.editTextEdad);
        enfermedades = (EditText) findViewById(R.id.editTextEnfermedades);
        relacionCuidador = (EditText) findViewById(R.id.editTextRelacion);
        medicoFamilia = (EditText) findViewById(R.id.editTextMedicoFamilia);
        medicoEspecialista = (EditText) findViewById(R.id.editTextEspecialista);
        cuidador = (EditText) findViewById(R.id.editTextCuidadorInformal);
        telefonoCuidador = (EditText) findViewById(R.id.editTextTelCuidadorInformal);
        enfermero = (EditText) findViewById(R.id.editTextEnfermeroReferencia);
        estado = (EditText) findViewById(R.id.editTextEstado);
        alergias = (EditText) findViewById(R.id.editTextAlergias);
        guardar = (Button) findViewById(R.id.buttonGuardarDatos);
        editar = (ImageButton) findViewById(R.id.imageEditar);
        scroll = (ScrollView) findViewById(R.id.scrollViewConsultaEdicion);
        fSi = (RadioButton) findViewById(R.id.radioButtonFumadorSi);
        fNo = (RadioButton) findViewById(R.id.radioButtonFumadorNo);
        dSi = (RadioButton) findViewById(R.id.radioButtonDiabeticoSi);
        dNo = (RadioButton) findViewById(R.id.radioButtonDiabeticoNo);
        fRG = (RadioGroup) findViewById(R.id.radioGroupFumador);
        dRG = (RadioGroup) findViewById(R.id.radioGroupDiabetico);

        editar.setVisibility(View.VISIBLE);
        guardar.setText(R.string.botonGuardar);
        guardar.setVisibility(View.GONE);
        nombre.setEnabled(false);
        apellidos.setEnabled(false);
        nsg.setEnabled(false);
        alergias.setEnabled(false);
        cuidador.setEnabled(false);
        telefonoCuidador.setEnabled(false);
        enfermero.setEnabled(false);
        estado.setEnabled(false);
        //titulo.setVisibility(View.GONE);
        altura.setEnabled(false);
        peso.setEnabled(false);
        edad.setEnabled(false);
        enfermedades.setEnabled(false);
        medicoFamilia.setEnabled(false);
        medicoEspecialista.setEnabled(false);
        relacionCuidador.setEnabled(false);
        for (int i = 0; i < fRG.getChildCount(); i++) {
            fRG.getChildAt(i).setEnabled(false);
        }
        for (int i = 0; i < dRG.getChildCount(); i++) {
            dRG.getChildAt(i).setEnabled(false);
        }
        //fRG.setEnabled(false);
        //dRG.setEnabled(false);




        PatientPersonalData patient = modelo.getPatientPersonalData();

        nombre.setText(patient.getName());
        apellidos.setText(patient.getSurname());
        nsg.setText(patient.getNss());
        cuidador.setText(patient.getCasualCaregiverName());
        telefonoCuidador.setText(patient.getCasualCaregiverPhone());
        enfermero.setText(patient.getNurseInCharge());
        alergias.setText(patient.getAlergy());
        estado.setText(patient.getSicknessState());
        altura.setText(patient.getHeight());
        peso.setText(patient.getPeso());
        edad.setText(patient.getEdad());
        enfermedades.setText(patient.getEnfermedades());
        medicoFamilia.setText(patient.getMedicoFamilia());
        medicoEspecialista.setText(patient.getMedicoEspecialista());
        relacionCuidador.setText(patient.getRelacionCuidador());

        if(patient.isFumador()){
            fSi.setChecked(true);
            fum = true;
        }else{
            fNo.setChecked(true);
            fum = false;

        }

        if(patient.isDiabetico()){
            dSi.setChecked(true);
            diab = true;
        }else{
            dNo.setChecked(true);
            diab = false;
        }

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    public void onClickEditar(View view) {
        editar.setVisibility(View.GONE);
        guardar.setVisibility(View.VISIBLE);
        nombre.setEnabled(true);
        apellidos.setEnabled(true);
        nsg.setEnabled(true);
        alergias.setEnabled(true);
        cuidador.setEnabled(true);
        telefonoCuidador.setEnabled(true);
        enfermero.setEnabled(true);
        estado.setEnabled(true);
        altura.setEnabled(true);
        peso.setEnabled(true);
        edad.setEnabled(true);
        enfermedades.setEnabled(true);
        medicoFamilia.setEnabled(true);
        medicoEspecialista.setEnabled(true);
        relacionCuidador.setEnabled(true);
        for (int i = 0; i < fRG.getChildCount(); i++) {
            fRG.getChildAt(i).setEnabled(true);
        }
        for (int i = 0; i < dRG.getChildCount(); i++) {
            dRG.getChildAt(i).setEnabled(true);
        }
        //fRG.setEnabled(true);
        //dRG.setEnabled(true);

        scroll.scrollTo(0, 0);
    }


    public void onClickRadioButtonFumador (View view){
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButtonFumadorSi:
                if (checked)
                    fum = true;
                break;
            case R.id.radioButtonFumadorNo:
                if (checked)
                    fum = false;
                break;
        }
    }
    public void onClickRadioButtonDiabetico (View view){
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButtonDiabeticoSi:
                if (checked)
                    diab = true;
                break;
            case R.id.radioButtonDiabeticoNo:
                if (checked)
                    diab = false;
                break;
        }
    }


    public void onClickGuardar(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
        alertDialogBuilder
                .setMessage(R.string.alertDialogTexto)
                .setCancelable(false)
                .setPositiveButton(R.string.alertDialogBotonSi, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        modelo.vaciarTablaDatos();
                        PatientPersonalData p = new PatientPersonalData(nombre.getText().toString(), apellidos.getText().toString(),
                                nsg.getText().toString(), altura.getText().toString(), peso.getText().toString(),
                                edad.getText().toString(),fum,diab, enfermedades.getText().toString(),
                                estado.getText().toString(),alergias.getText().toString(),
                                medicoFamilia.getText().toString(), medicoEspecialista.getText().toString(),
                                cuidador.getText().toString(), telefonoCuidador.getText().toString(),
                                relacionCuidador.getText().toString(),enfermero.getText().toString());
                        modelo.insertPatientPersonalData(p);
                        NavUtils.navigateUpFromSameTask(ConsultaEdicionDatos.this);
                    }
                })
                .setNegativeButton(R.string.alertDialogBotonNo, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK && guardar.getVisibility() == View.VISIBLE) {
            return alertExitTreatment();
        }
        //para las demas cosas, se reenvia el evento al listener habitual
        return super.onKeyDown(keyCode, event);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (guardar.getVisibility() == View.VISIBLE) {
            switch (menuItem.getItemId()) {
                case android.R.id.home:
                    return alertExitTreatment();
            }
            return (super.onOptionsItemSelected(menuItem));
        } else {
            ConsultaEdicionDatos.this.finish();
        }
        return true;
    }

    private boolean alertExitTreatment() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.alerDialogTitulo)
                .setMessage(R.string.alertDialogSalirSinGuardarDatos)
                .setNegativeButton(R.string.alertDialogBotonNoSalir, null)//sin listener
                .setPositiveButton(R.string.alertDialogBotonSalir, new DialogInterface.OnClickListener() {//un listener que al pulsar, cierre la aplicacion
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Salir
                        ConsultaEdicionDatos.this.finish();
                    }
                })
                .show();
        // Si el listener devuelve true, significa que el evento esta procesado, y nadie debe hacer nada mas
        return true;
    }

}


