/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.alarms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.os.Vibrator;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {


        Vibrator vib=(Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = new long[8];
        pattern[0] = 1000; // Wait one second
        pattern[1] = 950;  // Vibrate for most a second
        pattern[2] = 50;   // A pause long enough to feel distinction
        pattern[3] = 950;  // Repeat 3 more times
        pattern[4] = 50;
        pattern[5] = 950;
        pattern[6] = 50;
        pattern[7] = 950;
        vib.vibrate(pattern,-1);


        Intent i=new Intent(context,ConsultarTomasAlarmas.class);
        i.putExtra("hora",intent.getExtras().getInt("hora"));
        i.putExtra("min",intent.getExtras().getInt("min"));
        i.putExtra("id",intent.getExtras().getInt("id"));
        i.putExtra("veces",intent.getExtras().getInt("veces"));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        context.startActivity(i);


    }

}