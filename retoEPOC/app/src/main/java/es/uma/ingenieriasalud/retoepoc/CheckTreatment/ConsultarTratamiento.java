/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.checkTreatment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.persistence.Hour;
import es.uma.ingenieriasalud.retoepoc.persistence.Inhalador;
import es.uma.ingenieriasalud.retoepoc.persistence.Medicamento;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.persistence.Paso;
import es.uma.ingenieriasalud.retoepoc.persistence.Toma;
import es.uma.ingenieriasalud.retoepoc.R;
import es.uma.ingenieriasalud.retoepoc.manuals.ScreenSlideActivity;

public class ConsultarTratamiento extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_consultar_tratamiento);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Modelo modelo = Modelo.getInstance();
        modelo.init(this);

        ArrayList<ArrayList<String>> lista_tomas = new ArrayList<>();

        ArrayList<Toma> ltoma = modelo.obtenerListaTomas();

        if(ltoma.size() == 0){
            final ConsultarTratamiento ct= this;
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
            alertDialogBuilder
                    .setMessage(R.string.emptyTreatment)
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            NavUtils.navigateUpFromSameTask(ct);
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

        TextView tVrecomm = (TextView) findViewById(R.id.textViewConsultarTratamientoRecomendaciones);

        tVrecomm.setText(modelo.getTreatmentById(1).getRecomendaciones());
        tVrecomm.setTextColor(Color.RED);

        for (Toma toma : ltoma){

            ArrayList<String> listaView = new ArrayList<>();

            Medicamento medForListView = modelo.getMedicineByName(toma.getMedicamento());
            listaView.add(medForListView.getNameText()); // MEDICINE.NAME
            Inhalador inhForListView = modelo.getInhalerByName(toma.getInhalador());
            listaView.add(String.valueOf(inhForListView.getId())); // INHALER._ID
            listaView.add(inhForListView.getNameText()); // INHALER.NAME_INHALER_XML
            listaView.add(inhForListView.getImagen()); // INHALER.IMAGE
            Hour h = toma.getHour();
            listaView.add(String.valueOf(h.getHour())+":"+String.format(Locale.getDefault(),"%02d", h.getMin())); // HOUR:MIN
            int numeroTomas = toma.getInhalations();
            listaView.add(String.valueOf(numeroTomas)); // NUMBER OF INHALATIONS

            lista_tomas.add(listaView);

        }

        ListView listaTomas = (ListView) findViewById(R.id.listViewTomas);

        // Sorting
        Collections.sort(lista_tomas, new Comparator<ArrayList<String>>() {
            @Override
            public int compare(ArrayList<String> s1, ArrayList<String> s2)
            {
                return  s1.get(4).compareTo(s2.get(4));
            }
        });

        ListViewTomasAdapter mAdapter = new ListViewTomasAdapter(this,lista_tomas);
        listaTomas.setAdapter(mAdapter);

        Commodities.ajustarVistaListView(listaTomas);
        listaTomas.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView textViewIata = (TextView) view.findViewById(R.id.textView);
                String inhaladorSeleccionado = (String) textViewIata.getText();

                Modelo modelo = Modelo.getInstance();
                modelo.init(view.getContext());

                Inhalador inha = modelo.obtenerInhalador(Integer.parseInt(inhaladorSeleccionado));

                inha.setListaPasosManual(modelo.obtenerPasosInhalador(inha.getName()));

                ArrayList<String> titulos = new ArrayList<>();
                ArrayList<Integer> imagenes = new ArrayList<>();
                ArrayList<String> textos = new ArrayList<>();
                for(Paso p:inha.getListaPasosManual()){
                    titulos.add(Commodities.retrieveResourceStringFromId(view.getContext(),p.getTitulo()));
                    imagenes.add(p.getImagen());
                    textos.add(Commodities.retrieveResourceStringFromId(view.getContext(), p.getTexto()));
                }
                Intent intent = new Intent(view.getContext(), ScreenSlideActivity.class);
                Bundle b = new Bundle();

                b.putIntegerArrayList("imagenes", imagenes);
                b.putStringArrayList("texto", textos);
                b.putStringArrayList("titulos", titulos);

                intent.putExtras(b);
                startActivity(intent);
            }
        });


    }


}
