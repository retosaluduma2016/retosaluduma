/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.manuals;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.persistence.Inhalador;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.persistence.Paso;
import es.uma.ingenieriasalud.retoepoc.R;

public class ConsultarManualInhaladores extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_consultar_manual_inhaladores);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        Modelo modelo = Modelo.getInstance();
        modelo.init(this);

        ArrayList<Inhalador> lista_inhaladores = new ArrayList<>();

        obtenerListaInhaladoresUsuario(modelo,lista_inhaladores);

        if(lista_inhaladores.size() == 0){

            final ConsultarManualInhaladores cm = this;
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
            alertDialogBuilder
                    .setMessage(R.string.emptyTreatment)
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            NavUtils.navigateUpFromSameTask(cm);
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }

        ListView listaManuales = (ListView) findViewById(R.id.listViewManuales);

        ListViewManualesAdapter mAdapter = new ListViewManualesAdapter(this,lista_inhaladores);
        listaManuales.setAdapter(mAdapter);


        listaManuales.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Inhalador inhaladorSeleccionado = (Inhalador)parent.getItemAtPosition(position);
                consultar(inhaladorSeleccionado);

            }
        });
    }

    public void obtenerListaInhaladoresUsuario(Modelo modelo, ArrayList<Inhalador> lista_inhaladores) {

        ArrayList<Inhalador> listaInhaladoresUsuario = modelo.obtenerListaInhaladoresUsuario();

        for (Inhalador inha : listaInhaladoresUsuario){
            inha.setListaPasosManual(modelo.obtenerPasosInhalador(inha.getName()));

            boolean exists = false;

            for(Inhalador in : lista_inhaladores){
                if(in.getName().equals(inha.getName())){
                    exists = true;
                    break;
                }
            }

            if(!exists) {
                lista_inhaladores.add(inha);
            }

        }
    }

    public void consultar(Inhalador inh) {
        ArrayList<String> titulos = new ArrayList<>();
        ArrayList<Integer> imagenes = new ArrayList<>();
        ArrayList<String> textos = new ArrayList<>();
        for(Paso p:inh.getListaPasosManual()){

            titulos.add(Commodities.retrieveResourceStringFromId(this,p.getTitulo()));
            imagenes.add(p.getImagen());
            textos.add(Commodities.retrieveResourceStringFromId(this,p.getTexto()));

        }

        Intent intent = new Intent(this, ScreenSlideActivity.class);
        Bundle b = new Bundle();

        b.putIntegerArrayList("imagenes", imagenes);
        b.putStringArrayList("texto", textos);
        b.putStringArrayList("titulos", titulos);

        intent.putExtras(b);
        startActivity(intent);

    }

}
