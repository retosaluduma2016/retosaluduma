package es.uma.ingenieriasalud.retoepoc.xmlDatabase;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.persistence.DatabaseInserter;

/**
 * Created by jmalvarez on 8/6/16.
 */
public class XmlInhalerParser extends XmlEntryParser {

    private static final String ELEMENTS_TAGNAME = "inhalers";
    private static final String SINGLE_ELEMENT_TAGNAME = "inhaler";

    private static final String NAME_FIELD_TAGNAME = "name";
    private static final String IMAGE_FIELD_TAGNAME = "image";

    public XmlInhalerParser(DatabaseInserter dbIns, XmlEntryParser n) {
        super(dbIns, n);
        elementsTagName = ELEMENTS_TAGNAME;
    }

    @Override
    void parseElement(XmlPullParser parser, ArrayList<String> elementsList) throws XmlPullParserException, IOException {
        String name = parser.getName();
        // Starts by looking for the entry tag
        switch (name) {
            case SINGLE_ELEMENT_TAGNAME:
                elementsList.add(readInhalerEntry(parser));
                break;
            default:
                skip(parser);
        }
    }

    private String readInhalerEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, SINGLE_ELEMENT_TAGNAME);
        String inhName = null;
        String image = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case NAME_FIELD_TAGNAME:
                    inhName = readTextField(parser, NAME_FIELD_TAGNAME);
                    break;
                case IMAGE_FIELD_TAGNAME:
                    image = readTextField(parser, IMAGE_FIELD_TAGNAME);
                    break;
                default:
                    skip(parser);
            }
        }
        return inhName + FIELD_SEPARATOR + image;
    }
}
