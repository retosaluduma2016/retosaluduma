package es.uma.ingenieriasalud.retoepoc.persistence;

/**
 * Created by diegonarbonavilchez on 24/5/16.
 */
public class Appointment {
    private String id;
    private String dia;
    private String mes;
    private String año;
    private String hora;
    private String min;
    private String esp;
    private String n_doctor;
    private String p_diag;
    private String p_val;


    public Appointment(String id_,String dia_,String mes_, String año_, String hora_, String min_, String esp_, String n_doctor_,String p_diag_,String p_val_){
        this.id=id_;
        this.dia=dia_;
        this.mes=mes_;
        this.año=año_;
        this.hora=hora_;
        this.min=min_;
        this.esp=esp_;
        this.n_doctor=n_doctor_;
        this.p_diag=p_diag_;
        this.p_val=p_val_;
    }

    public Appointment(String dia_,String mes_, String año_, String hora_, String min_, String esp_, String n_doctor_,String p_diag_,String p_val_){
        this.dia=dia_;
        this.mes=mes_;
        this.año=año_;
        this.hora=hora_;
        this.min=min_;
        this.esp=esp_;
        this.n_doctor=n_doctor_;
        this.p_diag=p_diag_;
        this.p_val=p_val_;
    }

    public String getP_diag() {
        return p_diag;
    }

    public void setP_diag(String p_diag) {
        this.p_diag = p_diag;
    }

    public String getP_val() {
        return p_val;
    }

    public void setP_val(String p_val) {
        this.p_val = p_val;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getAño() {
        return año;
    }

    public void setAño(String año) {
        this.año = año;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getEsp() {
        return esp;
    }

    public void setEsp(String esp) {
        this.esp = esp;
    }

    public String getN_doctor() {
        return n_doctor;
    }

    public void setN_doctor(String n_doctor) {
        this.n_doctor = n_doctor;
    }
}
