package es.uma.ingenieriasalud.retoepoc.rescueMedicines;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import es.uma.ingenieriasalud.retoepoc.calls.CallManager;
import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.persistence.Inhalador;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.persistence.PresentacionMedicacionRescate;
import es.uma.ingenieriasalud.retoepoc.persistence.Treatment;
import es.uma.ingenieriasalud.retoepoc.R;

public class ConsultarInhaladorRescate extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_consultar_inhalador_rescate);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Modelo modelo = Modelo.getInstance();
        modelo.init(this);

        Treatment t = modelo.getTreatmentById(1);

        TextView nombre, medicina, dosis, texto, noRescate;
        ImageView imagen;
        nombre = (TextView) findViewById(R.id.textViewNombreInhaladorRescate);
        imagen = (ImageView) findViewById(R.id.imageViewInhaladorRescate);
        medicina = (TextView) findViewById(R.id.textViewMedicinaRescate);
        dosis = (TextView) findViewById(R.id.textViewDosisRescate);
        texto = (TextView) findViewById(R.id.textViewRescate);
        noRescate = (TextView) findViewById(R.id.textViewNoRescateAsociado);

        if (t.getRescate() == -1) {
            nombre.setVisibility(View.GONE);
            imagen.setVisibility(View.GONE);
            medicina.setVisibility(View.GONE);
            dosis.setVisibility(View.GONE);
            texto.setVisibility(View.GONE);
            noRescate.setVisibility(View.VISIBLE);
        }else{
            noRescate.setVisibility(View.GONE);
            nombre.setVisibility(View.VISIBLE);
            imagen.setVisibility(View.VISIBLE);
            medicina.setVisibility(View.VISIBLE);
            dosis.setVisibility(View.VISIBLE);
            texto.setVisibility(View.VISIBLE);

            PresentacionMedicacionRescate p = modelo.getRescueById(t.getRescate());
            Inhalador i = modelo.getInhalerByName(p.getInhaler());

            nombre.setText(Commodities.retrieveResourceStringFromId(this,i.getName()));

            imagen.setImageBitmap(decodeSampledBitmapFromResource(this.getResources(),
                    this.getResources().getIdentifier(i.getImagen(), "drawable", this.getPackageName()),
                    142, 174));

            medicina.append(": " + Commodities.retrieveResourceStringFromId(this,p.getMedicine()));
            if (p.getDose_ai2() == 0) {
                dosis.append(": " + Float.toString(p.getDose_ai1()));

            } else {
                dosis.append(": " + Float.toString(p.getDose_ai1()) + " / " + Float.toString(p.getDose_ai2()));

            }
        }
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is content_crear_tratamiento power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public void onClickLlamarCuidador(View v) {
        CallManager.callCaregiver(this);
    }

    public void onClickLlamar112(View v) {
        CallManager.callEmergency(this);
    }

    public void onClickBotonVolver(View view) {
        this.onBackPressed();
    }
}
