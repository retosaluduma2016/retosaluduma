/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.uma.ingenieriasalud.retoepoc.manuals;

import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.R;

/**
 * Demonstrates content_crear_tratamiento "screen-slide" animation using content_crear_tratamiento {@link ViewPager}. Because {@link ViewPager}
 * automatically plays such an animation when calling {@link ViewPager#setCurrentItem(int)}, there
 * isn't any animation-specific code in this sample.
 *
 * <p>This sample shows content_crear_tratamiento "next" button that advances the user to the next step in content_crear_tratamiento wizard,
 * animating the current screen out (to the left) and the next screen in (from the right). The
 * reverse animation is played when the user presses the "previous" button.</p>
 *
 * @see ScreenSlidePageFragment
 */
public class ScreenSlideActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private int NUM_PAGES;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    private TextToSpeech textToSpeech;

    Bundle b;

    private ArrayList<String> titulos;
    private ArrayList<Integer> imagenes;
    private ArrayList<String> texto;

    public ArrayList<String> getTitulos() {
        return titulos;
    }

    public ArrayList<String> getTexto() {
        return texto;
    }

    public ArrayList<Integer> getImagenes() {
        return imagenes;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_screen_slide);

        b = this.getIntent().getExtras();
        titulos = b.getStringArrayList("titulos");
        imagenes = b.getIntegerArrayList("imagenes");
        texto = b.getStringArrayList("texto");
        NUM_PAGES = imagenes.size();

        // Instantiate content_crear_tratamiento ViewPager and content_crear_tratamiento PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When changing pages, reset the action bar actions since they are dependent
                // on which page is currently active. An alternative approach is to have each
                // fragment expose actions itself (rather than the activity exposing actions),
                // but for simplicity, the activity provides the actions in this sample.

                invalidateOptionsMenu();
            }
        });
        final Button botonLeer = (Button) findViewById(R.id.botonLeerManual);
        textToSpeech = new TextToSpeech(this, this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_manual_inhaladores, menu);

        menu.findItem(R.id.menu_anterior).setVisible(mPager.getCurrentItem() > 0);
        menu.findItem(R.id.menu_siguiente).setVisible(mPager.getCurrentItem() < NUM_PAGES - 1);
        menu.findItem(R.id.menu_fin).setVisible(mPager.getCurrentItem() == NUM_PAGES - 1);
        //MenuItem itemAnterior =  menu.add(Menu.NONE,R.id.action_previous,Menu.NONE,R.string.action_previous);
        //MenuItem itemSiguiente =  menu.add(Menu.NONE,R.id.action_next,Menu.NONE,R.string.action_next);

        //itemAnterior.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        // itemSiguiente.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);


        // Add either content_crear_tratamiento "next" or "finish" button to the action bar, depending on which page
        // is currently selected.
        /*MenuItem item = menu.add(Menu.NONE, R.id.action_next, Menu.NONE,
                (mPager.getCurrentItem() == mPagerAdapter.getCount() - 1)
                        ? R.string.action_finish
                        : R.string.action_next);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Navigate "up" the demo structure to the launchpad activity.
                // See http://developer.android.com/design/patterns/navigation.html for more.
                this.finish();
                return true;

            case R.id.menu_anterior:
                // Go to the previous step in the wizard. If there is no previous step,
                // setCurrentItem will do nothing.
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                return true;

            case R.id.menu_siguiente:
                // Advance to the next step in the wizard. If there is no next step, setCurrentItem
                // will do nothing.
                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                return true;
            case R.id.menu_fin:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (mPager.getCurrentItem() == 0) {
            this.finish();
        } else {
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    public void onClickLeerManual(View view) {
        textToSpeech.setLanguage(new Locale(getResources().getString(R.string.language), getResources().getString(R.string.country)));
        speak(texto.get(mPager.getCurrentItem()));
    }

    private void speak(String str) {
        textToSpeech.speak(str, TextToSpeech.QUEUE_FLUSH, null);
        textToSpeech.setSpeechRate(0.0f);
        textToSpeech.setPitch(0.0f);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.LANG_MISSING_DATA | status == TextToSpeech.LANG_NOT_SUPPORTED) {
            Toast.makeText(this, "ERROR LANG_MISSING_DATA | LANG_NOT_SUPPORTED", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

    /**
     * A simple pager adapter that represents 5 {@link ScreenSlidePageFragment} objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            return ScreenSlidePageFragment.create(position);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
