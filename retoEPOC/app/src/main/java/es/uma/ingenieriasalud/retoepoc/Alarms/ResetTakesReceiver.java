/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.alarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.util.Calendar;

import es.uma.ingenieriasalud.retoepoc.createTreatment.CrearTratamiento;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;

public class ResetTakesReceiver extends BroadcastReceiver {

    private Modelo modelo;

    @Override
    public void onReceive(Context context, Intent intent) {

        modelo = Modelo.getInstance();
        modelo.init(context);
        modelo.resetearEstadoTomas();

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(CrearTratamiento.ALARM_SERVICE);
        Intent refrescarTomasIntent = new Intent(context, ResetTakesReceiver.class);

        Calendar now = Calendar.getInstance();
        Calendar alarm = Calendar.getInstance();
        alarm.set(Calendar.HOUR_OF_DAY, 23);
        alarm.set(Calendar.MINUTE, 59);
        alarm.set(Calendar.SECOND, 0);
        if (alarm.before(now)) alarm.add(Calendar.DAY_OF_MONTH, 1);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, refrescarTomasIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        System.out.println("Alarm de reseteo " + 0 + " is set at " + alarm.getTime());
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getTimeInMillis(), pendingIntent);
        } else {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarm.getTimeInMillis(), pendingIntent);
        }

    }

}