/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.persistence;

import android.support.annotation.NonNull;

import es.uma.ingenieriasalud.retoepoc.HasGetNameText;

public class Medicamento implements HasGetNameText, Comparable<Medicamento> {

    private int id;
    private String name;
    private String nameText;

    public Medicamento(int id, String name, String nameText) {
        this.id = id;
        this.name = name;
        this.nameText = nameText;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameText() {
        return nameText;
    }

    public void setNameText(String nmTxt) {
        nameText = nmTxt;
    }

    @Override
    public int compareTo(@NonNull Medicamento another) {
        return this.nameText.compareTo(another.nameText);
    }
}
