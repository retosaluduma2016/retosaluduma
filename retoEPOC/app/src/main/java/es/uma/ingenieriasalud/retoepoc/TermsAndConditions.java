/***********************************************************************
 * This file is part of MyEPOC.
 * <p/>
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

public class TermsAndConditions extends AppCompatActivity {

    //lista de ids donde se almacenan los strings de los terminos
    //esto deberá ir en la BD
    private int listaDeIdsTerms[] = {R.string.textViewTerm1, R.string.textViewTerm2, R.string.textViewTerm3, R.string.textViewTerm4, R.string.textViewTerm5};
    //contador que usaremos para iterar sobre la lista
    int contadorDeIds = 0;
    //textview donde se mostrara el contenido
    private TextView textTerm;
    private ScrollView scroll;
    private Button buttonBack;
    private Button buttonTermFinal;
    private Button buttonContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_terms_and_conditions);
        //inicializamos con el primer String
        textTerm = (TextView) findViewById(R.id.textViewTerm1);
        scroll = (ScrollView) findViewById(R.id.scrollView3);
        buttonBack = (Button) findViewById(R.id.buttonBack);
        buttonContinue = (Button) findViewById(R.id.buttonContinueCondition);

        buttonTermFinal = (Button) findViewById(R.id.buttonAcceptCondition);

        if (contadorDeIds == 0) {
            buttonBack.setVisibility(View.GONE);
            buttonTermFinal.setVisibility(View.GONE);
        }

    }

    public void onClickButtonTerm(View v) {

        //id del boton que se haya pulsado
        int id = v.getId();
        //reseteamos el scrollView arriba
        //para obligar a "leer"
        scroll.scrollTo(0, 0);
        //si no hemos llegado al final, incrementamos contador
        //aplicado sobre la lista
        if (id == R.id.buttonContinueCondition && contadorDeIds < listaDeIdsTerms.length - 1) {
            contadorDeIds++;
            buttonBack.setVisibility(View.VISIBLE);
        } else if (id == R.id.buttonBack && contadorDeIds <= listaDeIdsTerms.length) {
            contadorDeIds--;
        }
        if (contadorDeIds == 0) {
            buttonBack.setVisibility(View.GONE);
            buttonTermFinal.setVisibility(View.GONE);
        }
        //mandamos a textTerm el texto que corresponde
        //segun el contador de la lista
        textTerm.setText(listaDeIdsTerms[contadorDeIds]);

        if (contadorDeIds < listaDeIdsTerms.length - 1) {
            buttonContinue.setVisibility(View.VISIBLE);
            buttonTermFinal.setVisibility(View.GONE);
        }
        //if para cuando llegamos al final de la lista
        Button buttonTermFinal = (Button) findViewById(R.id.buttonAcceptCondition);
        Button buttonContinue = (Button) findViewById(R.id.buttonContinueCondition);
        if (contadorDeIds == listaDeIdsTerms.length - 1) {

            buttonContinue.setVisibility(View.GONE);
            buttonTermFinal.setVisibility(View.VISIBLE);
            //bll.removeView(buttonContinue);
            //bll.addView(buttonTermFinal, bll.getChildCount());
            //le asignamos el texto correspondiente al ultimo paso de los terminos
            buttonTermFinal.setText(R.string.buttonAcceptCondition);
            buttonTermFinal.setBackgroundColor(Color.rgb(99, 204, 00));
        }

    }

    public void onClickAceptar(View l) {

        //si ha llegado aquí, se han aceptado los terminos
        SharedPreferences prefs = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("terminoAceptado", true);
        editor.commit();
        //Intent myIntent = new Intent(TermsAndConditions.this, MainActivity.class);

        Intent myIntent = new Intent(TermsAndConditions.this, IntroducirContrasena.class);
        startActivity(myIntent);
    }

    //en los términos y condiciones se bloquea el botón atrás
    @Override
    public void onBackPressed() {
    }


}
