package es.uma.ingenieriasalud.retoepoc.xmlDatabase;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.persistence.DatabaseInserter;

/**
 * Created by jmalvarez on 8/6/16.
 */
public class XmlActiveIngredientParser extends XmlEntryParser {

    private static final String ELEMENTS_TAGNAME = "activeIngredients";
    private static final String SINGLE_ELEMENT_TAGNAME = "activeIngredient";

    private static final String NAME_FIELD_TAGNAME = "name";

    public XmlActiveIngredientParser(DatabaseInserter dbIns, XmlEntryParser n) {
        super(dbIns, n);
        elementsTagName = ELEMENTS_TAGNAME;
    }

    @Override
    void parseElement(XmlPullParser parser, ArrayList<String> elementsList) throws XmlPullParserException, IOException {
        String activeIngredientTagName = parser.getName();
        // Starts by looking for the entry tag
        switch (activeIngredientTagName) {
            case SINGLE_ELEMENT_TAGNAME:
                elementsList.add(readActiveIngredientEntry(parser));
                break;
            default:
                skip(parser);
        }
    }

    private String readActiveIngredientEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, SINGLE_ELEMENT_TAGNAME);
        String aiName = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case NAME_FIELD_TAGNAME:
                    aiName = readTextField(parser, NAME_FIELD_TAGNAME);
                    break;
                default:
                    skip(parser);
            }
        }
        return aiName;
    }
}
