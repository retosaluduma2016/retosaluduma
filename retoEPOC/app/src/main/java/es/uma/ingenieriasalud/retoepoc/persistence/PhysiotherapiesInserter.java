package es.uma.ingenieriasalud.retoepoc.persistence;

import android.content.ContentValues;

import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.xmlDatabase.DatabaseXMLParser;

/**
 * Created by jmalvarez on 8/6/16.
 */
public class PhysiotherapiesInserter extends DatabaseInserter {
    public PhysiotherapiesInserter(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public void insertData(ArrayList<String> elementsList) {
        for (String physiotherapyName : elementsList) {
            String[] physiotherapyParts = physiotherapyName.split(String.valueOf(DatabaseXMLParser.FIELD_SEPARATOR));
            if (physiotherapyParts.length == 2) {
                int err;
                ContentValues values = new ContentValues();
                values.put(ModeloContract.PhysiotherapyTable.NAME, physiotherapyParts[0]);
                values.put(ModeloContract.PhysiotherapyTable.IMAGE, physiotherapyParts[1]);
                long rowid = database.insert(ModeloContract.PhysiotherapyTable.TABLE_NAME, null, values);
                if (-1 == rowid) {
                    err = 1;
                } else {
                    err = 0;
                }
            } else {
                System.err.println("Error when inserting " + physiotherapyName + " in DB.");
            }
        }

    }
}
