package es.uma.ingenieriasalud.retoepoc.xmlDatabase;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.persistence.DatabaseInserter;

/**
 * Created by jmalvarez on 8/6/16.
 */
public class XMLResMedPresParser extends XmlEntryParser {

    private static final String ELEMENTS_TAGNAME = "rescueMedicinePresentations";
    private static final String SINGLE_ELEMENT_TAGNAME = "rescueMedicinePresentation";

    private static final String MEDREF_FIELD_TAGNAME = "medicineRef";
    private static final String ACTINGREF_FIELD_TAGNAME = "activeIngRef";
    private static final String INHREF_FIELD_TAGNAME = "inhalerRef";
    private static final String DOSE_FIELD_TAGNAME = "dose";

    public XMLResMedPresParser(DatabaseInserter dbIns, XmlEntryParser n) {
        super(dbIns, n);
        elementsTagName = ELEMENTS_TAGNAME;
    }

    @Override
    void parseElement(XmlPullParser parser, ArrayList<String> elementsList) throws XmlPullParserException, IOException {
        String medicinePresentationTagName = parser.getName();
        // Starts by looking for the entry tag
        switch (medicinePresentationTagName) {
            case SINGLE_ELEMENT_TAGNAME:
                elementsList.add(readRescueMedicinePresentationEntry(parser));
                break;
            default:
                skip(parser);
        }
    }

    private String readRescueMedicinePresentationEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, SINGLE_ELEMENT_TAGNAME);
        String medRef = null;
        String aiRef = null;
        String inhRef = null;
        String dose = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case MEDREF_FIELD_TAGNAME:
                    medRef = readTextField(parser, MEDREF_FIELD_TAGNAME);
                    break;
                case ACTINGREF_FIELD_TAGNAME:
                    aiRef = readTextField(parser, ACTINGREF_FIELD_TAGNAME);
                    break;
                case INHREF_FIELD_TAGNAME:
                    inhRef = readTextField(parser, INHREF_FIELD_TAGNAME);
                    break;
                case DOSE_FIELD_TAGNAME:
                    dose = readTextField(parser, DOSE_FIELD_TAGNAME);
                    break;
                default:
                    skip(parser);
            }

        }
        return medRef + FIELD_SEPARATOR + aiRef + FIELD_SEPARATOR +
                inhRef + FIELD_SEPARATOR + dose;
    }
}
