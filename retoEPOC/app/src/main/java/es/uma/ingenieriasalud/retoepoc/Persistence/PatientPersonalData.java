package es.uma.ingenieriasalud.retoepoc.persistence;

/**
 * Created by jmalvarez on 17/5/16.
 */
public class PatientPersonalData {

    private String name, surname, nss, height, peso, edad;
    private String alergy, enfermedades;
    private boolean fumador, diabetico;
    private String casualCaregiverName, medicoFamilia, medicoEspecialista, relacionCuidador;
    private String casualCaregiverPhone;

    private String nurseInCharge;
    private String sicknessState;

    /*public PatientPersonalData(String name, String surname, String nss,
                               String alergy, String caregiverName, String caregiverPhone,
                               String nurseInCharge, String sicknessState) {
        this.name = name;
        this.surname = surname;
        this.nss = nss;
        this.alergy = alergy;
        this.casualCaregiverName = caregiverName;
        this.casualCaregiverPhone = caregiverPhone;
        this.nurseInCharge = nurseInCharge;
        this.sicknessState = sicknessState;
    }*/

    public PatientPersonalData(String name, String surname, String nss, String altura, String peso,
                               String edad, boolean fumador,
                               boolean diabetico, String enfermedades, String sicknessState,
                               String alergy, String medicoFamilia, String medicoEspecialista, String casualCaregiverName,
                               String casualCaregiverPhone, String relacionCuidador,
                               String nurseInCharge) {
        this.name = name;
        this.surname = surname;
        this.nss = nss;
        this.height = altura;
        this.peso = peso;
        this.edad = edad;
        this.alergy = alergy;
        this.enfermedades = enfermedades;
        this.fumador = fumador;
        this.diabetico = diabetico;
        this.casualCaregiverName = casualCaregiverName;
        this.medicoFamilia = medicoFamilia;
        this.medicoEspecialista = medicoEspecialista;
        this.relacionCuidador = relacionCuidador;
        this.casualCaregiverPhone = casualCaregiverPhone;
        this.nurseInCharge = nurseInCharge;
        this.sicknessState = sicknessState;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getNss() {
        return nss;
    }

    public String getAlergy() {
        return alergy;
    }

    public String getCasualCaregiverName() {
        return casualCaregiverName;
    }

    public String getCasualCaregiverPhone() {
        return casualCaregiverPhone;
    }

    public String getSicknessState() {
        return sicknessState;
    }


    public String getNurseInCharge() {
        return nurseInCharge;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public void setAlergy(String alergy) {
        this.alergy = alergy;
    }

    public String getEnfermedades() {
        return enfermedades;
    }

    public void setEnfermedades(String enfermedades) {
        this.enfermedades = enfermedades;
    }

    public boolean isFumador() {
        return fumador;
    }

    public void setFumador(boolean fumador) {
        this.fumador = fumador;
    }

    public boolean isDiabetico() {
        return diabetico;
    }

    public void setDiabetico(boolean diabetico) {
        this.diabetico = diabetico;
    }

    public void setCasualCaregiverName(String casualCaregiverName) {
        this.casualCaregiverName = casualCaregiverName;
    }

    public String getMedicoFamilia() {
        return medicoFamilia;
    }

    public void setMedicoFamilia(String medicoFamilia) {
        this.medicoFamilia = medicoFamilia;
    }

    public String getMedicoEspecialista() {
        return medicoEspecialista;
    }

    public void setMedicoEspecialista(String medicoEspecialista) {
        this.medicoEspecialista = medicoEspecialista;
    }

    public String getRelacionCuidador() {
        return relacionCuidador;
    }

    public void setRelacionCuidador(String relacionCuidador) {
        this.relacionCuidador = relacionCuidador;
    }

    public void setCasualCaregiverPhone(String casualCaregiverPhone) {
        this.casualCaregiverPhone = casualCaregiverPhone;
    }

    public void setNurseInCharge(String nurseInCharge) {
        this.nurseInCharge = nurseInCharge;
    }

    public void setSicknessState(String sicknessState) {
        this.sicknessState = sicknessState;
    }
}
