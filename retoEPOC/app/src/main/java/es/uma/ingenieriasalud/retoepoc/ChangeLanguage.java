/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Locale;

public class ChangeLanguage extends AppCompatActivity {

    public final static String prefsFileName = "myPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_cambiar_idioma);

    }


    public void onClickButtonChangeLang(View v) {

        //id del boton que se haya pulsado
        int id = v.getId();

        String lenguage = null, textoAviso = null;
        if (id == R.id.imageButtonEs || id == R.id.textViewCambiarEspañol) {
            lenguage = "es";
            textoAviso = "Lenguaje cambiado a Español";
        }

        else if (id == R.id.imageButtonIg || id == R.id.textViewCambiarIngles) {
            lenguage = "en";
            textoAviso = "Language set to English";

        }
        //TODO: Comprobar traduccion de "Lenguaje cambiado a frances"
        else if (id == R.id.imageButtonFr || id == R.id.textViewCambiarFrances) {
            lenguage = "fr";
            textoAviso = "Langue changée en français";


        }
        SharedPreferences prefs = getSharedPreferences(prefsFileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("languageLocale", lenguage);
        editor.commit();
        Toast.makeText(getApplicationContext(), textoAviso,
                Toast.LENGTH_LONG).show();

    }
}
