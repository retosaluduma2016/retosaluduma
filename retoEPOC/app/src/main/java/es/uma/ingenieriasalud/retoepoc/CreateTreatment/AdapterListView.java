/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.createTreatment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.persistence.Inhalador;
import es.uma.ingenieriasalud.retoepoc.persistence.Medicamento;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.persistence.PrincipioActivo;
import es.uma.ingenieriasalud.retoepoc.persistence.Toma;
import es.uma.ingenieriasalud.retoepoc.R;

public class AdapterListView extends ArrayAdapter<Toma> {

    private Context context;
    private ListView listView;
    private Modelo modelo;




    public AdapterListView(Modelo modelo, Context context, ArrayList<Toma> listaTomas, ListView listView) {
        super(context, R.layout.vista_listview_tomas, listaTomas);
        // Guardamos los parámetros en variables de clase.
        this.context = context;
        this.listView = listView;
        this.modelo = modelo;


    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final int maxString = 32;
        final int subString = 28;

        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.vista_listview_tomas, null);


        TextView medicamento = (TextView) item.findViewById(R.id.textViewListaMedicamento);

        Medicamento med = modelo.getMedicineByName(getItem(position).getMedicamento());
        if (med.getNameText().length() > maxString) {
            medicamento.setText(String.format("%s...",med.getNameText().substring(0, subString)));
        } else {
            medicamento.setText(med.getNameText());
        }

        TextView principio = (TextView) item.findViewById(R.id.textViewListaPrincipio);

        PrincipioActivo pa = modelo.getActiveIngredientByName(getItem(position).getPrincipio());
        if (pa.getNameText().length() > maxString) {
            principio.setText(String.format("%s...",pa.getNameText().substring(0, subString)));
        } else {
            principio.setText(pa.getNameText());
        }

        TextView dosis = (TextView) item.findViewById(R.id.textViewListaDosis);
        String d = (getItem(position).getDosis()).toString();

        if (d.length() > maxString) {
            d = d.substring(0, subString) + "...";
        }
        dosis.setText(d);

        TextView hora = (TextView) item.findViewById(R.id.textViewListaHora);
        String h = (getItem(position).getHour()).toString();
        hora.setText(String.format("%s %s",context.getResources().getString(R.string.textViewHora), h));

        TextView inhalaciones = (TextView) item.findViewById(R.id.textViewListaInhalaciones);
        String inh = Integer.toString(getItem(position).getInhalations());
        inhalaciones.setText(String.format("%s %s", "Inhalaciones: ", inh));


        TextView inhalador = (TextView) item.findViewById(R.id.textViewListaInhalador);

        Inhalador in = modelo.getInhalerByName(getItem(position).getInhalador());
        if (in.getNameText().length() > maxString) {
            inhalador.setText(String.format("%s...", in.getNameText().substring(0, subString)));
        } else {
            inhalador.setText(in.getNameText());
        }

        final Toma toma = getItem(position);

        ImageButton borrar = (ImageButton) item.findViewById(R.id.imageButtonBorrarVistaLista);
        borrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
                alertDialogBuilder
                        .setMessage(R.string.alertDialogEliminarTexto)
                        .setCancelable(false)
                        .setPositiveButton(R.string.alertDialogBotonSi, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast pulsarAdd = Toast.makeText(context, R.string.toastBorrar, Toast.LENGTH_SHORT);
                                pulsarAdd.show();
                                AdapterListView.this.remove(toma);
                                AdapterListView.this.notifyDataSetChanged();
                                Commodities.ajustarVistaListView(listView);
                            }
                        })
                        .setNegativeButton(R.string.alertDialogBotonNo, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();

            }
        });

        return item;
    }




}
