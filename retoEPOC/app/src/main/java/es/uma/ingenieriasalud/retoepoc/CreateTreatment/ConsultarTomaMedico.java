/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.createTreatment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.persistence.Toma;
import es.uma.ingenieriasalud.retoepoc.R;

public class ConsultarTomaMedico extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_consultar_tratamiento_particular);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        TextView medicamento,principio, dosis, inhalador, hora, inhalaciones ;

        medicamento = (TextView) findViewById(R.id.textViewMedicamentoDetallado);
        principio = (TextView) findViewById(R.id.textViewPrincipioDetallado);
        dosis = (TextView) findViewById(R.id.textViewDosisDetallada);
        inhalador = (TextView) findViewById(R.id.textViewInhaladorDetallado);
        hora = (TextView) findViewById(R.id.textViewHoraDetallada);
        inhalaciones = (TextView) findViewById(R.id.textViewInhalacionesDetallada);

        medicamento.setText(getIntent().getExtras().getString(Toma.MEDICAMENTO));
        principio.setText(getIntent().getExtras().getString(Toma.PRINCIPIO));
        dosis.setText(getIntent().getExtras().getString(Toma.DOSIS));
        inhalador.setText(getIntent().getExtras().getString(Toma.INHALADOR));
        hora.setText(getIntent().getExtras().getString(Toma.HOUR));
        inhalaciones.setText(getIntent().getExtras().getString(Toma.INHALATIONS));

    }

    //Accion del boton Consultar manual de inahaladores de la ventana principal.
    public void onClickBotonVolver(View view) {
        this.onBackPressed();
    }


}
