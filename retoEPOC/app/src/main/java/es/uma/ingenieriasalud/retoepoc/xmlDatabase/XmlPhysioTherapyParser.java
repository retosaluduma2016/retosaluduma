package es.uma.ingenieriasalud.retoepoc.xmlDatabase;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.persistence.DatabaseInserter;

/**
 * Created by jmalvarez on 8/6/16.
 */
public class XmlPhysiotherapyParser extends XmlEntryParser {

    private static final String ELEMENTS_TAGNAME = "physiotherapies";
    private static final String SINGLE_ELEMENT_TAGNAME = "physiotherapy";

    private static final String NAME_FIELD_TAGNAME = "name";
    private static final String IMAGE_FIELD_TAGNAME = "image";

    public XmlPhysiotherapyParser(DatabaseInserter dbIns, XmlEntryParser n) {
        super(dbIns, n);
        elementsTagName = ELEMENTS_TAGNAME;
    }

    @Override
    void parseElement(XmlPullParser parser, ArrayList<String> elementsList) throws XmlPullParserException, IOException {
        String name = parser.getName();
        // Starts by looking for the entry tag
        switch (name) {
            case SINGLE_ELEMENT_TAGNAME:
                elementsList.add(readPhysiotherapyEntry(parser));
                break;
            default:
                skip(parser);
        }
    }

    private String readPhysiotherapyEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, SINGLE_ELEMENT_TAGNAME);
        String phyName = null;
        String image = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case NAME_FIELD_TAGNAME:
                    phyName = readTextField(parser, NAME_FIELD_TAGNAME);
                    break;
                case IMAGE_FIELD_TAGNAME:
                    image = readTextField(parser, IMAGE_FIELD_TAGNAME);
                    break;
                default:
                    skip(parser);
            }
        }
        return phyName + FIELD_SEPARATOR + image;
    }

}
