package es.uma.ingenieriasalud.retoepoc.persistence;

import android.provider.ContactsContract;

import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by jmalvarez on 8/6/16.
 */
public abstract class DatabaseInserter {

    SQLiteDatabase database;

    public DatabaseInserter(SQLiteDatabase db) { database = db; }

    abstract public void insertData(ArrayList<String> elementsList);

}
