/***********************************************************************
 * This file is part of MyEPOC.
 * <p/>
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.createTreatment;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import es.uma.ingenieriasalud.retoepoc.alarms.AlarmReceiver;
import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.persistence.Toma;
import es.uma.ingenieriasalud.retoepoc.persistence.Treatment;
import es.uma.ingenieriasalud.retoepoc.persistence.PresentacionMedicacionRescate;
import es.uma.ingenieriasalud.retoepoc.R;
import es.uma.ingenieriasalud.retoepoc.alarms.ResetTakesReceiver;


public class CrearTratamiento extends AppCompatActivity {

    private ListView listView;
    private AdapterListView adapterListView;
    private int rescateId;

    private Modelo modelo;

    private final ArrayList<Toma> listaTratamientosNuevos = new ArrayList<>();
    private final ArrayList<Toma> listaTratamientosOriginal = new ArrayList<>();

    private Spinner spinnerEmergencia;

    private ArrayList<PresentacionMedicacionRescate> listaPresentacionRescate = new ArrayList<>();

    private Button botonRescate, botonBorrarRescate;
    private TextView medicinaR, principioR, inhaladorR, dosisR, noRescate;

    private EditText recomendaciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_crear_tratamiento);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //Inicializar base de datos

        modelo = Modelo.getInstance();
        modelo.init(this);

        //Obtener lista de la BD----------------------------
        spinnerEmergencia = (Spinner) findViewById(R.id.spinnerRescate);
        listaPresentacionRescate = modelo.obtenerListaMedicamentosRescate();
        AdapterSpinner adapterSpinner = new AdapterSpinner(this,R.layout.vista_spinner_rescate,
                                                           listaPresentacionRescate);
        spinnerEmergencia.setAdapter(adapterSpinner);

        spinnerEmergencia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                rescateId = listaPresentacionRescate.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Treatment tratamiento = modelo.getTreatmentById(1);
        rescateId = tratamiento.getRescate();

        medicinaR = (TextView) findViewById(R.id.textViewMedicinaCrearRescate);
        principioR = (TextView) findViewById(R.id.textViewPrincipioCrearRescate);
        inhaladorR = (TextView) findViewById(R.id.textViewInhaladorCrearRescate);
        dosisR = (TextView) findViewById(R.id.textViewDosisCrearRescate);
        noRescate = (TextView) findViewById(R.id.textViewNoRescate);
        botonRescate = (Button) findViewById(R.id.buttonCrearRescate);
        botonBorrarRescate = (Button) findViewById(R.id.buttonBorrarRescate);


        spinnerEmergencia.setVisibility(View.GONE);

        if(tratamiento.getRescate() == -1){
            medicinaR.setVisibility(View.GONE);
            principioR.setVisibility(View.GONE);
            inhaladorR.setVisibility(View.GONE);
            dosisR.setVisibility(View.GONE);
            noRescate.setVisibility(View.VISIBLE);
            noRescate.setText(R.string.noSeleccRescate);
            botonRescate.setVisibility(View.VISIBLE);
            botonBorrarRescate.setVisibility(View.GONE);

        }else{
            PresentacionMedicacionRescate rescate = modelo.getRescueById(tratamiento.getRescate());
            botonRescate.setVisibility(View.GONE);
            botonBorrarRescate.setVisibility(View.VISIBLE);
            noRescate.setVisibility(View.GONE);
            medicinaR.setVisibility(View.VISIBLE);
            principioR.setVisibility(View.VISIBLE);
            inhaladorR.setVisibility(View.VISIBLE);
            dosisR.setVisibility(View.VISIBLE);
            medicinaR.append(": " + Commodities.retrieveResourceStringFromId(this,rescate.getMedicine()));
            principioR.append(": " + Commodities.retrieveResourceStringFromId(this,rescate.getActiveIngredient()));
            inhaladorR.append(": " + Commodities.retrieveResourceStringFromId(this,rescate.getInhaler()));
            if(rescate.getDose_ai2() == 0){
                dosisR.append(": " + rescate.getDose_ai1());
            }else{
                dosisR.append(": " + Float.toString(rescate.getDose_ai1()) + " / " +
                                     Float.toString(rescate.getDose_ai2()));
            }

        }

        recomendaciones = (EditText) findViewById(R.id.editTextCrearTratamientoViewRecomendaciones);

        recomendaciones.setText(tratamiento.getRecomendaciones());

        //Inicializar lista de tomas
        listView = (ListView) findViewById(R.id.listView);

        ArrayList<Toma> listaTomas = modelo.obtenerListaTomas();

        for (Toma tm : listaTomas) {

            Toma t = new Toma(tm.getId(), tm.getMedicamento(), tm.getPrincipio(), tm.getInhalador(),
                    tm.getDosis(), tm.getHour(), tm.isDone(), tm.getInhalations());

            listaTratamientosNuevos.add(t);
            listaTratamientosOriginal.add(t);
        }

        adapterListView = new AdapterListView(modelo, this, listaTratamientosNuevos, listView);
        listView.setAdapter(adapterListView);

        Commodities.ajustarVistaListView(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getApplicationContext(), ConsultarTomaMedico.class);

                String mNameText = modelo.getMedicineByName(listaTratamientosNuevos.get(position).getMedicamento()).getNameText();
                intent.putExtra(Toma.MEDICAMENTO, mNameText);

                String pNameText = modelo.getActiveIngredientByName(listaTratamientosNuevos.get(position).getPrincipio()).getNameText();
                intent.putExtra(Toma.PRINCIPIO, pNameText);

                String d = (listaTratamientosNuevos.get(position).getDosis()).toString();
                intent.putExtra(Toma.DOSIS, d);

                String h = (listaTratamientosNuevos.get(position).getHour()).toString();
                intent.putExtra(Toma.HOUR, h);

                String inh = Integer.toString(listaTratamientosNuevos.get(position).getInhalations());
                intent.putExtra(Toma.INHALATIONS, inh);

                String inNameText = modelo.getInhalerByName(listaTratamientosNuevos.get(position).getInhalador()).getNameText();
                intent.putExtra(Toma.INHALADOR, inNameText);

                startActivity(intent);
            }
        });

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }


    public void onClickCrearRescate(View view){
        spinnerEmergencia.setVisibility(View.VISIBLE);
        noRescate.setVisibility(View.GONE);
        medicinaR.setVisibility(View.GONE);
        principioR.setVisibility(View.GONE);
        inhaladorR.setVisibility(View.GONE);
        dosisR.setVisibility(View.GONE);
        botonRescate.setVisibility(View.GONE);

    }

    public void onClickBorrarRescate(View view){
        spinnerEmergencia.setVisibility(View.GONE);
        noRescate.setVisibility(View.VISIBLE);
        medicinaR.setVisibility(View.GONE);
        principioR.setVisibility(View.GONE);
        inhaladorR.setVisibility(View.GONE);
        dosisR.setVisibility(View.GONE);
        botonRescate.setVisibility(View.VISIBLE);
        noRescate.setText(R.string.noSeleccRescate);
        botonBorrarRescate.setVisibility(View.GONE);

    }

    //Accion del boton Hecho de crear tratamiento.
    public void onClickBotonHecho(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
        alertDialogBuilder
                .setMessage(R.string.alertDialogTexto)
                .setCancelable(false)
                .setPositiveButton(R.string.alertDialogBotonSi, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Treatment t;
                        if (noRescate.getVisibility() == View.VISIBLE){
                            t = new Treatment(1, recomendaciones.getText().toString(), -1);
                        }else{
                            t = new Treatment(1, recomendaciones.getText().toString(), rescateId);
                        }
                        modelo.vaciarTablaTratamientos();
                        modelo.insertTreatment(t);

                        actualizarListaTomas();
                        NavUtils.navigateUpFromSameTask(CrearTratamiento.this);
                    }
                })
                .setNegativeButton(R.string.alertDialogBotonNo, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    //Insertar cambios en la BD (Boton "HECHO")
    public void actualizarListaTomas() {

        AlarmManager alarmManager = (AlarmManager) getSystemService(CrearTratamiento.ALARM_SERVICE);
        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        Intent refrescarTomasIntent = new Intent(getBaseContext(), ResetTakesReceiver.class);

        for (Toma t : listaTratamientosOriginal) {
            modelo.borrarToma(t);
            int id = (String.valueOf(t.getHour().getHour()) + String.valueOf(t.getHour().getMin())).hashCode();
            PendingIntent pi = PendingIntent.getBroadcast(this.getApplicationContext(), id, intent, PendingIntent.FLAG_NO_CREATE);
            if (pi != null) {
                alarmManager.cancel(pi);
            }
        }

        for (Toma t : listaTratamientosNuevos) {
            modelo.insertarToma(t);

            Calendar now = Calendar.getInstance();
            Calendar alarm = Calendar.getInstance();
            alarm.set(Calendar.HOUR_OF_DAY, t.getHour().getHour());
            alarm.set(Calendar.MINUTE, t.getHour().getMin());
            alarm.set(Calendar.SECOND, 0);
            if (alarm.before(now)) alarm.add(Calendar.DAY_OF_MONTH, 1);

            int id = (String.valueOf(t.getHour().getHour()) + String.valueOf(t.getHour().getMin())).hashCode();
            intent.putExtra("hora", t.getHour().getHour());
            intent.putExtra("min", t.getHour().getMin());
            intent.putExtra("id", id);
            intent.putExtra("veces", 0);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), id, intent, PendingIntent.FLAG_UPDATE_CURRENT);


            System.out.println("Alarm " + id + " is set at " + alarm.getTime());
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                alarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getTimeInMillis(), pendingIntent);
            } else {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarm.getTimeInMillis(), pendingIntent);
            }
        }

        Calendar now = Calendar.getInstance();
        Calendar alarm = Calendar.getInstance();
        alarm.set(Calendar.HOUR_OF_DAY, 23);
        alarm.set(Calendar.MINUTE, 59);
        alarm.set(Calendar.SECOND, 0);
        if (alarm.before(now)) alarm.add(Calendar.DAY_OF_MONTH, 1);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 0, refrescarTomasIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        System.out.println("Alarm de reseteo " + 0 + " is set at " + alarm.getTime());
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getTimeInMillis(), pendingIntent);
        } else {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarm.getTimeInMillis(), pendingIntent);
        }

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return alertExitTreatment();
        }
        //para las demas cosas, se reenvia el evento al listener habitual
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                return alertExitTreatment();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private boolean alertExitTreatment() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.alerDialogTitulo)
                .setMessage(R.string.alertDialogSalirSinGuardar)
                .setNegativeButton(R.string.alertDialogBotonNoSalir, null)//sin listener
                .setPositiveButton(R.string.alertDialogBotonSalir, new DialogInterface.OnClickListener() {//un listener que al pulsar, cierre la aplicacion
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Salir
                        CrearTratamiento.this.finish();
                    }
                })
                .show();
        // Si el listener devuelve true, significa que el evento esta procesado, y nadie debe hacer nada mas
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Comprobamos si el resultado de la segunda actividad es "RESULT_CANCELED".
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            //Toma t = data.getParcelableExtra("newTake");
            ArrayList<Toma> t = data.getParcelableArrayListExtra("newTake");

            for (Toma toma : t){
                listaTratamientosNuevos.add(toma);
                adapterListView.notifyDataSetChanged();
                Commodities.ajustarVistaListView(listView);
            }

        }
    }

    //Accion del boton Añadir de crear tratamiento.
    public void onClickBotonAdd(View view) {
        Intent intent = new Intent(this, AddNewTake.class);
        intent.putParcelableArrayListExtra("listaTomas",listaTratamientosNuevos);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onRestart() {
        super.onRestart();  // Always call the superclass method first

        adapterListView = new AdapterListView(modelo, this, listaTratamientosNuevos, listView);
        listView.setAdapter(adapterListView);

        Commodities.ajustarVistaListView(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getApplicationContext(), ConsultarTomaMedico.class);

                String mNameText = modelo.getMedicineByName(listaTratamientosNuevos.get(position).getMedicamento()).getNameText();
                intent.putExtra(Toma.MEDICAMENTO, mNameText);

                String pNameText = modelo.getActiveIngredientByName(listaTratamientosNuevos.get(position).getPrincipio()).getNameText();
                intent.putExtra(Toma.PRINCIPIO, pNameText);

                String d = (listaTratamientosNuevos.get(position).getDosis()).toString();
                intent.putExtra(Toma.DOSIS, d);

                String h = (listaTratamientosNuevos.get(position).getHour()).toString();
                intent.putExtra(Toma.HOUR, h);

                String inh = Integer.toString(listaTratamientosNuevos.get(position).getInhalations());
                intent.putExtra(Toma.INHALATIONS, inh);

                String inNameText = modelo.getInhalerByName(listaTratamientosNuevos.get(position).getInhalador()).getNameText();
                intent.putExtra(Toma.INHALADOR, inNameText);

                startActivity(intent);
            }
        });

    }

}
