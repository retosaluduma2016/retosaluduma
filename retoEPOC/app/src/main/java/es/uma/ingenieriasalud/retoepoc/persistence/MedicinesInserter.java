package es.uma.ingenieriasalud.retoepoc.persistence;

import android.content.ContentValues;

import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.xmlDatabase.DatabaseXMLParser;

/**
 * Created by jmalvarez on 8/6/16.
 */
public class MedicinesInserter extends DatabaseInserter{

    public MedicinesInserter(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public void insertData(ArrayList<String> elementsList) {

        for (String medicineName : elementsList) {
            int err;
            ContentValues values = new ContentValues();
            values.put(ModeloContract.MedicineTable.NAME, medicineName);
            long rowid = database.insert(ModeloContract.MedicineTable.TABLE_NAME, null, values);
            if (-1 == rowid) {
                err = 1;
            } else {
                err = 0;
            }
        }

    }
}
