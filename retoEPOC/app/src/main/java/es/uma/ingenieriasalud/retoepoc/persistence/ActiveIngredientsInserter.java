package es.uma.ingenieriasalud.retoepoc.persistence;

import android.content.ContentValues;

import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by jmalvarez on 8/6/16.
 */
public class ActiveIngredientsInserter extends DatabaseInserter {
    public ActiveIngredientsInserter(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public void insertData(ArrayList<String> elementsList) {
        for (String actIngrName : elementsList) {
            int err;
            ContentValues values = new ContentValues();
            values.put(ModeloContract.ActiveIngredientTable.NAME, actIngrName);
            long rowid = database.insert(ModeloContract.ActiveIngredientTable.TABLE_NAME, null, values);
            if (-1 == rowid) {
                err = 1;
            } else {
                err = 0;
            }
        }

    }
}
