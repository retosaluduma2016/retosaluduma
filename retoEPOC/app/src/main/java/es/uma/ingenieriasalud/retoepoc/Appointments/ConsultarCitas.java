package es.uma.ingenieriasalud.retoepoc.appointments;

import android.content.DialogInterface;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.persistence.Appointment;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.R;

public class ConsultarCitas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_consultar_citas);

        Modelo modelo = Modelo.getInstance();
        modelo.init(this);

        ArrayList<Appointment> lcitas = modelo.obtenerListaCitas();

        if(lcitas.size()==0){
            final ConsultarCitas cm = this;
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
            alertDialogBuilder
                    .setMessage(R.string.emptyAppointment)
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            NavUtils.navigateUpFromSameTask(cm);
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

        ListView listaCitas = (ListView) findViewById(R.id.listViewCitas);

        ListViewCitasAdapter mAdapter = new ListViewCitasAdapter(this,lcitas);
        listaCitas.setAdapter(mAdapter);

    }
}
