package es.uma.ingenieriasalud.retoepoc.xmlDatabase;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.persistence.DatabaseInserter;

/**
 * Created by jmalvarez on 8/6/16.
 */
public class XmlMedicineParser extends XmlEntryParser {

    private static final String ELEMENTS_TAGNAME = "medicines";
    private static final String SINGLE_ELEMENT_TAGNAME = "medicine";

    private static final String NAME_FIELD_TAGNAME = "name";

    public XmlMedicineParser(DatabaseInserter dbIns, XmlEntryParser n) {
        super(dbIns, n);
        elementsTagName = ELEMENTS_TAGNAME;
    }

    @Override
    void parseElement(XmlPullParser parser, ArrayList<String> elementsList) throws XmlPullParserException, IOException {
        String medicineTagName = parser.getName();
        // Starts by looking for the entry tag
        switch (medicineTagName) {
            case SINGLE_ELEMENT_TAGNAME:
                elementsList.add(readMedicineEntry(parser));
                break;
            default:
                skip(parser);
        }
    }

    // Parses the contents of a medicine. If it encounters a name tag, hands them off
// to the "read" method for processing. Otherwise, skips the tag.
    private String readMedicineEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, SINGLE_ELEMENT_TAGNAME);
        String medName = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case NAME_FIELD_TAGNAME:
                    medName = readTextField(parser, NAME_FIELD_TAGNAME);
                    break;
                default:
                    skip(parser);
            }
        }
        return medName;
    }

}
