/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.persistence;

public class Paso {

    public static final String TABLE_NAME = "UserManualStep";
    public static final String _ID = "_id";
    public static final String TITLE_XML = "title_xml";
    public static final String IMAGE_XML = "image_xml";
    public static final String TEXT_XML = "text_xml";
    public static final String INHALER_ID = "inhaler_id";

    private int id;
    private String titulo;
    private int imagen;
    private String texto;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Paso(int id_,String titulo_,int imagen_,String texto_){
        this.id = id_;
        this.titulo=titulo_;
        this.imagen=imagen_;
        this.texto=texto_;
    }
}
