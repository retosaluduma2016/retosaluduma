package es.uma.ingenieriasalud.retoepoc.persistence;

import android.content.ContentValues;

import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.xmlDatabase.DatabaseXMLParser;

/**
 * Created by jmalvarez on 8/6/16.
 */
public class InhalersInserter extends DatabaseInserter {
    public InhalersInserter(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public void insertData(ArrayList<String> elementsList) {
        for (String inhalerName : elementsList) {
            String[] inhalerParts = inhalerName.split(String.valueOf(DatabaseXMLParser.FIELD_SEPARATOR));
            if (inhalerParts.length == 2) {
                int err;
                ContentValues values = new ContentValues();
                values.put(ModeloContract.InhalerTable.NAME, inhalerParts[0]);
                values.put(ModeloContract.InhalerTable.IMAGE, inhalerParts[1]);
                long rowid = database.insert(ModeloContract.InhalerTable.TABLE_NAME, null, values);
                if (-1 == rowid) {
                    err = 1;
                } else {
                    err = 0;
                }
            } else {
                System.err.println("Error when inserting " + inhalerName + " in DB.");
            }
        }

    }
}
