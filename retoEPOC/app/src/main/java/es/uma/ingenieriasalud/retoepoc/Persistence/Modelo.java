/***********************************************************************
 * This file is part of MyEPOC.
 * <p/>
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import android.provider.BaseColumns;

import java.util.ArrayList;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.xmlDatabase.DatabaseXMLParser;
import es.uma.ingenieriasalud.retoepoc.xmlDatabase.XMLResMedPresParser;
import es.uma.ingenieriasalud.retoepoc.xmlDatabase.XmlActiveIngredientParser;
import es.uma.ingenieriasalud.retoepoc.xmlDatabase.XmlInhalerParser;
import es.uma.ingenieriasalud.retoepoc.xmlDatabase.XmlMedPresentationsParser;
import es.uma.ingenieriasalud.retoepoc.xmlDatabase.XmlMedicineParser;
import es.uma.ingenieriasalud.retoepoc.xmlDatabase.XmlPhysiotherapyParser;
import es.uma.ingenieriasalud.retoepoc.xmlDatabase.XmlPosologiesParser;
import es.uma.ingenieriasalud.retoepoc.xmlDatabase.XmlUserManualInhParser;
import es.uma.ingenieriasalud.retoepoc.xmlDatabase.XmlUserManualPhysioParser;

public class Modelo {

    private DBHelper dbHelper = null;
    private SQLiteDatabase db = null;
    private Context app_context = null;
    private String database_name = null;

    public void init(Context ctx) {
        if (app_context == null && database_name == null && ctx != null) {
            app_context = ctx.getApplicationContext();

            SharedPreferences preferences = app_context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
            String database_name = preferences.getString("databaseName", "");

            SQLiteDatabase.loadLibs(ctx);
            dbHelper = new DBHelper(app_context, database_name);
            if (dbHelper != null) {
                String password = preferences.getString("contrasena", "");
                db = dbHelper.getWritableDatabase(password);
            }
        }
    }

    @Override
    public void finalize() {
        if (dbHelper != null) {
            dbHelper.close();
            dbHelper = null;
        }
        database_name = null;
        app_context = null;
        try {
            super.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }


    private Modelo() {
    }

    private static class SingletonHolder {
        public static final Modelo INSTANCE = new Modelo();
    }

    public static Modelo getInstance() {
        return SingletonHolder.INSTANCE;
    }

    //Insertar toma
    private ContentValues generarValoresToma(Toma t) {
        ContentValues valores = new ContentValues();
        valores.put(ModeloContract.TakeTable.TREATMENT_ID, 1);
        valores.put(ModeloContract.TakeTable.MEDICINE_NAME, t.getMedicamento());
        valores.put(ModeloContract.TakeTable.ACTIVEINGREDIENT_NAME, t.getPrincipio());
        valores.put(ModeloContract.TakeTable.INHALER_NAME, t.getInhalador());
        valores.put(ModeloContract.TakeTable.DOSE_AI1, t.getDosis().getCantidad1());
        valores.put(ModeloContract.TakeTable.DOSE_AI2, t.getDosis().getCantidad2());
        valores.put(ModeloContract.TakeTable.HOUR, t.getHour().getHour());
        valores.put(ModeloContract.TakeTable.MINUTE, t.getHour().getMin());
        if (t.isDone()) {
            valores.put(ModeloContract.TakeTable.DONE, 1);
        } else {
            valores.put(ModeloContract.TakeTable.DONE, 0);
        }
        valores.put(ModeloContract.TakeTable.INHALATIONS, t.getInhalations());


        return valores;
    }

    public void insertarToma(Toma t) {
        db.insert(ModeloContract.TakeTable.TABLE_NAME, null, generarValoresToma(t));
    }

    public void vaciarTabla() {
        db.execSQL("DELETE FROM " + ModeloContract.TakeTable.TABLE_NAME);
    }

    public void vaciarTablaDatos() {
        db.execSQL("DELETE FROM " + ModeloContract.PatientPersonalDataTable.TABLE_NAME);
    }

    public void vaciarTablaTratamientos() {
        db.execSQL("DELETE FROM " + ModeloContract.TreatmentTable.TABLE_NAME);
    }


    public void borrarTabla() {
        dbHelper.onUpgrade(db, 1, 2);
    }

    public void borrarToma(Toma t) {
        String[] args = new String[]{Integer.toString(t.getId())};
        db.execSQL("DELETE FROM " + ModeloContract.TakeTable.TABLE_NAME + " WHERE " + ModeloContract.TakeTable._ID + "=?", args);
    }

    public void borrarCita(Appointment a) {
        String[] args = new String[]{(a.getId())};
        db.execSQL("DELETE FROM " + ModeloContract.AppointmentDataTable.TABLE_NAME + " WHERE " + ModeloContract.AppointmentDataTable._ID + "=?", args);
    }

    //Cargar la lista de tomas
    public ArrayList<Toma> obtenerListaTomas() {
        String columnas[] = {ModeloContract.TakeTable._ID, ModeloContract.TakeTable.TREATMENT_ID,
                ModeloContract.TakeTable.MEDICINE_NAME, ModeloContract.TakeTable.ACTIVEINGREDIENT_NAME,
                ModeloContract.TakeTable.INHALER_NAME, ModeloContract.TakeTable.DOSE_AI1,
                ModeloContract.TakeTable.DOSE_AI2,
                ModeloContract.TakeTable.HOUR,
                ModeloContract.TakeTable.MINUTE,
                ModeloContract.TakeTable.DONE,
                ModeloContract.TakeTable.INHALATIONS};
        ArrayList<Toma> lt = new ArrayList<>();
        Cursor crs = db.query(ModeloContract.TakeTable.TABLE_NAME, columnas, null, null, null, null, null);
        while (crs.moveToNext()) {
            //DB Table Take: _id, treatment_id, MedicineTable.name, ActiveIngredientTable.name, InhalerTable.name, dose_id, posology_id
            //    public Toma(int id, int medicamento, int principio, int inhalador, int dosis, int posologia)
            Toma t = new Toma(crs.getInt(0), crs.getString(2), crs.getString(3), crs.getString(4),
                    new Dosis(crs.getFloat(5), crs.getFloat(6)),
                    new Hour(crs.getInt(7), crs.getInt(8)), crs.getInt(9) == 1, crs.getInt(10));
            lt.add(t);
        }
        crs.close();
        return lt;
    }

    public ArrayList<Appointment> obtenerListaCitas() {
        String columnas[] = {ModeloContract.AppointmentDataTable._ID, ModeloContract.AppointmentDataTable.DAY,
                ModeloContract.AppointmentDataTable.MONTH, ModeloContract.AppointmentDataTable.YEAR,
                ModeloContract.AppointmentDataTable.HOUR, ModeloContract.AppointmentDataTable.MIN,
                ModeloContract.AppointmentDataTable.LOCATION,
                ModeloContract.AppointmentDataTable.DOCTOR_NAME,
                ModeloContract.AppointmentDataTable.DIAGNOSTIC_TEST,
                ModeloContract.AppointmentDataTable.VALORATION_TEST};
        ArrayList<Appointment> ap = new ArrayList<>();
        Cursor crs = db.query(ModeloContract.AppointmentDataTable.TABLE_NAME, columnas, null, null, null, null, null);
        while (crs.moveToNext()) {
            //DB Table Take: _id, treatment_id, MedicineTable.name, ActiveIngredientTable.name, InhalerTable.name, dose_id, posology_id
            //    public Toma(int id, int medicamento, int principio, int inhalador, int dosis, int posologia)
            Appointment a = new Appointment(crs.getString(0), crs.getString(1), crs.getString(2), crs.getString(3), crs.getString(4), crs.getString(5), crs.getString(6), crs.getString(7), crs.getString(8), crs.getString(9));
            ap.add(a);
        }
        crs.close();
        return ap;
    }

    // Retrieve from DB all rows from table Medicine
    public ArrayList<Medicamento> obtenerListaMedicamentos() {
        String columnas[] = {ModeloContract.MedicineTable._ID, ModeloContract.MedicineTable.NAME};
        Cursor cursorMedicamento = db.query(ModeloContract.MedicineTable.TABLE_NAME, columnas, null, null, null, null, null);
        ArrayList<Medicamento> lmed = new ArrayList<>();
        while (cursorMedicamento.moveToNext()) {
            String medName = cursorMedicamento.getString(1);
            String medNameText = Commodities.retrieveResourceStringFromId(app_context, medName);
            lmed.add(new Medicamento(cursorMedicamento.getInt(0), medName, medNameText));
        }
        cursorMedicamento.close();
        return lmed;

    }

    // Retrieve from DB all rows from table Medicine
    public ArrayList<PresentacionMedicacionRescate> obtenerListaMedicamentosRescate() {
        String columnas[] = {ModeloContract.RescueTable._ID, ModeloContract.RescueTable.MEDICINE_NAME,
                ModeloContract.RescueTable.ACTIVEINGREDIENT_NAME,
                ModeloContract.RescueTable.INHALER_NAME, ModeloContract.RescueTable.DOSE_AI1,
                ModeloContract.RescueTable.DOSE_AI2};
        Cursor resMedPres = db.query(ModeloContract.RescueTable.TABLE_NAME, columnas, null, null, null, null, null);
/*
        Cursor resMedPres = db.rawQuery("SELECT * FROM " + ModeloContract.RescueTable.TABLE_NAME, null);
*/
        ArrayList<PresentacionMedicacionRescate> lrespres = new ArrayList<>();

        while (resMedPres.moveToNext()) {
            String medName = resMedPres.getString(1);
            String medNameText = Commodities.retrieveResourceStringFromId(app_context, medName);
            String aiName = resMedPres.getString(2);
            String aiNameText = Commodities.retrieveResourceStringFromId(app_context, aiName);
            String inhName = resMedPres.getString(3);
            String inhNameText = Commodities.retrieveResourceStringFromId(app_context, inhName);
            lrespres.add(new PresentacionMedicacionRescate(resMedPres.getInt(0), medNameText,
                    aiNameText, inhNameText,
                    resMedPres.getFloat(4),
                    resMedPres.getFloat(5)));
        }
        resMedPres.close();
        return lrespres;

    }

    public ArrayList<Inhalador> obtenerListaInhaladoresByMedicine(String name) {
        // Inhaler columns: id, name, image
        Cursor cursorInhalador =
                db.rawQuery("Select Distinct " + ModeloContract.InhalerTable.TABLE_NAME + ".* from " +
                                ModeloContract.InhalerTable.TABLE_NAME + ", " +
                                ModeloContract.MedicinePresentationTable.TABLE_NAME + " where " +
                                ModeloContract.InhalerTable.TABLE_NAME + "." +
                                ModeloContract.InhalerTable.NAME + " = " +
                                ModeloContract.MedicinePresentationTable.TABLE_NAME + "." +
                                ModeloContract.MedicinePresentationTable.INHALER_NAME + " and " +
                                ModeloContract.MedicinePresentationTable.TABLE_NAME + "." +
                                ModeloContract.MedicinePresentationTable.MEDICINE_NAME + " = ?",
                        new String[]{name});
        ArrayList<Inhalador> listaInhaladores = new ArrayList<>();
        while (cursorInhalador.moveToNext()) {
            String inhName = cursorInhalador.getString(1);
            String inhNameText = Commodities.retrieveResourceStringFromId(app_context, inhName);
            Inhalador inhalador = new Inhalador(cursorInhalador.getInt(0), inhName, inhNameText, cursorInhalador.getString(2), null);
            listaInhaladores.add(inhalador);
        }
        cursorInhalador.close();
        return listaInhaladores;
    }

    public ArrayList<PrincipioActivo> obtenerListaPrincipiosActivosByMedicine(String name) {
        // ActiveIngredient columns: id, name
        Cursor cursorPrincipioActivo =
                db.rawQuery("Select Distinct " +
                                ModeloContract.ActiveIngredientTable.TABLE_NAME + ".* from " +
                                ModeloContract.ActiveIngredientTable.TABLE_NAME + ", " +
                                ModeloContract.MedicinePresentationTable.TABLE_NAME + " where " +
                                ModeloContract.ActiveIngredientTable.TABLE_NAME + "." +
                                ModeloContract.ActiveIngredientTable.NAME + " = " +
                                ModeloContract.MedicinePresentationTable.TABLE_NAME + "." +
                                ModeloContract.MedicinePresentationTable.ACTIVEINGREDIENT_NAME + " and " +
                                ModeloContract.MedicinePresentationTable.TABLE_NAME + "." +
                                ModeloContract.MedicinePresentationTable.MEDICINE_NAME + " = ?",
                        new String[]{name});
        ArrayList<PrincipioActivo> listaPrincipios = new ArrayList<>();
        while (cursorPrincipioActivo.moveToNext()) {
            String paName = cursorPrincipioActivo.getString(1);
            String paNameText = Commodities.retrieveResourceStringFromId(app_context, paName);
            PrincipioActivo actIngr = new PrincipioActivo(cursorPrincipioActivo.getInt(0), paName, paNameText);
            listaPrincipios.add(actIngr);
        }
        cursorPrincipioActivo.close();
        return listaPrincipios;
    }

    // Retrieve from DB table Medicine all rows for a given Active Ingredient ID
    public ArrayList<Dosis> obtenerListaDosisByMedicine(String name) {
        Cursor cursorDosis =
                db.rawQuery("Select Distinct " +
                        ModeloContract.MedicinePresentationTable.TABLE_NAME + "." +
                        ModeloContract.MedicinePresentationTable.DOSE_AI1 + ", " +
                        ModeloContract.MedicinePresentationTable.TABLE_NAME + "." +
                        ModeloContract.MedicinePresentationTable.DOSE_AI2 + " from " +
                        ModeloContract.MedicinePresentationTable.TABLE_NAME + " where " +
                        ModeloContract.MedicinePresentationTable.TABLE_NAME + "." +
                        ModeloContract.MedicinePresentationTable.MEDICINE_NAME +
                        "= ?", new String[]{name});
        ArrayList<Dosis> listaDosis = new ArrayList<>();
        while (cursorDosis.moveToNext()) {
            Dosis ds = new Dosis(cursorDosis.getDouble(0), cursorDosis.getDouble(1));
            listaDosis.add(ds);
        }
        cursorDosis.close();
        return listaDosis;
    }

    // Retrieve Medicine from id
    public Medicamento getMedicineByName(String name) {
        // Medicine columns : _id (int), name (text)
        String rawQueryText = "SELECT * FROM " +
                ModeloContract.MedicineTable.TABLE_NAME + " WHERE " +
                ModeloContract.MedicineTable.NAME + " = ?";
        Cursor cmed = db.rawQuery(rawQueryText, new String[]{name});
        Medicamento medic;
        if (0 < cmed.getCount()) {
            cmed.moveToNext();
            String medName = cmed.getString(1);
            String medNameText = Commodities.retrieveResourceStringFromId(app_context, medName);
            medic = new Medicamento(cmed.getInt(0), medName, medNameText);
        } else {
            medic = null;
        }
        cmed.close();
        return medic;
    }

    // Retrieve Treatment by id
    public Treatment getTreatmentById(int id) {

        String[] columns = {ModeloContract.TreatmentTable._ID,
                ModeloContract.TreatmentTable.RECOMMENDATION,
                ModeloContract.TreatmentTable.RESCUE_ID};
        Cursor t = db.query(ModeloContract.TreatmentTable.TABLE_NAME, columns, null, null, null, null, null);

        Treatment tr;
        if (0 < t.getCount()) {
            t.moveToNext();
            int treatmentId = t.getInt(0);
            String treatmentRecomend = t.getString(1);
            int treatmentIdRescue = t.getInt(2);

            tr = new Treatment(treatmentId, treatmentRecomend, treatmentIdRescue);
        } else {
            tr = null;
        }
        t.close();
        return tr;
    }

    // Retrieve Rescue by id
    public PresentacionMedicacionRescate getRescueById(int id) {

        String[] columns = {ModeloContract.RescueTable._ID,
                ModeloContract.RescueTable.MEDICINE_NAME,
                ModeloContract.RescueTable.ACTIVEINGREDIENT_NAME,
                ModeloContract.RescueTable.INHALER_NAME,
                ModeloContract.RescueTable.DOSE_AI1,
                ModeloContract.RescueTable.DOSE_AI2};


        /*Cursor t = db.query(ModeloContract.RescueTable.TABLE_NAME,columns,ModeloContract.RescueTable._ID,
                new String[] { String.valueOf(id) },null,null,null);*/
        Cursor t = db.rawQuery("SELECT  * FROM " +
                        ModeloContract.RescueTable.TABLE_NAME + " WHERE " +
                        ModeloContract.RescueTable._ID + " = ?",
                new String[]{Integer.toString(id)});

        PresentacionMedicacionRescate tr;
        if (0 < t.getCount()) {
            t.moveToNext();
            int rescueId = t.getInt(0);
            String medicine = t.getString(1);
            String activeIngredient = t.getString(2);
            String inhaler = t.getString(3);
            float dose1 = t.getFloat(4);
            float dose2 = t.getFloat(5);

            tr = new PresentacionMedicacionRescate(rescueId, medicine, activeIngredient, inhaler, dose1, dose2);
        } else {
            tr = null;
        }
        t.close();
        return tr;
    }


    //Obtener principio
    public PrincipioActivo getActiveIngredientByName(String name) {
        // ActiveIngredient columns : _id (int), name (text)
        Cursor cActIng = db.rawQuery("SELECT  * FROM " +
                        ModeloContract.ActiveIngredientTable.TABLE_NAME + " WHERE " +
                        ModeloContract.ActiveIngredientTable.NAME + " = ?",
                new String[]{name});
        PrincipioActivo actIngr;
        if (0 < cActIng.getCount()) {
            cActIng.moveToNext();
            String paName = cActIng.getString(1);
            String paNameText = Commodities.retrieveResourceStringFromId(app_context, paName);
            actIngr = new PrincipioActivo(cActIng.getInt(0), paName, paNameText);
        } else {
            actIngr = null;
        }
        cActIng.close();
        return actIngr;
    }

    // Retrieve inhaler for a given id
    public Inhalador obtenerInhalador(int i) {

        Cursor curInh = db.rawQuery("SELECT * FROM " +
                        ModeloContract.InhalerTable.TABLE_NAME + " WHERE " +
                        ModeloContract.InhalerTable._ID + " = ?",
                new String[]{String.valueOf(i)});
        // Inhaler columns : _id (int), name_ModeloContract.InhalerTable.xml (text), image (text)
        Inhalador inh;
        if (0 < curInh.getCount()) {
            curInh.moveToNext();
            String inhName = curInh.getString(1);
            String inhNameText = Commodities.retrieveResourceStringFromId(app_context, inhName);
            inh = new Inhalador(curInh.getInt(0), inhName, inhNameText, curInh.getString(2), null);
        } else {
            inh = null;
        }
        curInh.close();
        return inh;
    }

    // Retrieve inhaler for a given name
    public Inhalador getInhalerByName(String name) {

        Cursor curInh = db.rawQuery("SELECT * FROM " +
                        ModeloContract.InhalerTable.TABLE_NAME + " WHERE " +
                        ModeloContract.InhalerTable.NAME + " = ?",
                new String[]{name});
        // Inhaler columns : _id (int), name_ModeloContract.InhalerTable.xml (text), image (text)
        Inhalador inh;
        if (0 < curInh.getCount()) {
            curInh.moveToNext();
            String inhName = curInh.getString(1);
            String inhNameText = Commodities.retrieveResourceStringFromId(app_context, inhName);
            inh = new Inhalador(curInh.getInt(0), inhName, inhNameText, curInh.getString(2), null);
        } else {
            inh = null;
        }
        curInh.close();
        return inh;
    }

    //Obtener posologia
    public Posologia getPosologyByDescription(String desc) {
        Cursor cursorPosol = db.rawQuery("SELECT * FROM " +
                        ModeloContract.PosologyTable.TABLE_NAME + " WHERE " +
                        ModeloContract.PosologyTable.TEXT_FREQUENCY_XML + " = ?",
                new String[]{desc});
        // Posology columns : _id (int), text_frequency_xml (text)
        Posologia pos;
        if (0 < cursorPosol.getCount()) {
            cursorPosol.moveToNext();
            String posolName = cursorPosol.getString(1);
            String posolStringsText = Commodities.retrieveResourceStringFromId(app_context, posolName);
            pos = new Posologia(cursorPosol.getInt(0), posolName, posolStringsText);
        } else {
            pos = null;
        }
        cursorPosol.close();
        return pos;
    }

    public PatientPersonalData getPatientPersonalData() {
        String columnas[] = {ModeloContract.PatientPersonalDataTable.NAME,
                ModeloContract.PatientPersonalDataTable.SURNAME,
                ModeloContract.PatientPersonalDataTable.NSS,
                ModeloContract.PatientPersonalDataTable.HEIGHT,
                ModeloContract.PatientPersonalDataTable.WEIGHT,
                ModeloContract.PatientPersonalDataTable.AGE,

                ModeloContract.PatientPersonalDataTable.SMOKER,
                ModeloContract.PatientPersonalDataTable.DIABETIC,
                ModeloContract.PatientPersonalDataTable.SICKNESS,
                ModeloContract.PatientPersonalDataTable.SICKNESS_STATE,
                ModeloContract.PatientPersonalDataTable.ALERGY,

                ModeloContract.PatientPersonalDataTable.FAMILY_DOCTOR,
                ModeloContract.PatientPersonalDataTable.MEDICAL_SPECIALIST,
                ModeloContract.PatientPersonalDataTable.CASUAL_CAREGIVER_NAME,
                ModeloContract.PatientPersonalDataTable.CASUAL_CAREGIVER_PHONE,
                ModeloContract.PatientPersonalDataTable.CASUAL_CAREGIVER_RELATION,
                ModeloContract.PatientPersonalDataTable.NURSE_IN_CHARGE
        };
        Cursor cursorPPD = db.query(ModeloContract.PatientPersonalDataTable.TABLE_NAME,
                columnas, null, null, null, null, null);
        PatientPersonalData ppd;
        if (0 < cursorPPD.getCount()) {
            cursorPPD.moveToNext();
            ppd = new PatientPersonalData(cursorPPD.getString(0), cursorPPD.getString(1),
                    cursorPPD.getString(2), cursorPPD.getString(3),
                    cursorPPD.getString(4), cursorPPD.getString(5),
                    cursorPPD.getInt(6) == 1, cursorPPD.getInt(7) == 1
                    , cursorPPD.getString(8), cursorPPD.getString(9)
                    , cursorPPD.getString(10), cursorPPD.getString(11)
                    , cursorPPD.getString(12), cursorPPD.getString(13)
                    , cursorPPD.getString(14), cursorPPD.getString(15)
                    , cursorPPD.getString(16));
        } else {
            ppd = null;
        }
        cursorPPD.close();
        return ppd;

    }

    public void cambiarEstadoTomaAlarma(boolean check, int id) {

//        db.execSQL("UPDATE TAKE SET "+ModeloContract.TakeTable.DONE + " = " + (check?1:0) + " WHERE " +
//                   ModeloContract.TakeTable._ID +" = "+String.valueOf(id)+";");
        if (check) {

            db.execSQL("UPDATE TAKE SET " + ModeloContract.TakeTable.DONE + " = 0 WHERE " + ModeloContract.TakeTable._ID + " = " + String.valueOf(id) + ";");
            //System.out.println("UPDATE TAKE SET " + ModeloContract.TakeTable.DONE + " = 0 WHERE " + ModeloContract.TakeTable._ID + " = " + String.valueOf(id));

        } else {

            db.execSQL("UPDATE TAKE SET " + ModeloContract.TakeTable.DONE + " = 1 WHERE " + ModeloContract.TakeTable._ID + " = " + String.valueOf(id) + ";");
            //System.out.println("UPDATE TAKE SET " + ModeloContract.TakeTable.DONE + " = 1 WHERE " + ModeloContract.TakeTable._ID + " = " + String.valueOf(id) + ";");
        }

    }

    public void resetearEstadoTomas() {

        db.execSQL("UPDATE TAKE SET " + ModeloContract.TakeTable.DONE + " = 0" + ";");

    }

    // Retrieve from DB all rows from table Physiotherapy
    public ArrayList<Fisioterapia> obtenerListaFisioterapia() {
        String columnas[] = {ModeloContract.PhysiotherapyTable._ID, ModeloContract.PhysiotherapyTable.NAME, ModeloContract.PhysiotherapyTable.IMAGE};
        Cursor cursorPhysio = db.query(ModeloContract.PhysiotherapyTable.TABLE_NAME, columnas, null, null, null, null, null);
        ArrayList<Fisioterapia> fisioterapias = new ArrayList<>();
        while (cursorPhysio.moveToNext()) {
            String physioName = cursorPhysio.getString(1);
            String physioText = Commodities.retrieveResourceStringFromId(app_context, physioName);
            fisioterapias.add(new Fisioterapia(cursorPhysio.getInt(0), physioName, physioText, cursorPhysio.getString(2), null));
        }
        cursorPhysio.close();
        return fisioterapias;
    }

    // Retrieve from DB all rows from table Inhaler
    public ArrayList<Inhalador> obtenerListaInhaladoresUsuario() {

        String query = "SELECT " +
                ModeloContract.InhalerTable.TABLE_NAME + "." +
                ModeloContract.InhalerTable._ID + ", " +
                ModeloContract.InhalerTable.TABLE_NAME + "." +
                ModeloContract.InhalerTable.NAME + ", " +
                ModeloContract.InhalerTable.TABLE_NAME + "." +
                ModeloContract.InhalerTable.IMAGE + " FROM " +
                ModeloContract.TreatmentTable.TABLE_NAME + ", " +
                ModeloContract.TakeTable.TABLE_NAME + ", " +
                ModeloContract.InhalerTable.TABLE_NAME + " WHERE " +
                ModeloContract.TakeTable.TABLE_NAME + "." +
                ModeloContract.TakeTable.TREATMENT_ID + " = " +
                ModeloContract.TreatmentTable.TABLE_NAME + "." +
                ModeloContract.TreatmentTable._ID + " AND " +
                ModeloContract.TakeTable.TABLE_NAME + "." +
                ModeloContract.TakeTable.INHALER_NAME + " = " +
                ModeloContract.InhalerTable.TABLE_NAME + "." +
                ModeloContract.InhalerTable.NAME;

//        query = "SELECT Inhaler._id, Inhaler.name_inhaler_xml, Inhaler.image FROM Take, Inhaler WHERE Take.inhaler_id = Inhaler._id";

        Cursor cursorInhalador = db.rawQuery(query, null);

        ArrayList<Inhalador> listaInhaladores = new ArrayList<>();
        while (cursorInhalador.moveToNext()) {
            String inhName = cursorInhalador.getString(1);
            String inhNameText = Commodities.retrieveResourceStringFromId(app_context, inhName);
            Inhalador inhalador = new Inhalador(cursorInhalador.getInt(0), inhName, inhNameText, cursorInhalador.getString(2), null);
            listaInhaladores.add(inhalador);
        }
        cursorInhalador.close();
        return listaInhaladores;
    }

    public ArrayList<Paso> obtenerPasosInhalador(String name) {
        Cursor cursorPasos = db.rawQuery("SELECT * FROM USERMANUALSTEP WHERE INHALER_NAME =?",
                new String[]{name});
        ArrayList<Paso> listaPasos = new ArrayList<>();
        while (cursorPasos.moveToNext()) {
            Paso p = new Paso(cursorPasos.getInt(0),
                    cursorPasos.getString(1),
                    app_context.getResources().getIdentifier(cursorPasos.getString(2), "drawable",
                            app_context.getPackageName()),
                    cursorPasos.getString(3));
            listaPasos.add(p);
        }
        cursorPasos.close();
        return listaPasos;
    }

    public ArrayList<Paso> obtenerPasosFisioterapia(String name) {
        Cursor cursorPasos = db.rawQuery("SELECT * FROM USERMANUALPHYSIOSTEP WHERE PHYSIO_NAME =?",
                new String[]{name});
        ArrayList<Paso> listaPasos = new ArrayList<>();
        while (cursorPasos.moveToNext()) {
            Paso p = new Paso(cursorPasos.getInt(0),
                    cursorPasos.getString(1),
                    app_context.getResources().getIdentifier(cursorPasos.getString(2), "drawable",
                            app_context.getPackageName()),
                    cursorPasos.getString(3));
            listaPasos.add(p);
        }
        cursorPasos.close();
        return listaPasos;
    }

    public Cursor obtenerListaTomasUsuario() {

        return db.rawQuery("SELECT * FROM OBTENERLISTATOMASUSUARIO", null);
    }

    public int insertMedicineRow(SQLiteDatabase db, String medName) {
        int err;
        ContentValues values = new ContentValues();
        values.put(ModeloContract.MedicineTable.NAME, medName);
        long rowid = db.insert(ModeloContract.MedicineTable.TABLE_NAME, null, values);
        if (-1 == rowid) {
            err = 1;
        } else {
            err = 0;
        }
        return err;
    }

    public int insertActiveIngredientRow(SQLiteDatabase db, String aiName) {
        int err;
        ContentValues values = new ContentValues();
        values.put(ModeloContract.ActiveIngredientTable.NAME, aiName);
        long rowid = db.insert(ModeloContract.ActiveIngredientTable.TABLE_NAME, null, values);
        if (-1 == rowid) {
            err = 1;
        } else {
            err = 0;
        }
        return err;
    }

    public int insertInhalerRow(SQLiteDatabase db, String inhName, String imgFile) {
        int err;
        ContentValues values = new ContentValues();
        values.put(ModeloContract.InhalerTable.NAME, inhName);
        values.put(ModeloContract.InhalerTable.IMAGE, imgFile);
        long rowid = db.insert(ModeloContract.InhalerTable.TABLE_NAME, null, values);
        if (-1 == rowid) {
            err = 1;
        } else {
            err = 0;
        }
        return err;
    }

    public int insertPhsyotherapyrRow(SQLiteDatabase db, String phyhName, String imgFile) {
        int err;
        ContentValues values = new ContentValues();
        values.put(ModeloContract.PhysiotherapyTable.NAME, phyhName);
        values.put(ModeloContract.PhysiotherapyTable.IMAGE, imgFile);
        long rowid = db.insert(ModeloContract.PhysiotherapyTable.TABLE_NAME, null, values);
        if (-1 == rowid) {
            err = 1;
        } else {
            err = 0;
        }
        return err;
    }

    public int insertPosologyRow(SQLiteDatabase db, String freq) {
        int err;
        ContentValues values = new ContentValues();
        values.put(ModeloContract.PosologyTable.TEXT_FREQUENCY_XML, freq);
        long rowid = db.insert(ModeloContract.PosologyTable.TABLE_NAME, null, values);
        if (-1 == rowid) {
            err = 1;
        } else {
            err = 0;
        }
        return err;
    }

    public int insertMedicinePresentationRow(SQLiteDatabase db, String med_name, String ai_name,
                                              String inh_name, double dose_ai1, double dose_ai2,
                                              String posol_name) {
        int err;
        ContentValues values = new ContentValues();
        values.put(ModeloContract.MedicinePresentationTable.MEDICINE_NAME, med_name);
        values.put(ModeloContract.MedicinePresentationTable.ACTIVEINGREDIENT_NAME, ai_name);
        values.put(ModeloContract.MedicinePresentationTable.INHALER_NAME, inh_name);
        values.put(ModeloContract.MedicinePresentationTable.DOSE_AI1, dose_ai1);
        values.put(ModeloContract.MedicinePresentationTable.DOSE_AI2, dose_ai2);
        values.put(ModeloContract.MedicinePresentationTable.POSOLOGY_NAME, posol_name);
        long rowid = db.insert(ModeloContract.MedicinePresentationTable.TABLE_NAME, null, values);
        if (-1 == rowid) {
            err = 1;
        } else {
            err = 0;
        }
        return err;
    }

    public int insertRescueRow(SQLiteDatabase db, String med_name, String ai_name,
                                String inh_name, double dose_ai1, double dose_ai2) {
        int err;
        ContentValues values = new ContentValues();
        values.put(ModeloContract.RescueTable.MEDICINE_NAME, med_name);
        values.put(ModeloContract.RescueTable.ACTIVEINGREDIENT_NAME, ai_name);
        values.put(ModeloContract.RescueTable.INHALER_NAME, inh_name);
        values.put(ModeloContract.RescueTable.DOSE_AI1, dose_ai1);
        values.put(ModeloContract.RescueTable.DOSE_AI2, dose_ai2);
        long rowid = db.insert(ModeloContract.RescueTable.TABLE_NAME, null, values);
        if (-1 == rowid) {
            err = 1;
        } else {
            err = 0;
        }
        return err;
    }

    public int insertUserManualStepRow(SQLiteDatabase db, String title, String image,
                                        String text, String inh_name) {
        int err;
        ContentValues values = new ContentValues();
        values.put(ModeloContract.UserManualStepTable.TITLE_XML, title);
        values.put(ModeloContract.UserManualStepTable.IMAGE_XML, image);
        values.put(ModeloContract.UserManualStepTable.TEXT_XML, text);
        values.put(ModeloContract.UserManualStepTable.INHALER_NAME, inh_name);
        long rowid = db.insert(ModeloContract.UserManualStepTable.TABLE_NAME, null, values);
        if (-1 == rowid) {
            err = 1;
        } else {
            err = 0;
        }
        return err;
    }

    public void insertTreatment(Treatment t) {
        this.insertTreatmentRow(db, t.getId(), t.getRecomendaciones(), t.getRescate());
    }

    public int insertUserManualPhysioStepRow(SQLiteDatabase db, String title, String image,
                                              String text, String phy_name) {
        int err;
        ContentValues values = new ContentValues();
        values.put(ModeloContract.UserManualPhysioStepTable.TITLE_XML, title);
        values.put(ModeloContract.UserManualPhysioStepTable.IMAGE_XML, image);
        values.put(ModeloContract.UserManualPhysioStepTable.TEXT_XML, text);
        values.put(ModeloContract.UserManualPhysioStepTable.PHYSIO_NAME, phy_name);
        long rowid = db.insert(ModeloContract.UserManualPhysioStepTable.TABLE_NAME, null, values);
        if (-1 == rowid) {
            err = 1;
        } else {
            err = 0;
        }
        return err;
    }

    private int insertTreatmentRow(SQLiteDatabase db, int id, String recommendation, int rescue) {
        int err;
        ContentValues values = new ContentValues();
        values.put(ModeloContract.TreatmentTable._ID, id);
        values.put(ModeloContract.TreatmentTable.RECOMMENDATION, recommendation);
        values.put(ModeloContract.TreatmentTable.RESCUE_ID, rescue);
        long rowid = db.insert(ModeloContract.TreatmentTable.TABLE_NAME, null, values);
        if (-1 == rowid) {
            err = 1;
        } else {
            err = 0;
        }
        return err;

    }

    public void insertPatientPersonalData(PatientPersonalData p) {
        ContentValues values = new ContentValues();
        values.put(ModeloContract.PatientPersonalDataTable.NAME, p.getName());
        values.put(ModeloContract.PatientPersonalDataTable.SURNAME, p.getSurname());
        values.put(ModeloContract.PatientPersonalDataTable.NSS, p.getNss());
        values.put(ModeloContract.PatientPersonalDataTable.HEIGHT  , p.getHeight());
        values.put(ModeloContract.PatientPersonalDataTable.WEIGHT, p.getPeso());
        values.put(ModeloContract.PatientPersonalDataTable.AGE, p.getEdad());

        //values.put(ModeloContract.PatientPersonalDataTable.SMOKER, p.isFumador());
        if ( p.isFumador()) {
            values.put(ModeloContract.PatientPersonalDataTable.SMOKER, 1);
        } else {
            values.put(ModeloContract.PatientPersonalDataTable.SMOKER, 0);
        }

        //values.put(ModeloContract.PatientPersonalDataTable.DIABETIC, p.isDiabetico());
        if ( p.isDiabetico()) {
            values.put(ModeloContract.PatientPersonalDataTable.DIABETIC, 1);
        } else {
            values.put(ModeloContract.PatientPersonalDataTable.DIABETIC, 0);
        }
        values.put(ModeloContract.PatientPersonalDataTable.SICKNESS, p.getEnfermedades());
        values.put(ModeloContract.PatientPersonalDataTable.SICKNESS_STATE, p.getSicknessState());
        values.put(ModeloContract.PatientPersonalDataTable.ALERGY, p.getAlergy());

        values.put(ModeloContract.PatientPersonalDataTable.FAMILY_DOCTOR, p.getMedicoFamilia());
        values.put(ModeloContract.PatientPersonalDataTable.MEDICAL_SPECIALIST, p.getMedicoEspecialista());
        values.put(ModeloContract.PatientPersonalDataTable.CASUAL_CAREGIVER_NAME, p.getCasualCaregiverName());
        values.put(ModeloContract.PatientPersonalDataTable.CASUAL_CAREGIVER_PHONE, p.getCasualCaregiverPhone());
        values.put(ModeloContract.PatientPersonalDataTable.CASUAL_CAREGIVER_RELATION, p.getRelacionCuidador());
        values.put(ModeloContract.PatientPersonalDataTable.NURSE_IN_CHARGE, p.getNurseInCharge());


        db.insert(ModeloContract.PatientPersonalDataTable.TABLE_NAME, null, values);
    }

    public void insertAppointment(Appointment ap){
        ContentValues values = new ContentValues();
        values.put(ModeloContract.AppointmentDataTable.DAY, ap.getDia());
        values.put(ModeloContract.AppointmentDataTable.MONTH, ap.getMes());
        values.put(ModeloContract.AppointmentDataTable.YEAR, ap.getAño());
        values.put(ModeloContract.AppointmentDataTable.HOUR, ap.getHora());
        values.put(ModeloContract.AppointmentDataTable.MIN, ap.getMin());
        values.put(ModeloContract.AppointmentDataTable.LOCATION, ap.getEsp());
        values.put(ModeloContract.AppointmentDataTable.DOCTOR_NAME, ap.getN_doctor());
        values.put(ModeloContract.AppointmentDataTable.DIAGNOSTIC_TEST, ap.getP_diag());
        values.put(ModeloContract.AppointmentDataTable.VALORATION_TEST, ap.getP_val());
        db.insert(ModeloContract.AppointmentDataTable.TABLE_NAME, null, values);
    }

    public class DBHelper extends SQLiteOpenHelper {

        public static final int DATABASE_VERSION = 2;

        private static final String SQL_CREATE_MEDICINE_TABLE =
                "CREATE TABLE " + ModeloContract.MedicineTable.TABLE_NAME + " (" +
                        ModeloContract.MedicineTable._ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        ModeloContract.MedicineTable.NAME + " TEXT NOT NULL UNIQUE" +
                        " )";

        private static final String SQL_CREATE_ACTIVEINGREDIENT_TABLE =
                "CREATE TABLE " + ModeloContract.ActiveIngredientTable.TABLE_NAME + " (" +
                        ModeloContract.ActiveIngredientTable._ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        ModeloContract.ActiveIngredientTable.NAME + " TEXT NOT NULL UNIQUE" +
                        " )";

        private static final String SQL_CREATE_PHYSIOTERAPY_TABLE =
                "CREATE TABLE " + ModeloContract.PhysiotherapyTable.TABLE_NAME + " (" +
                        ModeloContract.PhysiotherapyTable._ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        ModeloContract.PhysiotherapyTable.NAME + " TEXT NOT NULL UNIQUE, " +
                        ModeloContract.PhysiotherapyTable.IMAGE + " TEXT NOT NULL" +
                        " )";
        private static final String SQL_CREATE_INHALER_TABLE =
                "CREATE TABLE " + ModeloContract.InhalerTable.TABLE_NAME + " (" +
                        ModeloContract.InhalerTable._ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        ModeloContract.InhalerTable.NAME + " TEXT NOT NULL UNIQUE, " +
                        ModeloContract.InhalerTable.IMAGE + " TEXT NOT NULL" +
                        " )";
        private static final String SQL_CREATE_POSOLOGY_TABLE =
                "CREATE TABLE " + ModeloContract.PosologyTable.TABLE_NAME + " (" +
                        ModeloContract.PosologyTable._ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        ModeloContract.PosologyTable.TEXT_FREQUENCY_XML + " TEXT NOT NULL UNIQUE" +
                        " )";

        private static final String SQL_CREATE_MEDICINE_PRESENTATION_TABLE =
                "CREATE TABLE " + ModeloContract.MedicinePresentationTable.TABLE_NAME + " (" +
                        ModeloContract.MedicinePresentationTable.MEDICINE_NAME + " TEXT NOT NULL, " +
                        ModeloContract.MedicinePresentationTable.ACTIVEINGREDIENT_NAME + " TEXT NOT NULL, " +
                        ModeloContract.MedicinePresentationTable.INHALER_NAME + " TEXT NOT NULL, " +
                        ModeloContract.MedicinePresentationTable.DOSE_AI1 + " REAL NOT NULL, " +
                        ModeloContract.MedicinePresentationTable.DOSE_AI2 + " REAL NOT NULL, " +
                        ModeloContract.MedicinePresentationTable.POSOLOGY_NAME + " INTEGER NOT NULL, " +
                        "FOREIGN KEY(" + ModeloContract.MedicinePresentationTable.MEDICINE_NAME + ") REFERENCES " +
                        ModeloContract.MedicineTable.TABLE_NAME + "(" + ModeloContract.MedicineTable.NAME + ")," +
                        "FOREIGN KEY(" + ModeloContract.MedicinePresentationTable.ACTIVEINGREDIENT_NAME + ") REFERENCES " +
                        ModeloContract.ActiveIngredientTable.TABLE_NAME + "(" + ModeloContract.ActiveIngredientTable.NAME + ")," +
                        "FOREIGN KEY(" + ModeloContract.MedicinePresentationTable.INHALER_NAME + ") REFERENCES " +
                        ModeloContract.InhalerTable.TABLE_NAME + "(" + ModeloContract.InhalerTable.NAME + ")" +
                        " )";

        private static final String SQL_CREATE_USERMANUALSTEP_TABLE =
                "CREATE TABLE " + ModeloContract.UserManualStepTable.TABLE_NAME + " (" +
                        ModeloContract.UserManualStepTable._ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        ModeloContract.UserManualStepTable.TITLE_XML + " TEXT NOT NULL, " +
                        ModeloContract.UserManualStepTable.IMAGE_XML + " TEXT NOT NULL, " +
                        ModeloContract.UserManualStepTable.TEXT_XML + " TEXT NOT NULL, " +
                        ModeloContract.UserManualStepTable.INHALER_NAME + " TEXT NOT NULL, " +
                        "FOREIGN KEY(" + ModeloContract.UserManualStepTable.INHALER_NAME + ") REFERENCES " +
                        ModeloContract.InhalerTable.TABLE_NAME + "(" + ModeloContract.InhalerTable.NAME + ")" +
                        " )";

        private static final String SQL_CREATE_USERMANUALPHYSIOMANUALSTEP_TABLE =
                "CREATE TABLE " + ModeloContract.UserManualPhysioStepTable.TABLE_NAME + " (" +
                        ModeloContract.UserManualPhysioStepTable._ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        ModeloContract.UserManualPhysioStepTable.TITLE_XML + " TEXT NOT NULL, " +
                        ModeloContract.UserManualPhysioStepTable.IMAGE_XML + " TEXT NOT NULL, " +
                        ModeloContract.UserManualPhysioStepTable.TEXT_XML + " TEXT NOT NULL, " +
                        ModeloContract.UserManualPhysioStepTable.PHYSIO_NAME + " TEXT NOT NULL, " +
                        "FOREIGN KEY(" + ModeloContract.UserManualPhysioStepTable.PHYSIO_NAME + ") REFERENCES " +
                        ModeloContract.PhysiotherapyTable.TABLE_NAME + "(" + ModeloContract.PhysiotherapyTable.NAME + ")" +
                        " )";

        private static final String SQL_CREATE_TREATMENT_TABLE =
                "CREATE TABLE " + ModeloContract.TreatmentTable.TABLE_NAME + " (" +
                        ModeloContract.TreatmentTable._ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        ModeloContract.TreatmentTable.RECOMMENDATION + " TEXT, " +
                        ModeloContract.TreatmentTable.RESCUE_ID + " INTEGER NOT NULL, " +
                        "FOREIGN KEY(" + ModeloContract.TreatmentTable.RESCUE_ID + ") REFERENCES " +
                        ModeloContract.InhalerTable.TABLE_NAME + "(" + ModeloContract.InhalerTable.NAME + ")" +
                        " )";

        private static final String SQL_CREATE_RESCUE_TABLE =
                "CREATE TABLE " + ModeloContract.RescueTable.TABLE_NAME + " (" +
                        ModeloContract.RescueTable._ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        ModeloContract.RescueTable.MEDICINE_NAME + " TEXT NOT NULL, " +
                        ModeloContract.RescueTable.ACTIVEINGREDIENT_NAME + " TEXT NOT NULL, " +
                        ModeloContract.RescueTable.INHALER_NAME + " TEXT NOT NULL, " +
                        ModeloContract.RescueTable.DOSE_AI1 + " REAL NOT NULL, " +
                        ModeloContract.RescueTable.DOSE_AI2 + " REAL NOT NULL, " +
                        "FOREIGN KEY(" + ModeloContract.RescueTable.MEDICINE_NAME + ") REFERENCES " +
                        ModeloContract.MedicineTable.TABLE_NAME + "(" + ModeloContract.MedicineTable.NAME + ")," +
                        "FOREIGN KEY(" + ModeloContract.RescueTable.ACTIVEINGREDIENT_NAME + ") REFERENCES " +
                        ModeloContract.ActiveIngredientTable.TABLE_NAME + "(" + ModeloContract.ActiveIngredientTable.NAME + ")," +
                        "FOREIGN KEY(" + ModeloContract.RescueTable.INHALER_NAME + ") REFERENCES " +
                        ModeloContract.InhalerTable.TABLE_NAME + "(" + ModeloContract.InhalerTable.NAME + ")" +
                        " )";

        private static final String SQL_CREATE_TAKE_TABLE =
                "CREATE TABLE " + ModeloContract.TakeTable.TABLE_NAME + " (" +
                        ModeloContract.TakeTable._ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        ModeloContract.TakeTable.TREATMENT_ID + " INTEGER NOT NULL, " +
                        ModeloContract.TakeTable.MEDICINE_NAME + " TEXT NOT NULL, " +
                        ModeloContract.TakeTable.ACTIVEINGREDIENT_NAME + " TEXT NOT NULL, " +
                        ModeloContract.TakeTable.INHALER_NAME + " TEXT NOT NULL, " +
                        ModeloContract.TakeTable.DOSE_AI1 + " REAL NOT NULL, " +
                        ModeloContract.TakeTable.DOSE_AI2 + " REAL NOT NULL, " +
                        ModeloContract.TakeTable.HOUR + " INTEGER NOT NULL, " +
                        ModeloContract.TakeTable.MINUTE + " INTEGER NOT NULL, " +
                        ModeloContract.TakeTable.DONE + " INTEGER NOT NULL, " +
                        ModeloContract.TakeTable.INHALATIONS + " INTEGER NOT NULL, " +
                        "FOREIGN KEY(" + ModeloContract.TakeTable.TREATMENT_ID + ") REFERENCES "
                        + ModeloContract.MedicineTable.TABLE_NAME + "(" + ModeloContract.TakeTable._ID + ")," +
                        "FOREIGN KEY(" + ModeloContract.TakeTable.MEDICINE_NAME + ") REFERENCES "
                        + ModeloContract.MedicineTable.TABLE_NAME + "(" + ModeloContract.MedicineTable.NAME + ")," +
                        "FOREIGN KEY(" + ModeloContract.TakeTable.ACTIVEINGREDIENT_NAME + ") REFERENCES " +
                        ModeloContract.ActiveIngredientTable.TABLE_NAME + "(" + ModeloContract.ActiveIngredientTable.NAME + ")," +
                        "FOREIGN KEY(" + ModeloContract.TakeTable.INHALER_NAME + ") REFERENCES " +
                        ModeloContract.InhalerTable.TABLE_NAME + "(" + ModeloContract.InhalerTable.NAME + ")" +
                        " )";

        private static final String SQL_CREATE_APPOINTMENT_TABLE =
                "CREATE TABLE " + ModeloContract.AppointmentDataTable.TABLE_NAME + " (" +
                        ModeloContract.AppointmentDataTable._ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        ModeloContract.AppointmentDataTable.DAY + " TEXT NOT NULL, " +
                        ModeloContract.AppointmentDataTable.MONTH + " TEXT NOT NULL, " +
                        ModeloContract.AppointmentDataTable.YEAR + " TEXT NOT NULL, " +
                        ModeloContract.AppointmentDataTable.HOUR + " TEXT NOT NULL, " +
                        ModeloContract.AppointmentDataTable.MIN + " TEXT NOT NULL, " +
                        ModeloContract.AppointmentDataTable.LOCATION + " TEXT NOT NULL, " +
                        ModeloContract.AppointmentDataTable.DOCTOR_NAME + " TEXT NOT NULL," +
                        ModeloContract.AppointmentDataTable.DIAGNOSTIC_TEST + " TEXT NOT NULL," +
                        ModeloContract.AppointmentDataTable.VALORATION_TEST + " TEXT NOT NULL" +
                        " )";

        private static final String SQL_CREATE_PATIENTPERSONALDATA_TABLE =
                "CREATE TABLE " + ModeloContract.PatientPersonalDataTable.TABLE_NAME + " (" +
                        ModeloContract.PatientPersonalDataTable._ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        ModeloContract.PatientPersonalDataTable.NAME + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.SURNAME + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.NSS + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.HEIGHT + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.WEIGHT + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.AGE + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.SMOKER + " INTEGER NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.DIABETIC + " INTEGER NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.SICKNESS + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.SICKNESS_STATE + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.ALERGY + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.FAMILY_DOCTOR + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.MEDICAL_SPECIALIST + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.CASUAL_CAREGIVER_NAME + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.CASUAL_CAREGIVER_PHONE + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.CASUAL_CAREGIVER_RELATION + " TEXT NOT NULL, " +
                        ModeloContract.PatientPersonalDataTable.NURSE_IN_CHARGE + " TEXT NOT NULL " +
                        " )";

        private static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + ModeloContract.MedicineTable.TABLE_NAME;

        public DBHelper(Context context, String database_name) {
            super(context, database_name, null, DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_MEDICINE_TABLE);
            db.execSQL(SQL_CREATE_ACTIVEINGREDIENT_TABLE);
            db.execSQL(SQL_CREATE_PHYSIOTERAPY_TABLE);
            db.execSQL(SQL_CREATE_INHALER_TABLE);
            db.execSQL(SQL_CREATE_POSOLOGY_TABLE);
            db.execSQL(SQL_CREATE_MEDICINE_PRESENTATION_TABLE);
            db.execSQL(SQL_CREATE_USERMANUALSTEP_TABLE);
            db.execSQL(SQL_CREATE_USERMANUALPHYSIOMANUALSTEP_TABLE);
            db.execSQL(SQL_CREATE_RESCUE_TABLE);
            db.execSQL(SQL_CREATE_TREATMENT_TABLE);
            db.execSQL(SQL_CREATE_TAKE_TABLE);
            db.execSQL(SQL_CREATE_PATIENTPERSONALDATA_TABLE);
            db.execSQL(SQL_CREATE_APPOINTMENT_TABLE);

            insertTreatmentRow(db, 1, "", -1);
            fillInitialData(db);

        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }

        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }


        private void fillInitialData(SQLiteDatabase db) {

            // controlar excepciones

            XmlUserManualInhParser umInhParser = new XmlUserManualInhParser(new UserManualInhalersInserter(db), null);
            XmlUserManualPhysioParser umPhysioParser = new XmlUserManualPhysioParser(new UserManualPhysiotherapiesInserter(db), umInhParser);
            XMLResMedPresParser resMedPresParser = new XMLResMedPresParser(new ResMedPresentationsInserter(db), umPhysioParser);
            XmlMedPresentationsParser medPresParser = new XmlMedPresentationsParser(new MedPresentationsInserter(db), resMedPresParser);
            XmlPosologiesParser posolParser = new XmlPosologiesParser(new PosologiesInserter(db), medPresParser);
            XmlInhalerParser inhParser = new XmlInhalerParser(new InhalersInserter(db), posolParser);
            XmlPhysiotherapyParser physioParser = new XmlPhysiotherapyParser(new PhysiotherapiesInserter(db), inhParser);
            XmlActiveIngredientParser actIngParser = new XmlActiveIngredientParser(new ActiveIngredientsInserter(db), physioParser);
            XmlMedicineParser medParser = new XmlMedicineParser(new MedicinesInserter(db), actIngParser);

            DatabaseXMLParser databaseXMLParser = new DatabaseXMLParser(medParser);

            try {
                ArrayList<String> medicineNamesList = new ArrayList<>();
                ArrayList<String> actIngrNamesList = new ArrayList<>();
                ArrayList<String> inhalersList = new ArrayList<>();
                ArrayList<String> physiotherapyList = new ArrayList<>();
                ArrayList<String> posologiesList = new ArrayList<>();
                ArrayList<String> medicinePresentations = new ArrayList<>();
                ArrayList<String> rescueMedPresentations = new ArrayList<>();
                ArrayList<String> userManualPhysioSteps = new ArrayList<>();
                ArrayList<String> userManualSteps = new ArrayList<>();


                databaseXMLParser.parse(app_context, medicineNamesList, actIngrNamesList, inhalersList,
                        physiotherapyList, posologiesList, medicinePresentations, userManualPhysioSteps,
                        rescueMedPresentations, userManualSteps);

                for (String medicineName : medicineNamesList) {
                    insertMedicineRow(db, medicineName);
                }
                for (String actIngrName : actIngrNamesList) {
                    insertActiveIngredientRow(db, actIngrName);
                }
                for (String inhalerName : inhalersList) {
                    String[] inhalerParts = inhalerName.split(String.valueOf(DatabaseXMLParser.FIELD_SEPARATOR));
                    if (inhalerParts.length == 2) {
                        insertInhalerRow(db, inhalerParts[0], inhalerParts[1]);
                    } else {
                        System.err.println("Error when inserting " + inhalerName + " in DB.");
                    }
                }
                for (String physiotherapyName : physiotherapyList) {
                    String[] physiotherapyParts = physiotherapyName.split(String.valueOf(DatabaseXMLParser.FIELD_SEPARATOR));
                    if (physiotherapyParts.length == 2) {
                        insertPhsyotherapyrRow(db, physiotherapyParts[0], physiotherapyParts[1]);
                    } else {
                        System.err.println("Error when inserting " + physiotherapyName + " in DB.");
                    }
                }
                for (String posologyName : posologiesList) {
                    insertPosologyRow(db, posologyName);
                }
                for (String medPresentation : medicinePresentations) {
                    String[] medPresParts = medPresentation.split(String.valueOf(DatabaseXMLParser.FIELD_SEPARATOR));
                    if (medPresParts.length == 5) {
                        String doses[] = medPresParts[3].split(",");
                        if (doses.length == 2) {
                            float doseAI1AsFloat = Float.parseFloat(doses[0]);
                            float doseAI2AsFloat = Float.parseFloat(doses[1]);
                            insertMedicinePresentationRow(db, medPresParts[0], medPresParts[1],
                                    medPresParts[2], doseAI1AsFloat, doseAI2AsFloat,
                                    medPresParts[4]);
                        } else {
                            System.err.println("Error when inserting " + medPresentation + " in DB.");
                        }
                    } else {
                        System.err.println("Error when inserting " + medPresentation + " in DB.");
                    }
                }
                for (String rescueMedPresentation : rescueMedPresentations) {
                    String[] resMedPresParts = rescueMedPresentation.split(String.valueOf(DatabaseXMLParser.FIELD_SEPARATOR));
                    if (resMedPresParts.length == 4) {
                        String doses[] = resMedPresParts[3].split(",");
                        if (doses.length == 2) {
                            float doseAI1AsFloat = Float.parseFloat(doses[0]);
                            float doseAI2AsFloat = Float.parseFloat(doses[1]);
                            insertRescueRow(db, resMedPresParts[0], resMedPresParts[1],
                                    resMedPresParts[2], doseAI1AsFloat, doseAI2AsFloat);
                        } else {
                            System.err.println("Error when inserting " + rescueMedPresentation + " in DB.");
                        }
                    } else {
                        System.err.println("Error when inserting " + rescueMedPresentation + " in DB.");
                    }
                }

                for (String userManualStep : userManualSteps) {
                    String[] userManualStepParts = userManualStep.split(String.valueOf(DatabaseXMLParser.FIELD_SEPARATOR));
                    if (userManualStepParts.length == 4) {
                        insertUserManualStepRow(db, userManualStepParts[0], userManualStepParts[1],
                                userManualStepParts[2], userManualStepParts[3]);
                    } else {
                        System.err.println("Error when inserting " + userManualStep + " in DB.");
                    }
                }
                for (String userManualPhysioStep : userManualPhysioSteps) {
                    String[] userManualPhysioStepParts = userManualPhysioStep.split(String.valueOf(DatabaseXMLParser.FIELD_SEPARATOR));
                    if (userManualPhysioStepParts.length == 4) {
                        insertUserManualPhysioStepRow(db, userManualPhysioStepParts[0], userManualPhysioStepParts[1],
                                userManualPhysioStepParts[2], userManualPhysioStepParts[3]);
                    } else {
                        System.err.println("Error when inserting " + userManualPhysioStep + " in DB.");
                    }
                }
            } catch (java.io.IOException jioExcep) {
                System.err.println("io exception");
            } catch (org.xmlpull.v1.XmlPullParserException xmlExcep) {
                System.err.println("XmlPullParserException exception");
            }
        }

    }

    /*
    public ArrayList<Inhalador> obtenerListaInhaladores() {
        String columnas[] = {ModeloContract.InhalerTable._ID, ModeloContract.InhalerTable.NAME, ModeloContract.InhalerTable.IMAGE};
        Cursor cursorInhalador = db.query(ModeloContract.InhalerTable.TABLE_NAME, columnas, null, null, null, null, null);
        ArrayList<Inhalador> listaInhaladores = new ArrayList<>();
        while (cursorInhalador.moveToNext()) {
            String inhName = cursorInhalador.getString(1);
            String inhNameText = Commodities.retrieveResourceStringFromId(app_context, inhName);
            Inhalador inhalador = new Inhalador(cursorInhalador.getInt(0), inhName, inhNameText, cursorInhalador.getString(2), null);
            listaInhaladores.add(inhalador);
        }
        cursorInhalador.close();
        return listaInhaladores;
    }
*/

/*
    // Retrieve from DB all rows from table ActiveIngredient
    public ArrayList<PrincipioActivo> obtenerListaPrincipiosActivos() {
        String columnas[] = {ModeloContract.ActiveIngredientTable._ID, ModeloContract.ActiveIngredientTable.NAME};
        Cursor cursorPrincipioActivo =  db.query(ModeloContract.ActiveIngredientTable.TABLE_NAME, columnas, null, null, null, null, null);
        ArrayList<PrincipioActivo> listaPrincipios = new ArrayList<>();
        while (cursorPrincipioActivo.moveToNext()) {
            String paName = cursorPrincipioActivo.getString(1);
            String paNameText = Commodities.retrieveResourceStringFromId(app_context, paName);
            PrincipioActivo principioActivo = new PrincipioActivo(cursorPrincipioActivo.getInt(0), paName, paNameText);
            listaPrincipios.add(principioActivo);
        }
        cursorPrincipioActivo.close();
        return listaPrincipios;
    }
*/
/*
    public ArrayList<Posologia> obtenerListaPosologias() {
        String columnas[] = {ModeloContract.PosologyTable._ID, ModeloContract.PosologyTable.TEXT_FREQUENCY_XML};
        Cursor cursorPosologia = db.query(ModeloContract.PosologyTable.TABLE_NAME,
                                          columnas, null, null, null, null, null);
        ArrayList<Posologia> listaPosologias = new ArrayList<>();
        while (cursorPosologia.moveToNext()) {
            String posolName = cursorPosologia.getString(1);
            String posolStringsText = Commodities.retrieveResourceStringFromId(app_context, posolName);
            Posologia posol = new Posologia(cursorPosologia.getInt(0), posolName, posolStringsText);
            listaPosologias.add(posol);
        }
        cursorPosologia.close();
        return listaPosologias;
    }
*/
/*
    // Retrieve Medicine from id
    public Medicamento obtenerMedicamento(int m) {
        // Medicine columns : _id (int), name (text)
        Cursor cmed= db.rawQuery("SELECT * FROM " +
                        ModeloContract.MedicineTable.TABLE_NAME + " WHERE " +
                        ModeloContract.MedicineTable._ID + " = ?",
                new String[]{String.valueOf(m)});
        Medicamento medic;
        if (0 < cmed.getCount()) {
            cmed.moveToNext();
            String medName = cmed.getString(1);
            String medNameText = Commodities.retrieveResourceStringFromId(app_context, medName);
            medic = new Medicamento(cmed.getInt(0), medName, medNameText);
        }
        else { medic = null; }
        cmed.close();
        return medic;
    }
*/
/*
    //Obtener principio
    public PrincipioActivo obtenerPrincipio(int ai) {
        // ActiveIngredient columns : _id (int), name (text)
        Cursor cActIng = db.rawQuery("SELECT  * FROM " +
                        ModeloContract.ActiveIngredientTable.TABLE_NAME + " WHERE " +
                        ModeloContract.ActiveIngredientTable._ID +  " = ?",
                new String[]{String.valueOf(ai)});
        PrincipioActivo actIngr;
        if (0 < cActIng.getCount()) {
            cActIng.moveToNext();
            String paName = cActIng.getString(1);
            String paNameText = Commodities.retrieveResourceStringFromId(app_context, paName);
            actIngr = new PrincipioActivo(cActIng.getInt(0), paName, paNameText);
        }
        else { actIngr = null; }
        cActIng.close();
        return actIngr;
    }
*/
/*
        //Obtener posologia
        public Posologia obtenerPosologia(int p) {
            Cursor cursorPosol = db.rawQuery("SELECT * FROM " +
                            ModeloContract.PosologyTable.TABLE_NAME + " WHERE " +
                            ModeloContract.PosologyTable._ID + " = ?",
                    new String[]{String.valueOf(p)});
            // Posology columns : _id (int), text_frequency_xml (text)
            Posologia pos;
            if (0 < cursorPosol.getCount()) {
                cursorPosol.moveToNext();
                String posolName = cursorPosol.getString(1);
                String posolNameText = Commodities.retrieveResourceStringFromId(app_context, posolName);
                pos = new Posologia(cursorPosol.getInt(0), posolName, posolNameText);
            }
            else { pos = null; }
            cursorPosol.close();
            return pos;
        }
*/

/*
    public void testDB(){

        ArrayList<Medicamento> listaMedicamentos = obtenerListaMedicamentos();
        for (Medicamento med : listaMedicamentos){
            Medicamento med2 = getMedicineByName(med.getName());
            System.out.println(med2.getId() + " " + med2.getName() + " " + med2.getNameText());
        }

        ArrayList<PrincipioActivo> listaPrincipios = obtenerListaPrincipiosActivos();
        for (PrincipioActivo pa : listaPrincipios){
            PrincipioActivo pa2 = getActiveIngredientByName(pa.getName());
            System.out.println(pa2.getId() + " " + pa2.getName() + " " + pa2.getNameText());
        }

        ArrayList<Inhalador> listaInhaladores = obtenerListaInhaladores();
        for (Inhalador inh : listaInhaladores){
            Inhalador inh2 = getInhalerByName(inh.getName());
            System.out.println(inh2.getId() + " " + inh2.getName() + " " + inh2.getNameText() +
                    inh2.getImagen());
        }

        ArrayList<Posologia> listaPosologias = obtenerListaPosologias();
        for (Posologia pos : listaPosologias){
            Posologia pos2 = getPosologyByDescription(pos.getName());
            System.out.println(pos2.getId() + " " + pos2.getName() + " " + pos2.getNameText());
        }

        Cursor cursorMedPres = db.rawQuery("SELECT * FROM " +
                                           ModeloContract.MedicinePresentationTable.TABLE_NAME, null);
        while(cursorMedPres.moveToNext()){
            System.out.println(cursorMedPres.getInt(0) + " " + cursorMedPres.getInt(1) + " "  +
                               cursorMedPres.getInt(2) + " " + cursorMedPres.getFloat(3) + " "  +
                               cursorMedPres.getFloat(4) + cursorMedPres.getInt(5));
        }
        cursorMedPres.close();

        for (Inhalador inh : obtenerListaInhaladores()){
            ArrayList<Paso> pasos = obtenerPasosInhalador(inh.getId());
            for (Paso p : pasos){
                System.out.println(p.getId() + " " + p.getTitulo() + " " + p.getImagen() + " " +
                                   p.getTexto() + " " + inh.getId());
            }
        }

    }
*/

}
