/***********************************************************************
 * This file is part of MyEPOC.
 *
 * MyEPOC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MyEPOC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MyEPOC.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

package es.uma.ingenieriasalud.retoepoc.alarms;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import es.uma.ingenieriasalud.retoepoc.Commodities;
import es.uma.ingenieriasalud.retoepoc.MainActivity;
import es.uma.ingenieriasalud.retoepoc.manuals.ScreenSlideActivity;
import es.uma.ingenieriasalud.retoepoc.persistence.Inhalador;
import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.persistence.Paso;
import es.uma.ingenieriasalud.retoepoc.persistence.Toma;
import es.uma.ingenieriasalud.retoepoc.R;

public class ConsultarTomasAlarmas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commodities.setSelectedLanguage(this);

        setContentView(R.layout.activity_consultar_tomas_alarmas);

        Modelo modelo;
        ListView listView;
        ListViewAlarmaAdapter adapterAlarma;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if(notification == null){
            // notification is null, using backup
            notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            // I can't see this ever being null (as always have a default notification)
            // but just incase
            if(notification == null) {
                // notification backup is null, using 2nd backup
                notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
        //final MediaPlayer mMediaPlayer = new MediaPlayer();

        //Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();

        /*try {
            mMediaPlayer.setDataSource(this, notification);
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
        final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        final int vol = audioManager.getStreamVolume(AudioManager.STREAM_ALARM);

       /* try {
            mMediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        //mMediaPlayer.start();


        final int hora = this.getIntent().getIntExtra("hora",-1);
        final int min = this.getIntent().getIntExtra("min",-1);
        final int id = this.getIntent().getIntExtra("id",-1);
        final int veces = this.getIntent().getIntExtra("veces",0);
        final Intent i = new Intent(getBaseContext(), AlarmReceiver.class);
        i.putExtra("hora",hora);
        i.putExtra("min",min);
        i.putExtra("id",id);

        SharedPreferences preferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        boolean smsEnabled = preferences.getBoolean("sms", false);

        modelo = Modelo.getInstance();
        modelo.init(this);

        String casualCaregiverPhone = modelo.getPatientPersonalData().getCasualCaregiverPhone();

        if(smsEnabled && casualCaregiverPhone != null && !casualCaregiverPhone.equals("")) {
            modelo.getPatientPersonalData().getCasualCaregiverPhone();
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(casualCaregiverPhone, null, getString(R.string.smsText), null, null);
        }

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.alertDialogAlarmaTitulo)
                .setMessage(R.string.alertDialogAlarmaTexto)
                .setPositiveButton(R.string.alertDialogAlarmaPosponer, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        //Posponer
                            r.stop();
                            audioManager.setStreamVolume(AudioManager.STREAM_ALARM, vol,AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);

                            i.putExtra("veces",veces+1);
                            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            PendingIntent pendingIntent = PendingIntent.getBroadcast(ConsultarTomasAlarmas.this.getApplicationContext(), id, i, PendingIntent.FLAG_UPDATE_CURRENT);
                            if(veces<3) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                    System.out.println("Alarm " + id + " is set at 15 min more");
                                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + AlarmManager.INTERVAL_FIFTEEN_MINUTES, pendingIntent);
                                } else {
                                    System.out.println("Alarm " + id + " is set at 15 min more");
                                    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + AlarmManager.INTERVAL_FIFTEEN_MINUTES, pendingIntent);
                                }
                            }else{
                                Calendar alarm = Calendar.getInstance();
                                alarm.set(Calendar.HOUR_OF_DAY, hora);
                                alarm.set(Calendar.MINUTE, min);
                                alarm.set(Calendar.SECOND, 0);
                                alarm.add(Calendar.DAY_OF_MONTH, 1);

                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                    System.out.println("Alarm " + id + " is set at " + alarm.getTime());
                                    alarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getTimeInMillis(), pendingIntent);
                                } else {
                                    System.out.println("Alarm " + id + " is set at " + alarm.getTime());
                                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarm.getTimeInMillis(), pendingIntent);
                                }
                            }


                        Intent i = new Intent(ConsultarTomasAlarmas.this.getApplicationContext(), MainActivity.class);
                        startActivity(i);

                    }
                })
                .setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Ok
                        r.stop();
                        audioManager.setStreamVolume(AudioManager.STREAM_ALARM, vol,AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);

                        i.putExtra("veces",0);
                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(ConsultarTomasAlarmas.this.getApplicationContext(), id, i, PendingIntent.FLAG_UPDATE_CURRENT);

                        Calendar alarm = Calendar.getInstance();
                        alarm.set(Calendar.HOUR_OF_DAY, hora);
                        alarm.set(Calendar.MINUTE, min);
                        alarm.set(Calendar.SECOND, 0);
                        alarm.add(Calendar.DAY_OF_MONTH, 1);

                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                            System.out.println("Alarm " + id + " is set at " + alarm.getTime());
                            alarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getTimeInMillis(), pendingIntent);
                        } else {
                            System.out.println("Alarm " + id + " is set at " + alarm.getTime());
                            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarm.getTimeInMillis(), pendingIntent);
                        }
                    }
                })
                .show();


        //Inicializar lista de tomas
        listView = (ListView) findViewById(R.id.listViewTomasAlarmas);

        ArrayList<Toma> listaTomas = modelo.obtenerListaTomas();
        ArrayList<Toma> listaTomasHora = new ArrayList<>();

        for (Toma tm : listaTomas){

            if(tm.getHour().getHour() == hora && tm.getHour().getMin() == min){
                listaTomasHora.add(tm);
            }

        }

        adapterAlarma = new ListViewAlarmaAdapter(modelo, this, listaTomasHora);
        listView.setAdapter(adapterAlarma);

        Commodities.ajustarVistaListView(listView);
//     listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

    }


    public void onClickManual(View view){

        TextView textViewIata = (TextView) findViewById(R.id.hiddenIdTake);
        String inhaladorSeleccionado = (String) textViewIata.getText();

        Modelo modelo = Modelo.getInstance();
        modelo.init(view.getContext());
        Inhalador inha = modelo.obtenerInhalador(Integer.parseInt(inhaladorSeleccionado));

        inha.setListaPasosManual(modelo.obtenerPasosInhalador(inha.getName()));

        ArrayList<String> titulos = new ArrayList<>();
        ArrayList<Integer> imagenes = new ArrayList<>();
        ArrayList<String> textos = new ArrayList<>();
        for(Paso p:inha.getListaPasosManual()){
            titulos.add(Commodities.retrieveResourceStringFromId(view.getContext(),p.getTitulo()));
            imagenes.add(p.getImagen());
            textos.add(Commodities.retrieveResourceStringFromId(view.getContext(), p.getTexto()));
        }
        Intent intent = new Intent(view.getContext(), ScreenSlideActivity.class);
        Bundle b = new Bundle();

        b.putIntegerArrayList("imagenes", imagenes);
        b.putStringArrayList("texto", textos);
        b.putStringArrayList("titulos", titulos);

        intent.putExtras(b);
        startActivity(intent);

    }

public void onClickFinalizar(View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    /*public void ajustarVistaListView(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount()+15));
        listView.setLayoutParams(params);
    }*/

}
