package es.uma.ingenieriasalud.retoepoc.calls;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import es.uma.ingenieriasalud.retoepoc.persistence.Modelo;
import es.uma.ingenieriasalud.retoepoc.R;

/**
 * Created by jmalvarez on 27/5/16.
 */
public class CallManager {

    public static void callCaregiver(Context context) {

        Modelo modelo = Modelo.getInstance();
        modelo.init(context);
        String caregiverPhone = modelo.getPatientPersonalData().getCasualCaregiverPhone();
        if (caregiverPhone == null || caregiverPhone.equals("")) {

            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
            alertDialogBuilder.setTitle(R.string.alerDialogTitulo);
            alertDialogBuilder
                    .setCancelable(true)
                    .setMessage(R.string.mensajeNoCuidador)
                    .setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",
                    caregiverPhone, null));
            context.startActivity(intent);
        }
    }

    public static void callEmergency(Context context) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",
                context.getString(R.string.emergencyPhoneNumber), null));
        context.startActivity(intent);
    }

}
