package es.uma.ingenieriasalud.retoepoc;

import android.app.Application;
import android.test.ApplicationTestCase;

/**
 * <content_crear_tratamiento href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</content_crear_tratamiento>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }
}